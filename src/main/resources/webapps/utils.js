//Date JQUERY UI
function initDate(id) {
	$(id).datepicker({
		changeMonth : true,
		changeYear : true,
		dateFormat : "dd M yy",
		maxDate : "+0D"
	}).datepicker("setDate", new Date());
}

function initRangeDate(from,to,minOrMax){
	$(from).datepicker({
		defaultDate: "+0d",
		changeMonth: true,
		changeYear : true,
		dateFormat : "dd M yy",
		numberOfMonths: 1,
		maxDate : "+0D",
		onClose: function( selectedDate ) {
			$(to).datepicker( "option", minOrMax, selectedDate );
		}
	}).datepicker("setDate", new Date());
}

//Check numeric 
function isNumeric(n) {
	return !isNaN(parseFloat(n)) && isFinite(n) && n > 0;
}

function isNumericZero(n) {
	return !isNaN(parseFloat(n)) && isFinite(n) && n >= 0;
}

//Round up float with truncate to 2 decimal places
function truncateFloat(n){
	Math.round(n*100)/100;
	return n;
}


//Checkbox toggle utility
function toggleRow(){
	$(".case").click(function() {
		$(this).parent().parent().toggleClass('info');
		if ($(".case").length == $(".case:checked").length) {
			$("#selectall").attr("checked", "checked");
		} else {
			$("#selectall").removeAttr("checked");
		}

	});
}

function toggleAll() {
	$("#selectall").click(function() {
		if (this.checked) {
			$('.case').each(function() {
				$(this).click();
			});
		} else {
			$('.case').each(function() {
				$(this).click();
			});
		}
	});
}

//Feedback utility i.e. textbox success/ failure
function validateInputSuccess(txtFieldId,statusFieldId){
	$(txtFieldId).parent().removeClass('has-error').addClass(
	'has-feedback').addClass('has-success');
	$(statusFieldId).removeClass('glyphicon-remove').addClass(
	'glyphicon-ok').show();
}

function validateInputError(txtFieldId,statusFieldId){
	$(txtFieldId).parent().removeClass('has-success').addClass(
	'has-feedback').addClass('has-error');
	$(statusFieldId).removeClass('glyphicon-ok').addClass(
	'glyphicon-remove').show();
}

function validateAutcomplete(txtFieldId,statusFieldId) {
	var name = $(txtFieldId).val();
	var status=false;
	if (name != "") {
		validateInputSuccess(txtFieldId,statusFieldId);
		status=true;
	} else {
		validateInputError(txtFieldId,statusFieldId);
		alert('Please select Name from list');
	}
	return status;
}

function validateNumber(txtFieldId,statusFieldId){
	var value = $(txtFieldId).val();
	var status=false;
	if(isNumeric(value)){
			validateInputSuccess(txtFieldId,statusFieldId);
			status=true;
		} else {
			validateInputError(txtFieldId,statusFieldId);
		}
	return status;
}