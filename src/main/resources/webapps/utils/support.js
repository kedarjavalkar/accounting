$('document').ready(function() {
	$.ajaxSetup({
		beforeSend : function() {
			loadingDiv.start();
		},
		complete : function() {
			loadingDiv.stop();
		},
		error : function (jqXHR) {
			if(jqXHR.status != 200) {
				if (jqXHR.status == 400) {
					jAlert.error(jqXHR.responseJSON.warning);
				}
				else if(jqXHR.status == 401) {
					jAlert.error('Invalid credentials., Please login again...');
					setTimeout(function(){
						document.write(jqXHR.responseText);
					},2000);
				}
				else {
					jAlert.error(jqXHR.responseText);
				}
			}
		}
	});
});

function logout() {
	$.ajax({
		url : "api/user/logout",
		type : 'POST',
		dataType : 'json',
		contentType : 'application/json',
		success : function(data) {
			document.write(data);
		},
	});
}

function openCloseBS() {
	$.ajax({
		url : "api/balancesheet/openclose?from=oldest",
		type : 'GET',
		dataType : 'json',
		contentType : 'application/json',
	});
}

/***** Date JQUERY UI *******/
function initDate(id) {
	$(id).datepicker({
		changeMonth : true,
		changeYear : true,
		dateFormat : "dd M yy",
		maxDate : "+0D"
	}).datepicker("setDate", new Date());
}

function initRangeDate(from,to,minOrMax){
	$(from).datepicker({
		defaultDate: "+0d",
		changeMonth: true,
		changeYear : true,
		dateFormat : "dd M yy",
		numberOfMonths: 1,
		maxDate : "+0D",
		onClose: function( selectedDate ) {
			$(to).datepicker( "option", minOrMax, selectedDate );
		}
	}).datepicker("setDate", new Date());
}

//Check numeric 
function isNumeric(n) {
	return !isNaN(parseFloat(n)) && isFinite(n) && n > 0;
}

function isNumericZero(n) {
	return !isNaN(parseFloat(n)) && isFinite(n) && n >= 0;
}

//Round up float with truncate to 2 decimal places
function truncateFloat(n){
	Math.round(n*100)/100;
	return n;
}


//Checkbox toggle utility
function toggleRow(){
	$(".case").click(function() {
		$(this).parent().parent().toggleClass('info');
		if ($(".case").length == $(".case:checked").length) {
			$("#selectall").attr("checked", "checked");
		} else {
			$("#selectall").removeAttr("checked");
		}

	});
}

function toggleAll() {
	$("#selectall").click(function() {
		if (this.checked) {
			$('.case').each(function() {
				$(this).click();
			});
		} else {
			$('.case').each(function() {
				$(this).click();
			});
		}
	});
}

//Feedback utility i.e. textbox success/ failure
function validateInputSuccess(txtFieldId,statusFieldId){
	$(txtFieldId).parent().removeClass('has-error').addClass(
	'has-feedback').addClass('has-success');
	$(statusFieldId).removeClass('glyphicon-remove').addClass(
	'glyphicon-ok').show();
}

function validateInputError(txtFieldId,statusFieldId){
	$(txtFieldId).parent().removeClass('has-success').addClass(
	'has-feedback').addClass('has-error');
	$(statusFieldId).removeClass('glyphicon-ok').addClass(
	'glyphicon-remove').show();
}

function validateAutcomplete(txtFieldId,statusFieldId) {
	var name = $(txtFieldId).val();
	var status=false;
	if (name != "") {
		validateInputSuccess(txtFieldId,statusFieldId);
		status=true;
	} else {
		validateInputError(txtFieldId,statusFieldId);
		alert('Please select Name from list');
	}
	return status;
}

function validateNumber(txtFieldId,statusFieldId){
	var value = $(txtFieldId).val();
	var status=false;
	if(isNumeric(value)){
		validateInputSuccess(txtFieldId,statusFieldId);
		status=true;
	} else {
		validateInputError(txtFieldId,statusFieldId);
	}
	return status;
}


/**** Alert *****/
var btn = '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
var jAlert = {'btn':btn};
jAlert.success = function(msg) {
	$('body').append('<div role="alert" class="alert alert-success" style="top:'+getTop()+'px;">'+jAlert.btn+'<strong>Success! </strong>'+ msg +'</div>');
	var ele = $('body .alert-success').last();
	setTimeout(function(){
		$(ele).remove();
	},5000);
}
jAlert.error = function(msg) {
	$('body').append('<div role="alert" class="alert alert-danger" style="top:'+getTop()+'px;">'+jAlert.btn+'<strong>Error! </strong>'+ msg +'</div>');
	var ele = $('body .alert-danger').last();
	setTimeout(function(){
		$(ele).remove();
	},5000);
}
jAlert.info = function(msg) {
	$('body').append('<div role="alert" class="alert alert-info" style="top:'+getTop()+'px;">'+jAlert.btn+'<strong>Info! </strong>'+ msg +'</div>');
	var ele = $('body .alert-info').last();
	setTimeout(function(){
		$(ele).remove();
	},5000);
}
function getTop() {
	return 25 + ($('.alert').length * 10);
}

/**** Loading div *****/
var html = '<div id="loadingDiv" class="hide">'+
'<span class="rect1"></span>'+
'<span class="rect2"></span>'+
'<span class="rect3"></span>'+
'<span class="rect4"></span>'+
'<span class="rect5"></span>'+
'<span class="rect6"></span>'+
'<span class="rect7"></span>'+
'<span class="rect8"></span>'+
'<div>Loading...</div>'+
'</div>';
var loadingDiv = {'html':html};
$('body').append(loadingDiv.html);

loadingDiv.start = function () {
	$('.theme-showcase').addClass('blur');
	$('#loadingDiv').removeClass('hide');
}

loadingDiv.stop = function () {
	$('.theme-showcase').removeClass('blur');
	$('#loadingDiv').addClass('hide');
}