function drawBalanceSheet(date,incomeBoiler,expenseBoiler,calcClosing,closingAmt){
	var bsBoiler='<h3>Balance Sheet - '+date+'</h3>'+
	'<div class="table-responsive col-sm-6">'+
	'<table class="table table-bordered">'+
	'<thead>'+
	'<tr><th colspan="2">Income</th></tr>'+
	'<tr><th>Name</th><th>Amount</th></tr>'+
	'</thead>'+
	'<tbody class="incomeTableBody">'+incomeBoiler+'</tbody>'+
	'</table>'+
	'</div>'+
	'<div class="table-responsive col-sm-6">'+
	'<table class="table table-bordered">'+
	'<thead>'+
	'<tr><th colspan="2">Expenses</th></tr>'+
	'<tr><th>Name</th><th>Amount</th></tr>'+
	'</thead>'+
	'<tbody class="expenseTableBody">'+expenseBoiler+'</tbody>'+
	'</table>'+
	'</div>'+
	'<div class="table-responsive col-sm-12">'+
	'<table class="table table-bordered">'+
	'<thead>'+
	'<tr class="success">'+
	'<th>Closing Balance</th>'+
	'<th class="calcClosing">'+calcClosing+'</th>'+
	'<th class="closingAmt">'+closingAmt+'</th>'+
	'</tr>'+
	'</thead>'+
	'</table>'+
	'</div>';
	//'<br/><br/>';
	return bsBoiler;
}

function searchBSByDate() {
	var rVal = $("input[name='inlineRadioOptions']:checked").val();
	var url='';
	if(rVal=='range')
		url="api/balancesheet/date?from="+$('#from').val()+"&to="+$('#to').val();
	else
		url="api/balancesheet/date?date="+$('#txtDate').val();
	$('#bsDispDiv').html('');
	//$('#sheetDate').html(optionVal);
	$
	.ajax({
		url : url,
		type : 'GET',
		dataType : 'json',
		/* error : function() {
					alert("PO List Error Occured");
				}, */
		success : function(data) {
			var incomeBoiler = '';
			var incomeTotal = parseInt(0);
			var expenseBoiler = '';
			var expenseTotal = parseInt(0);
			var dae='';
			var tableBoiler='';
			//balanceSheet loop
			for ( var i = 0; i < data.length; i++) {
				var incomeRowCount = 1;
				var expenseRowCount = 0;
				var totalRowCount = data[i]["balanceSheetTransaction"].length;

				incomeBoiler = '<tr><th>Opening Balance</th><th>'
					+ (data[i].openingAmount)
					.toLocaleString('en-IN')
					+ '</th></tr>';
				expenseBoiler='';
				incomeTotal=0;
				expenseTotal=0;
				incomeTotal += parseInt(data[i].openingAmount);
				dae=data[i]["date"];
				for ( var j = 0; j < totalRowCount; j++) {
					if (data[i]["balanceSheetTransaction"][j]["accountType"] == "1") {
						incomeBoiler += '<tr><td>'
							+ data[i]["balanceSheetTransaction"][j]["accountName"]
						+ '</td>';
						incomeBoiler += '<td>'
							+ data[i]["balanceSheetTransaction"][j]["amount"]
						.toLocaleString('en-IN')
						+ '</td></tr>';
						incomeTotal += data[i]["balanceSheetTransaction"][j]["amount"];
						incomeRowCount++;
					} else {
						expenseBoiler += '<tr><td>'
							+ data[i]["balanceSheetTransaction"][j]["accountName"]
						+ '</td>';
						expenseBoiler += '<td>'
							+ data[i]["balanceSheetTransaction"][j]["amount"]
						.toLocaleString('en-IN')
						+ '</td></tr>';
						expenseTotal += data[i]["balanceSheetTransaction"][j]["amount"];
						expenseRowCount++;
					}
				}
				if (incomeRowCount < expenseRowCount) {
					for ( var k = 0; k < expenseRowCount
					- incomeRowCount; k++) {
						incomeBoiler += '<tr><td>.</td><td>.</td></tr>';
					}
				}
				if (expenseRowCount < incomeRowCount) {
					for ( var k = 0; k < incomeRowCount
					- expenseRowCount; k++) {
						expenseBoiler += '<tr><td>.</td><td>.</td></tr>';
					}
				}
				incomeBoiler += '<tr><td class="text-right"><b>Total</b></td><td><b>'
					+ incomeTotal.toLocaleString('en-IN')
					+ '</b></td></tr>';
				expenseBoiler += '<tr><td class="text-right"><b>Total</b></td><td><b>'
					+ expenseTotal.toLocaleString('en-IN')
					+ '</b></td></tr>';

				//$("#incomeTableBody").html(incomeBoiler);
				//$("#expenseTableBody").html(expenseBoiler);
				var calcClosing=incomeTotal.toLocaleString('en-IN')+' - '+expenseTotal.toLocaleString('en-IN');
				var closingAmt=(incomeTotal - expenseTotal).toLocaleString('en-IN');
				tableBoiler+=drawBalanceSheet(dae,incomeBoiler,expenseBoiler,calcClosing,closingAmt);
				$('#bsDispDiv').html(tableBoiler);
			}
		}
	});
}