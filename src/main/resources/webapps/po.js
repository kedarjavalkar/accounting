function saveFarmer() {
	var farmer = {};
	farmer.name = $('#txtNewFarmer').val();

	$
	.ajax({
		url : "api/farmer",
		type : 'POST',
		contentType : 'application/json',
		dataType : 'json',
		data : JSON.stringify(farmer),
		/*error : function(data, status) {
			$('#newFarmerStatus')
			.html(
			'<p class=\"bg-danger\" style=\"padding:15px\"> Please Enter Valid Farmer Name </p>').show().delay(2000).fadeOut();
		},*/
		success : function(data) {
			$('#newFarmerStatus')
			.html(
			'<p class=\"bg-success\" style=\"padding:15px\"> Farmer Added Successfully !</p>').show().delay(2000).fadeOut();
			$('#txtNewFarmer').val('');
		}
	});
}

function format(d) {
	// `d` is the original data object for the row
	var rowData='';
	rowData+= '<table class="table table-bordered table-striped table-condensed">'+
	'<tr><th>Date</th><th>Paid</th><th>Mode</th></tr>';
	for(var i=0;i<d.purchaseOrderTransaction.length;i++){
		rowData+='<tr>'+
		'<td>'+d.purchaseOrderTransaction[i].date+'</td>'+
		'<td>'+d.purchaseOrderTransaction[i].paidAmount+'</td>'+
		'<td>'+d.purchaseOrderTransaction[i].paymentMethod+'</td>'+
		'</tr>';
	}
	rowData+='</table>';
	return rowData;
}

function updatePODataTable(dataSet){
	//var dataSet=[{"id": 1,"date": "2011/04/25","farmer": {"id": "1","name": "Kedar"},"weight": "1000","trays": "10","amount": "5421","pending": "5421","pot":[{"date":"2011/04/25","paid":"4500"},{"date":"2013/08/25","paid":"8500"}]},{"id": 2,"date": "2011/04/25","farmer": {"id": "2","name": "Amol"},"weight": "2000","trays": "20","amount": "55421","pending": "15421","pot":[{"date":"2011/04/25","paid":"4500"},{"date":"2013/08/25","paid":"8500"}]}];
	/*var poTable = */$('#updatePOTable').DataTable(
			{
				"destroy": true,
				"paging":   false,
				"searching": false,
				"columnDefs" : [ {
					"targets" : [ 0, 1, 3, 4, 5 ],
					"searchable" : false,
				}, {
					"orderable" : false,
					"targets" : [ 0 ]
				} ],
				"language" : {
					"decimal" : ".",
					"thousands" : ","
				},
				"processing": true,
				/*"ajax":{"url": "api/pot",
								"data": {
									"poids": JSON.stringify(poArr)
								}},*/
				"data":dataSet,
				"columns": [
				            {
				            	"className":      'details-control',
				            	"data":           null,
				            	"defaultContent": '',
				            },
				            { "className":      'poids',
				            	"data":           "id"
				            },
				            { "data": "date" },
				            { "data": "name" },
				            { "data": "weight.toLocaleString()" },
				            { "data": "tray.toLocaleString()" },
				            { "data": "amount.toLocaleString()" },
				            { "data": "pendingAmount.toLocaleString()" }
				            ],
				            "order": [[2, 'asc']],
				            "footerCallback" : function(row, data, start, end,
				            		display) {
				            	var api = this.api(), data;

				            	// Remove the formatting to get integer data for summation
				            	var intVal = function(i) {
				            		return typeof i === 'string' ? i.replace(
				            				/[\$,]/g, '') * 1
				            				: typeof i === 'number' ? i : 0;
				            	};

				            	// Total over all pages
				            	total = api.column(7).data().reduce(function(a, b) {
				            		return intVal(a) + intVal(b);
				            	});

				            	// Update footer
				            	$('#fullTotal').html(''+total.toLocaleString('en-IN'));
				            	$('#amtPending').val(total.toLocaleString('en-IN'));
				            }
			});
	addRows();
}

function addRows(){
	var poTable=$('#updatePOTable').DataTable();
	$('#updatePOTable tbody').on('click', 'td.details-control', function () {
		var tr = $(this).closest('tr');
		var row = poTable.row( tr );
		//console.log(row.data());
		if ( row.child.isShown() ) {
			// This row is already open - close it
			row.child.hide();
			tr.removeClass('shown');
		}
		else {
			// Open this row
			row.child( format(row.data()) ).show();
			tr.addClass('shown');
		}
	} );
}

function setAmountWeight() {
	var weight = $('#txtWeight').val();
	var rate = $('#txtRate').val();
	var amount = parseFloat(0.0);
	if (isNumeric(weight)) {
		amount += parseInt(weight);
	}
	if (isNumeric(rate)) {
		amount *= parseFloat(rate);
	}
	if (isNumeric(weight) && isNumeric(rate))
		$('#newPurchaseSubmit').attr('disabled', false);
	$('#txtAmount').val(amount.toLocaleString('en-IN'));
}

function setWastage() {
	var weight = $('#txtWeight').val();
	if (isNumeric(weight)) {
		var wastage = Math.round((0.05 * parseInt(weight))*100)/100;
		$('#txtWastage').val(wastage);
		var net = truncateFloat(parseFloat(weight) - parseFloat(wastage));
		$('#txtNet').val(net.toLocaleString('en-IN'));
		$('#txtTray').val(Math.round(net/18));
	}
}

function calculatePendingAmount() {
	var fullTotal = $('#fullTotal').text();
	var total=parseInt(fullTotal.replace(/,/g,''));
	var amtPaid = $('#amtPaid').val();
	amtPaid=parseInt(amtPaid==''?(0):amtPaid);
	var discountAmt = $('#discountAmt').val();
	discountAmt=parseInt(discountAmt==''?0:discountAmt);

	var totalPending=total-(amtPaid+discountAmt);
	if(totalPending>=0)
		$('#amtPending').val((total-(amtPaid+discountAmt)).toLocaleString());
}

function setPO() {
	initDate("#txtDate");
	$('#newPurchaseSubmit').attr('disabled', true);
	setFarmerList("#txtFarmer",'#farmerStatus',"#farmer_id");
}

function setSearchPO(status) {
	// ajax for all pending transactions
	$.ajax({
		url : "api/purchaseorder/search?"+status,
		type : 'GET',
		contentType : 'application/json',
		dataType : 'json',
		/*error : function(data, status) {
			alert(status+' Some error occured');
		},*/
		success : function(data) {
			//alert(JSON.stringify(data));
			drawSearchPOtable();
			if(data.length!=0)
				updateSearchDataTable(data);
		}
	});
}

function drawSearchPOtable(){
	var tableBoiler='<table id="searchTable" class="display table table-bordered table-striped table-condensed">'+
	'<thead><tr><th><input type="checkbox" id="selectall" /></th><th>Date</th><th>Farmer</th><th>Weight</th>'+
	'<th>Amount</th><th>Pending</th></tr></thead>'+
	'<tfoot><tr><th colspan="5" style="text-align: right">Page Total:</th><th id="pageTotal"></th></tr>'+
	'<tr><th colspan="3" style="text-align: right">Full Total:</th><th colspan="3" id="allTotal"></th></tr></tfoot>'+
	'<tbody><tr><td colspan="6"><p class="bg-info text-center" style="padding: 15px;"><span class="glyphicon glyphicon-hand-up" style="font-size: 1.5em"></span>'+
	'<strong> Please	Please Enter Search String</strong></p></td></tr></tbody>'+
	'</table>';
	$('#poListDiv').html(tableBoiler);
}

function updateSearchDataTable(ajax){
	//var dataSet=[{"id": 1,"date": "2011/04/25","farmer": {"id": "1","name": "Kedar"},"weight": "1000","trays": "10","amount": "5421","pending": "5421","pot":[{"date":"2011/04/25","paid":"4500"},{"date":"2013/08/25","paid":"8500"}]},{"id": 2,"date": "2011/04/25","farmer": {"id": "2","name": "Amol"},"weight": "2000","trays": "20","amount": "55421","pending": "15421","pot":[{"date":"2011/04/25","paid":"4500"},{"date":"2013/08/25","paid":"8500"}]}];
	$('#searchTable').DataTable(
			{
				destroy: true,
				"columnDefs" : [ {
					"targets" : [ 0, 1, 3, 4, 5 ],
					"searchable" : false,
				}, {
					"orderable" : false,
					"targets" : [ 0 ]
				} ],

				"language" : {
					"decimal" : ".",
					"thousands" : ","
				},
				"processing": true,
				"data":ajax,
				"columns": [
				            {
				            	"orderable": false,
				            	'render': function (data, type, full){
				            		return '<input type="checkbox" style="text-align:center" class="case" name="poSelected" value="'+ full["id"] + '">';
				            	}
				            },
				            { "data": "date" },
				            {
				            	'render': function (data, type, full){
				            		return '<a href="javascript:void(0);" class="ttip" data-original-title="x" data-placement="top" data-toggle="tooltip" title="'+full['pendingAdvance'].toLocaleString()+'">'+full['name']+'</a>';
				            	}
				            },
				            { "data": "weight.toLocaleString()"  },
				            { "data": "amount.toLocaleString()" },
				            { "data": "pendingAmount.toLocaleString()"  }
				            ],
				            "order": [[1, 'asc']],
				            "footerCallback" : function(row, data, start, end,
				            		display) {
				            	var api = this.api(), data;

				            	// Remove the formatting to get integer data for summation
				            	var intVal = function(i) {
				            		return typeof i === 'string' ? i.replace(
				            				/[\$,]/g, '') * 1
				            				: typeof i === 'number' ? i : 0;
				            	};

				            	// Total over all pages
				            	total = api.column(5).data().reduce(function(a, b) {
				            		return intVal(a) + intVal(b);
				            	});

				            	// Total over this page
				            	pageTotal = api.column(5, {
				            		page : 'current'
				            	}).data().reduce(function(a, b) {
				            		return intVal(a) + intVal(b);
				            	}, 0);

				            	// Update footer
				            	$('#pageTotal').html('' + pageTotal.toLocaleString('en-IN'));
				            	$('#allTotal').html('' + total.toLocaleString('en-IN'));
				            }
			});

	toggleRow();
	toggleAll();
	$('.ttip').tooltip();
}

function setFarmerList(txtFieldId,statusFieldId,hiddenFieldId) {
	var farmers=[];

	$.ajax({
		url : "api/farmer",
		type : 'GET',
		dataType : 'json',
		/*error : function() {
			alert("farmer Error Occured");
		},*/
		success : function(data) {
			for ( var i = 0; i < data.length; i++) {
				var farmerObj = new Object();
				farmerObj.value = data[i]["id"];
				farmerObj.label = data[i]["name"];
				farmers.push(farmerObj);
			}
		}
	});

	$(txtFieldId).autocomplete({
		source : farmers,
		focus : function(event, ui) {
			//$(this).val(ui.item.label);
			//$("#farmer_id").val(ui.item.value);
			return false;
		},
		select : function(event, ui) {
			$(this).removeClass("farmer-not-exists").addClass("farmer-exists");
			$(this).val(ui.item.label);
			validateInputSuccess(txtFieldId,statusFieldId);
			$(hiddenFieldId).val(ui.item.value);
			return false;
		}
	});

}

function savePO() {
	var status=validateAutcomplete("#txtFarmer",'#farmerStatus');
	//alert(status);
	if(status)
	{
		var purchaseorder={};
		var farmer={};
		farmer.id=$('#farmer_id').val();
		var amount = $('#txtAmount').val();

		purchaseorder.date = $('#txtDate').datepicker('getDate');
		purchaseorder.weight = $('#txtWeight').val();
		purchaseorder.rate = $('#txtRate').val();
		purchaseorder.amount = parseInt(amount.replace(/,/g,''));
		purchaseorder.wastage = $('#txtWastage').val();
		purchaseorder.net = parseInt($('#txtNet').val().replace(/,/g,''));
		purchaseorder.tray = parseInt($('#txtTray').val().replace(/,/g,''));
		purchaseorder.pendingAmount=parseInt(amount.replace(/,/g,''));
		purchaseorder.farmer = farmer;

		//console.log(purchaseorder);
		//return;

		$.ajax({
			url : "api/purchaseorder",
			type : 'POST',
			contentType : 'application/json',
			dataType : 'json',
			data : JSON.stringify(purchaseorder),
			/*error : function(data, status) {
				alert(status+' Some error occured');
			},*/
			success : function(data) {
				var poids= "";
				poids+="poids="+data.id;
				updatePO(poids);
			}
		});
		/* po.push(1);
				po.push(2);
				poArr.po=po; */
	}
	else
	{
		return;
	}

}

function searchPOByOptions(){
	var rVal=$("input[name='inlineRadioOptions']:checked").val();
	var farmerId=$('#search_farmer_id').val();
	var queryString="";
	if(rVal=="pending")
		queryString+="status=pending";
	else if(rVal=="completed")
		queryString+="status=complete";
	else if(rVal=="date"){
		queryString+="from="+$('#searchDate').val()+"&to="+$('#searchDate').val();
	}
	else if(rVal=="range")
		queryString+="from="+$('#from').val()+"&to="+$('#to').val();

	if(farmerId!=""){
		queryString+="&farmerid="+farmerId;
	}

	setSearchPO(queryString);
	//if(rVal=="date")
	//updateSearchDataTable(ajax);
}

function updatePO(poids){
	$.ajax({
		url : "api/purchaseorder/pot?"+poids,
		type : 'GET',
		contentType : 'application/json',
		dataType : 'json',
		/*error : function(data, status) {
			alert(status+' Some error occured');
		},*/
		success : function(data) {
			//alert(JSON.stringify(data));
			drawPOUpdateTable();
			if(data.length!=0)
				updatePODataTable(data);
		}
	});

	$('.potFormElement').prop('disabled', false);
	$('#updatePOMenu')[0].click();
	//$('#updatePOTable tbody').html('');
}

function drawPOUpdateTable(){
	var tableBoiler='<table class="display table table-bordered table-striped table-condensed" id="updatePOTable">'+
	'<thead><tr><th>#</th><th>ID</th><th>Date</th><th>Farmer</th><th>Weight</th><th>Trays</th>'+
	'<th>Amount</th><th>Pending</th></tr></thead>'+
	'<tfoot><tr><th colspan="7" style="text-align: right">Full Total:</th>'+
	'<th id="fullTotal"></th></tr></tfoot>'+
	'<tbody><tr><td colspan="8"><p class="bg-info text-center" style="padding: 15px;">'+
	'<span class="glyphicon glyphicon-hand-up" style="font-size: 1.5em"></span><strong> Please'+
	'Create New P.O. or Search P.O. for updating</strong></p></td></tr></tbody>'+
	'</table>';
	$('#updatePOtableDiv').html(tableBoiler);
}

function searchPOSubmit(flag){
	if($(".case:checked").length>0){

		var poids="";
		var first="";
		var validateFlag=true;
		$(".case:checked").each(function(){
			var currVal=$(this).parent().next().next().text();
			if(first=="")
				first=currVal;
			if(first!=currVal){
				jAlert.info("Selecting different farmers is not allowed for payment");
				validateFlag=false;
			}
			poids+="poids="+$(this).val()+"&";
		});
		if(validateFlag){
			if(flag==0)
				updatePO(poids.slice(0,-1));
			else
				payFromAdvance(poids.slice(0,-1));
		}

	}
	else{
		jAlert.info('Please select Purchase Orders for submitting');
	}
}

function submitPOT(){
	var status=validateNumber("#amtPaid","#paidStatus");
	if(status || $('#chkDiscount').is(':checked')){
		if($('#chkDiscount').is(':checked')){
			//var discTypeStatus=validateAutcomplete("#miscName","#discTypeStatus",misc);
			var discTypeStatus=true;
			var status=validateNumber("#discountAmt","#discAmtStatus");
			if(discTypeStatus && status){
				sendPOT(0);
				//alert("POST successful");
			}
		}
		else{
			sendPOT(1);
			//alert("POST successful");
		}
	}
	else{
		jAlert.info('Please enter amount for submitting');
	}
}

function sendPOT(flag){
	var amtPaid=$('#amtPaid').val();
	amtPaid=amtPaid==''?"0.00":amtPaid;

	var poids="?";
	$('#updatePOTable tbody tr td.poids').each( function(){
		poids+="poids="+$(this).text()+"&";
	});
	poids=poids.slice(0,-1);

	var discountAmt=$('#discountAmt').val();
	discountAmt=discountAmt==""?"0.00":discountAmt;
	var miscStr="";
	if(flag==0){
		var miscid=$('#miscId').val();
		miscStr+="&miscid="+miscid;
	}

	$.ajax({
		url : "api/purchaseordertransaction"+poids+"&discamt="+discountAmt+""+miscStr+"&amt="+amtPaid+"&date="+$('#potDate').val(),
		type : 'POST',
		contentType : 'application/json',
		dataType : 'json',
		async:false,
		//data : {amtpaid:amtPaid,date:$('#potDate').val()},
		/*error : function(data, status) {
			alert(status+" ");
		},*/
		success : function(data) {
			jAlert.success(data.success);
		}
	});
}

function findByOptions(){
	var rVal=$("input[name='editRadioOptions']:checked").val();
	if(rVal=="farmer"){
		drawDeleteFarmerTable();
		delFarmerDataTable();
	}
	else if(rVal=="po"){
		drawDeletePOTable();
		delPODataTable();
	}
	else if(rVal=="pot"){
		drawDeletePOPayTable();
		delPOPayTable();
	}
	else if(rVal=="adv"){
		drawDeletAdvTable();
		delAdvTable();
	}
}

function drawDeleteFarmerTable(){
	var tableBoiler='<table id="delFarmerTable" class="display table table-bordered table-striped table-condensed">'+
	'<thead><tr><th>ID</th><th>Name</th><th>Delete</th></tr></thead>'+
	'<tbody></tbody>'+
	'</table>';
	$('#editListDiv').html(tableBoiler);
}

function drawDeletePOTable(){
	var tableBoiler='<table id="delPOTable" class="display table table-bordered table-striped table-condensed">'+
	'<thead><tr><th>ID</th><th>Date</th><th>Name</th><th>Weight</th><th>Amount</th><th>Pending</th><th>Delete</th></tr></thead>'+
	'<tbody></tbody>'+
	'</table>';
	$('#editListDiv').html(tableBoiler);
}

function drawDeletePOPayTable(){
	var tableBoiler='<table id="delPOPayTable" class="display table table-bordered table-striped table-condensed">'+
	'<thead><tr><th>ID</th><th>Date</th><th>Name</th><th>Paid</th><th>Mode</th><th>Pending</th><th>Total</th><th>Delete</th></tr></thead>'+
	'<tbody></tbody>'+
	'</table>';
	$('#editListDiv').html(tableBoiler);
}

function drawDeletAdvTable(){
	var tableBoiler='<table id="delAdvTable" class="display table table-bordered table-striped table-condensed">'+
	'<thead><tr><th>ID</th><th>Date</th><th>Name</th><th>P. Amt</th><th>Total</th><th>Delete</th></tr></thead>'+
	'<tbody></tbody>'+
	'</table>';
	$('#editListDiv').html(tableBoiler);
}



function delFarmerDataTable(){
	$('#delFarmerTable').DataTable(
			{
				destroy: true,
				responsive: true,
				"columnDefs" : [ {
					"targets" : [ 0, 2],
					"searchable" : false
				}, {
					"orderable" : false,
					"targets" : [2]
				} ],
				"language" : {
					"decimal" : ".",
					"thousands" : ","
				},
				"processing": true,
				"ajax": {
					"url": "api/farmer",
					"dataSrc": ""
				},
				"columns": [
				            { "data": "id" },
				            { "data": "name" },
				            {
				            	'render': function (data, type, full){
				            		return '<button class="btn btn-danger btn-sm btnDel" style="text-align:center" onclick="showDeleteModal(this,\'#delFarmerTable\',\'farmer\')"><span'+
				            		' class="glyphicon glyphicon-trash" aria-hidden="true"></button>';
				            	}
				            }
				            ],
				            "order": [[0, 'desc']]
			});
}

function delPODataTable(){
	$('#delPOTable').DataTable(
			{
				destroy: true,
				responsive: true,
				"columnDefs" : [ {
					"targets" : [ 0,3,4,5,6],
					"searchable" : false
				}, {
					"orderable" : false,
					"targets" : [ 0,3,4,5,6]
				} ],
				"language" : {
					"decimal" : ".",
					"thousands" : ","
				},
				"processing": true,
				"ajax": {
					"url": "api/purchaseorder/search?from="+$('#fromDel').val()+"&to="+$('#toDel').val(),
					"dataSrc": ""
				},
				"columns": [
				            { "data": "id" },
				            { "data": "date" },
				            { "data": "name" },
				            { "data": "weight.toLocaleString()" },
				            { "data": "amount.toLocaleString()" },
				            { "data": "pendingAmount.toLocaleString()" },
				            {
				            	'render': function (data, type, full){
				            		return '<button class="btn btn-danger btn-sm btnDel" style="text-align:center" onclick="showDeleteModal(this,\'#delPOTable\',\'po\')"><span'+
				            		' class="glyphicon glyphicon-trash" aria-hidden="true"></button>';
				            	}
				            }
				            ],
				            "order": [[0, 'desc']]
			});
}

function delPOPayTable(){
	$('#delPOPayTable').DataTable(
			{
				destroy: true,
				responsive: true,
				"columnDefs" : [ {
					"targets" : [ 0,3,4,5,6],
					"searchable" : false
				}, {
					"orderable" : false,
					"targets" : [ 0,3,4,5,6]
				} ],
				"language" : {
					"decimal" : ".",
					"thousands" : ","
				},
				"processing": true,
				"ajax": {
					"url": "api/purchaseordertransaction/search?from="+$('#fromDel').val()+"&to="+$('#toDel').val(),
					"dataSrc": ""
				},
				"columns": [
				            { "data": "id" },
				            { "data": "date" },
				            { "data": "name" },
				            { "data": "paidAmount.toLocaleString()" },
				            { "data": "paymentMethod" },
				            { "data": "pendingAmount.toLocaleString()" },
				            { "data": "totalAmount.toLocaleString()" },
				            {
				            	'render': function (data, type, full){
				            		return '<button class="btn btn-danger btn-sm btnDel" style="text-align:center" onclick="showDeleteModal(this,\'#delPOPayTable\',\'pot\')"><span'+
				            		' class="glyphicon glyphicon-trash" aria-hidden="true"></button>';
				            	}
				            }
				            ],
				            "order": [[0, 'desc']]
			});
}

function delAdvTable(){
	$('#delAdvTable').DataTable(
			{
				destroy: true,
				responsive: true,
				"columnDefs" : [ {
					"targets" : [ 0,1, 3,4,5],
					"searchable" : false
				}, {
					"orderable" : false,
					"targets" : [ 0,3,4,5]
				} ],
				"language" : {
					"decimal" : ".",
					"thousands" : ","
				},
				"processing": true,
				"ajax": {
					"url": "api/advance",
					"dataSrc": ""
				},
				"columns": [
				            { "data": "id" },
				            { "data": "date" },
				            { "data": "name" },
				            { "data": "pendingAmount.toLocaleString()" },
				            { "data": "totalAmount.toLocaleString()" },
				            {
				            	'render': function (data, type, full){
				            		return '<button class="btn btn-danger btn-sm btnDel" style="text-align:center" onclick="showDeleteModal(this,\'#delAdvTable\',\'adv\')"><span'+
				            		' class="glyphicon glyphicon-trash" aria-hidden="true"></button>';
				            	}
				            }
				            ],
				            "order": [[0, 'desc']]
			});
}

function showDeleteModal(ele,tableid,type){
	var table=$(tableid).DataTable();
	$(tableid+' tbody').on('click', '.btnDel', function () {
		var tr = $(ele).closest('tr');
		var rowData = table.row( tr ).data();
		var deleteBoiler='<input type="hidden" id="delId" value="'+rowData['id']+'"/><p class="bg-danger"><b>Are you sure about deleting ?</b></p>';
		$('#deleteModal .modal-body').html(deleteBoiler);
		$('#deleteModal .modal-footer').html('<button type="button" class="btn btn-default"'+
				'data-dismiss="modal">Close</button>'+
				'<button type="button" class="btn btn-danger"'+
				'onclick="submitDelete(\''+type+'\')">Delete</button>');
	});
	$('#deleteModal').modal('toggle');
}

function submitDelete(type){
	var url='';
	if(type=='farmer')
		url="api/farmer/delete/"+$('#delId').val();
	else if(type=='po')
		url="api/purchaseorder/delete/"+$('#delId').val();
	else if(type=='pot')
		url="api/purchaseordertransaction/delete/"+$('#delId').val();
	else if(type=='adv')
		url="api/advance/delete/"+$('#delId').val();

	//purchaseordertransaction/search?from=stringDate&to=stringDate
	$.ajax({
		url : url,
		type : 'POST',
		contentType : 'application/json',
		dataType : 'json',
		async:false,
		//data : {amtpaid:amtPaid,date:$('#potDate').val()},
		/*error : function(data, status) {
			alert(status+" ");
		},*/
		success : function(data) {
			$('#deleteModal').modal('toggle');
			jAlert.info("Deleted Successfully");
			if(type=='farmer')
				delFarmerDataTable();
			else if(type=='po')
				delPODataTable();
			else if(type=='pot')
				delPOPayTable();
			else if(type=='adv')
				delAdvTable();
		}
	});
}

function bindAdvance(){
	$('#advAmt').bind(
			'input propertychange',
			function() {
				var currVal = $(this).val();
				if (isNumeric(currVal)) {
					validateInputSuccess("#advAmt",'#advAmtStatus');
					$('#advStatus').html(parseInt(currVal).toLocaleString());
				} else {
					validateInputError("#advAmt",'#advAmtStatus');
					$('#advStatus').html('');
				}
			});
}

function saveAdvance(){
	var farmer={};
	farmer.id = $("#adv_farmer_id").val();
	var advance = {};
	advance.date = $('#txtAdvDate').datepicker('getDate');
	advance.farmer = farmer;
	advance.totalAmount= parseInt($('#advAmt').val());
	$
	.ajax({
		url : "api/advance",
		type : 'POST',
		contentType : 'application/json',
		dataType : 'json',
		data : JSON.stringify(advance),
		/*error : function(data, status) {
			$('#advStatus').html('<p class=\"bg-danger\" style=\"padding:15px\"> Please Enter Valid Details </p>');
		},*/
		success : function(data) {
			$('#advStatus')
			.html(
			'<p class=\"bg-success\" style=\"padding:15px\"> Advance Added Successfully !</p>');
			$('#advAmt').val('');
		}
	});
}

function searchAcc(){
	//$('#accListDiv').html('<p class=\"bg-danger\" style=\"padding:15px\"><b> Please Check Input Parameters ! </b></p>');
	var farmerId = $('#acc_farmer_id').val();
	var queryString="from="+$('#fromAcc').val()+"&to="+$('#toAcc').val();
	if (farmerId != "") {
		queryString += "&farmerid=" + farmerId;
	}
	$.ajax({
		url : "api/report/farmer?"+queryString,
		type : 'GET',
		contentType : 'application/json',
		dataType : 'json',
		success : function(data) {
			drawPersonalTable(data);
		}
	});
}

function drawPersonalTable(data){
	var poStr='';
	var amtTotal=0, pdgTotal=0;
	for(i=0;i<data['po'].length;i++){
		amtTotal+=data['po'][i]['amount'];
		pdgTotal+=data['po'][i]['pendingAmount'];
		poStr+='<tr><td>'+data['po'][i]['date']+'</td><td>'+data['po'][i]['name']+'</td><td>'+data['po'][i]['weight'].toLocaleString('en-IN')+'</td><td>'+data['po'][i]['tray'].toLocaleString('en-IN')+'</td><td>'+data['po'][i]['amount'].toLocaleString('en-IN')+'</td><td>'+data['po'][i]['pendingAmount'].toLocaleString('en-IN')+'</td></tr>';
	}
	$('#fullPoAmtTotal').html(amtTotal.toLocaleString('en-IN'));
	$('#fullPoPdgTotal').html(pdgTotal.toLocaleString('en-IN'));
	$('#personalPOTable').html(poStr);

	var advStr='';
	amtTotal=0, pdgTotal=0;
	for(i=0;i<data['adv'].length;i++){
		amtTotal+=data['adv'][i]['totalAmount'];
		pdgTotal+=data['adv'][i]['pendingAmount'];
		advStr+='<tr><td>'+data['adv'][i]['date']+'</td><td>'+data['adv'][i]['name']+'</td><td>'+data['adv'][i]['totalAmount'].toLocaleString('en-IN')+'</td><td>'+data['adv'][i]['pendingAmount'].toLocaleString('en-IN')+'</td></tr>';
	}
	$('#fullAdvAmtTotal').html(amtTotal.toLocaleString('en-IN'));
	$('#fullAdvPdgTotal').html(pdgTotal.toLocaleString('en-IN'));
	$('#personalAdvTable').html(advStr);

	var payStr='';
	amtTotal=0;
	for(i=0;i<data['pot'].length;i++){
		amtTotal+=data['pot'][i]['paidAmount'];
		payStr+='<tr><td>'+data['pot'][i]['date']+'</td><td>'+data['pot'][i]['name']+'</td><td>'+data['pot'][i]['paidAmount'].toLocaleString('en-IN')+'</td><td>'+data['pot'][i]['paymentMethod']+'</td></tr>';
	}
	$('#fullPayAmtTotal').html(amtTotal.toLocaleString('en-IN'));
	$('#personalPayTable').html(payStr);
}

function payFromAdvance(poids){
	$.ajax({
		url : "api/purchaseorder/advance?"+poids,
		type : 'POST',
		contentType : 'application/json',
		dataType : 'json',
		/*error : function(data, status) {
			alert(status+' Pay From Advance Error');
		},*/
		success : function(data) {
			updatePO(poids);
		}
	});
}