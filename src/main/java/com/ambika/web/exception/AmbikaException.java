package com.ambika.web.exception;

public class AmbikaException extends Exception {

	private static final long serialVersionUID = 902439219291160194L;
	private String msg;

	public AmbikaException() {
		super();
	}

	public AmbikaException(String msg) {
		super(msg);
		this.msg = msg;
	}

	public AmbikaException(Throwable cause) {
		super(cause);
	}

	@Override
	public String toString() {
		return this.msg;
	}

	@Override
	public String getMessage() {
		return this.msg;
	}

}
