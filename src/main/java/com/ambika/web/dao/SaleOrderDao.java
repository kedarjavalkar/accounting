package com.ambika.web.dao;

import io.dropwizard.hibernate.AbstractDAO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.ambika.web.core.Buyer;
import com.ambika.web.core.SaleOrder;

public class SaleOrderDao extends AbstractDAO<SaleOrder>{

	SessionFactory sessionFactory;

	public SaleOrderDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		this.sessionFactory = sessionFactory;
	}

	public SaleOrder save(SaleOrder saleOrder) {
		return persist(saleOrder);
	}

	public SaleOrder getById(Long id) {
		return get(id);
	}

	public List<SaleOrder> findAll() {
		try {
			Query namedQuery = namedQuery("com.ambika.web.core.entity.SaleOrder.findAll");
			return list(namedQuery);
		} catch(NoResultException nrex) {
			return new ArrayList<SaleOrder>();
		}
	}

	public List<SaleOrder> search(Buyer buyerObj, String status, Date from, Date to) {
		Criteria criteria = criteria();

		criteria.add(Restrictions.eq("isDelete", false));

		if(buyerObj != null)
			criteria.add(Restrictions.eq("buyer", buyerObj));

		if(status != null) {
			if(status.equalsIgnoreCase("pending")){
				criteria.add(Restrictions.eq("complete", false));
			} else if(status.equalsIgnoreCase("complete")){
				criteria.add(Restrictions.eq("complete", true));
			}
		} 

		if(from != null && to != null)
			criteria.add(Restrictions.between("date", from, to));
		else if(from != null && to == null)
			criteria.add(Restrictions.eq("date", from));

		return list(criteria);
	}

}
