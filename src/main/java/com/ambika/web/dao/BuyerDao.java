package com.ambika.web.dao;

import io.dropwizard.hibernate.AbstractDAO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Query;
import org.hibernate.SessionFactory;

import com.ambika.web.core.Buyer;

public class BuyerDao extends AbstractDAO<Buyer>{

	public BuyerDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public Buyer save(Buyer buyer) {
		return persist(buyer);
	}

	public List<Buyer> findAll() {
		try {
			Query namedQuery = namedQuery("com.ambika.web.core.entity.Buyer.findAll");
			return list(namedQuery);
		} catch(NoResultException nrex) {
			return new ArrayList<Buyer>();
		}
	}

	public Buyer getById(Long id) {
		return get(id);
	}


}
