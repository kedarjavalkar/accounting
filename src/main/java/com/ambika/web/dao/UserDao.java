package com.ambika.web.dao;

import io.dropwizard.hibernate.AbstractDAO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.ambika.web.core.User;

public class UserDao extends AbstractDAO<User>{

	public UserDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public User save(User user) {
		return persist(user);
	}

	public List<User> findAll() {
		try {
			Query namedQuery = namedQuery("com.ambika.web.core.entity.User.findAll");
			return list(namedQuery);
		} catch(NoResultException nrex) {
			return new ArrayList<User>();
		}
	}

	public User getById(Long id) {
		return get(id);
	}

	public User findByEmail(String email) {
		// Do NOT filter is_delete., Used to check duplicate Email id
		Criteria criteria = criteria();
		criteria.add(Restrictions.eq("email", email));
		return uniqueResult(criteria);
	}

}
