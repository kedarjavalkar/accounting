package com.ambika.web.dao;

import io.dropwizard.hibernate.AbstractDAO;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.ambika.web.core.BalanceSheet;

public class BalanceSheetDao extends AbstractDAO<BalanceSheet>{

	public BalanceSheetDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public BalanceSheet save(BalanceSheet balanceSheet) {
		return persist(balanceSheet);
	}

	public BalanceSheet getById(Long id) {
		return get(id);
	}

	public List<BalanceSheet> findAll() {
		Criteria criteria = criteria()
				.addOrder(Order.desc("date"));
		return list(criteria);
	}

	public BalanceSheet findByDate(Date date) {
		Criteria criteria = criteria()
				.add(Restrictions.eq("date", date));
		return uniqueResult(criteria);
	}

	public List<BalanceSheet> findByDate(Date from, Date to) {
		Criteria criteria = criteria()
				.add(Restrictions.between("date", from, to))
				.addOrder(Order.desc("date"));
		return list(criteria);
	}

	public BalanceSheet getOldestBS() {
		Criteria criteria = criteria()
				.addOrder(Order.asc("date"))
				.setMaxResults(1);
		return uniqueResult(criteria);
	}

}
