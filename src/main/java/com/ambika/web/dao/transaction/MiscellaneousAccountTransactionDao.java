package com.ambika.web.dao.transaction;

import io.dropwizard.hibernate.AbstractDAO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Query;
import org.hibernate.SessionFactory;

import com.ambika.web.core.transaction.MiscellaneousAccountTransaction;

public class MiscellaneousAccountTransactionDao extends AbstractDAO<MiscellaneousAccountTransaction>{

	public MiscellaneousAccountTransactionDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public MiscellaneousAccountTransaction save(MiscellaneousAccountTransaction miscellaneousAccountTransaction) {
		return persist(miscellaneousAccountTransaction);
	}

	public List<MiscellaneousAccountTransaction> findAll() {
		try {
			Query namedQuery = namedQuery("com.ambika.web.core.entity.MiscellaneousAccountTransaction.findAll");
			return list(namedQuery);
		} catch(NoResultException nrex) {
			return new ArrayList<MiscellaneousAccountTransaction>();
		}
	}

	public MiscellaneousAccountTransaction getById(Long id) {
		return get(id);
	}

}
