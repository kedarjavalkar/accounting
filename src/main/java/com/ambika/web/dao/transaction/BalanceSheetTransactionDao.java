package com.ambika.web.dao.transaction;

import io.dropwizard.hibernate.AbstractDAO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.ambika.web.core.BalanceSheet;
import com.ambika.web.core.transaction.BalanceSheetTransaction;
import com.ambika.web.core.transaction.MiscellaneousAccountTransaction;
import com.ambika.web.core.transaction.PurchaseOrderTransaction;
import com.ambika.web.core.transaction.SaleOrderTransaction;

public class BalanceSheetTransactionDao extends AbstractDAO<BalanceSheetTransaction>{

	public BalanceSheetTransactionDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public BalanceSheetTransaction getById(Long id) {
		return get(id);
	}

	public BalanceSheetTransaction save(BalanceSheetTransaction balanceSheetTransaction) {
		return persist(balanceSheetTransaction);
	}

	public List<BalanceSheetTransaction> findAll() {
		try {
			Query namedQuery = namedQuery("com.ambika.web.core.entity.BalanceSheetTransaction.findAll");
			return list(namedQuery);
		} catch(NoResultException nrex) {
			return new ArrayList<BalanceSheetTransaction>();
		}
	}

	public List<BalanceSheetTransaction> findByBs(BalanceSheet balanceSheet) {
		Criteria criteria = criteria()
				.add(Restrictions.eq("balanceSheet", balanceSheet));

		return list(criteria);
	}
	
	public List<BalanceSheetTransaction> findByPot(PurchaseOrderTransaction purchaseOrderTransaction) {
		Criteria criteria = criteria()
				.add(Restrictions.eq("isDelete", false))
				.add(Restrictions.eq("purchaseOrderTransaction", purchaseOrderTransaction));

		return list(criteria);
	}

	public List<BalanceSheetTransaction> findBySot(SaleOrderTransaction saleOrderTransaction) {
		Criteria criteria = criteria()
				.add(Restrictions.eq("isDelete", false))
				.add(Restrictions.eq("saleOrderTransaction", saleOrderTransaction));

		return list(criteria);
	}
	
	public List<BalanceSheetTransaction> findByMat(MiscellaneousAccountTransaction miscellaneousAccountTransaction) {
		Criteria criteria = criteria()
				.add(Restrictions.eq("isDelete", false))
				.add(Restrictions.eq("miscellaneousAccountTransaction", miscellaneousAccountTransaction));

		return list(criteria);
	}

}
