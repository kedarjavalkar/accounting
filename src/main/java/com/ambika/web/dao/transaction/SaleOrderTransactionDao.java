package com.ambika.web.dao.transaction;

import io.dropwizard.hibernate.AbstractDAO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.ambika.web.core.SaleOrder;
import com.ambika.web.core.transaction.SaleOrderTransaction;

public class SaleOrderTransactionDao extends AbstractDAO<SaleOrderTransaction>{

	public SaleOrderTransactionDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public SaleOrderTransaction getById(Long id) {
		return get(id);
	}

	public SaleOrderTransaction save(SaleOrderTransaction saleOrderTransaction) {
		return persist(saleOrderTransaction);
	}

	public List<SaleOrderTransaction> findAll() {
		try {
			Query namedQuery = namedQuery("com.ambika.web.core.entity.SaleOrderTransaction.findAll");
			return list(namedQuery);
		} catch(NoResultException nrex) {
			return new ArrayList<SaleOrderTransaction>();
		}
	}

	public List<SaleOrderTransaction> search(List<SaleOrder> solist, Date fromDate, Date toDate) {
		Criteria criteria = criteria();
		criteria.add(Restrictions.eq("isDelete", false));

		if(solist != null)
			criteria.add(Restrictions.in("saleOrder", solist));
		if(fromDate != null)
			criteria.add(Restrictions.ge("date", fromDate));
		if(toDate != null)
			criteria.add(Restrictions.le("date", toDate));

		return list(criteria);
	}
}
