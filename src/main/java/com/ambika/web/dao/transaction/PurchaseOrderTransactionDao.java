package com.ambika.web.dao.transaction;

import io.dropwizard.hibernate.AbstractDAO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.ambika.web.core.PurchaseOrder;
import com.ambika.web.core.transaction.PurchaseOrderTransaction;

public class PurchaseOrderTransactionDao extends AbstractDAO<PurchaseOrderTransaction>{

	public PurchaseOrderTransactionDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public PurchaseOrderTransaction getById(Long id) {
		return get(id);
	}

	public PurchaseOrderTransaction save(PurchaseOrderTransaction purchaseOrderTransaction) {
		return persist(purchaseOrderTransaction);
	}

	public List<PurchaseOrderTransaction> findAll() {
		try {
			Query namedQuery = namedQuery("com.ambika.web.core.entity.PurchaseOrderTransaction.findAll");
			return list(namedQuery);
		} catch(NoResultException nrex) {
			return new ArrayList<PurchaseOrderTransaction>();
		}
	}

	public List<PurchaseOrderTransaction> search(List<PurchaseOrder> poList, Date fromDate, Date toDate) {
		Criteria criteria = criteria();
		criteria.add(Restrictions.eq("isDelete", false));

		if(poList != null && !poList.isEmpty())
			criteria.add(Restrictions.in("purchaseOrder", poList));
		if(fromDate != null)
			criteria.add(Restrictions.ge("date", fromDate));
		if(toDate != null)
			criteria.add(Restrictions.le("date", toDate));

		return list(criteria);
	}
}
