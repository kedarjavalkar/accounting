package com.ambika.web.dao;

import io.dropwizard.hibernate.AbstractDAO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.ambika.web.core.Advance;
import com.ambika.web.core.Farmer;

public class AdvanceDao extends AbstractDAO<Advance>{

	public AdvanceDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public Advance getById(Long id) {
		return get(id);
	}

	public Advance save(Advance advance) {
		return persist(advance);
	}

	public List<Advance> findAll() {
		try {
			Query namedQuery = namedQuery("com.ambika.web.core.entity.Advance.findAll");
			return list(namedQuery);
		} catch(NoResultException nrex) {
			return new ArrayList<Advance>();
		}
	}

	public List<Advance> findByType(String type) {
		Criteria criteria = criteria();
		criteria.add(Restrictions.eq("isDelete", false));
		if(type.equalsIgnoreCase("farmer"))
			criteria.add(Restrictions.isNotNull("farmer"));
		if(type.equalsIgnoreCase("buyer"))
			criteria.add(Restrictions.isNotNull("buyer"));

		return list(criteria);
	}

	public List<Advance> search(Farmer farmer, Date from, Date to) {
		Criteria criteria = criteria();
		criteria.add(Restrictions.eq("isDelete", false));

		if(farmer != null)
			criteria.add(Restrictions.eq("farmer", farmer));
		if(from != null)
			criteria.add(Restrictions.ge("date", from));
		if(to != null)
			criteria.add(Restrictions.le("date", to));

		return list(criteria);
	}

}
