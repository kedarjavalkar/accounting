package com.ambika.web.dao;

import io.dropwizard.hibernate.AbstractDAO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Query;
import org.hibernate.SessionFactory;

import com.ambika.web.core.transaction.AdvanceTransaction;

public class AdvanceTransactionDao extends AbstractDAO<AdvanceTransaction>{

	public AdvanceTransactionDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public AdvanceTransaction save(AdvanceTransaction advanceTransaction) {
		return persist(advanceTransaction);
	}

	public List<AdvanceTransaction> findAll() {
		try {
			Query namedQuery = namedQuery("com.ambika.web.core.entity.AdvanceTransaction.findAll");
			return list(namedQuery);
		} catch(NoResultException nrex) {
			return new ArrayList<AdvanceTransaction>();
		}
	}

	public AdvanceTransaction getById(Long id) {
		return get(id);
	}
	

}
