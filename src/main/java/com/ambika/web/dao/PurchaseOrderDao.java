package com.ambika.web.dao;

import io.dropwizard.hibernate.AbstractDAO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.ambika.web.core.Farmer;
import com.ambika.web.core.PurchaseOrder;

public class PurchaseOrderDao extends AbstractDAO<PurchaseOrder>{

	SessionFactory sessionFactory;

	public PurchaseOrderDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		this.sessionFactory = sessionFactory;
	}

	public PurchaseOrder save(PurchaseOrder purchaseOrder) {
		return persist(purchaseOrder);
	}

	public PurchaseOrder getById(Long id) {
		return get(id);
	}

	public List<PurchaseOrder> findAll() {
		try {
			Query namedQuery = namedQuery("com.ambika.web.core.entity.PurchaseOrder.findAll");
			return list(namedQuery);
		} catch(NoResultException nrex) {
			return new ArrayList<PurchaseOrder>();
		}
	}

	public List<PurchaseOrder> search(Farmer farmerObj, String status, Date from, Date to) {
		Criteria criteria = criteria();

		criteria.add(Restrictions.eq("isDelete", false));
		
		if(farmerObj != null)
			criteria.add(Restrictions.eq("farmer", farmerObj));

		if(status != null) {
			if(status.equalsIgnoreCase("pending")){
				criteria.add(Restrictions.eq("complete", false));
			} else if(status.equalsIgnoreCase("complete")){
				criteria.add(Restrictions.eq("complete", true));
			}
		} 

		if(from != null && to != null)
			criteria.add(Restrictions.between("date", from, to));
		else if(from != null && to == null)
			criteria.add(Restrictions.eq("date", from));

		return list(criteria);
	}

}
