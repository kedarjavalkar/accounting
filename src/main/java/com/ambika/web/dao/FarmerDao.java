package com.ambika.web.dao;

import io.dropwizard.hibernate.AbstractDAO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Query;
import org.hibernate.SessionFactory;

import com.ambika.web.core.Farmer;

public class FarmerDao extends AbstractDAO<Farmer>{

	public FarmerDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public Farmer save(Farmer farmer) {
		return persist(farmer);
	}

	public List<Farmer> findAll() {
		try {
			Query namedQuery = namedQuery("com.ambika.web.core.entity.Farmer.findAll");
			return list(namedQuery);
		} catch(NoResultException nrex) {
			return new ArrayList<Farmer>();
		}
	}

	public Farmer getById(Long id) {
		return get(id);
	}

}
