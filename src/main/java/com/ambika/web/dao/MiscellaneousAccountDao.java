package com.ambika.web.dao;

import io.dropwizard.hibernate.AbstractDAO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.ambika.web.core.MiscellaneousAccount;

public class MiscellaneousAccountDao extends AbstractDAO<MiscellaneousAccount>{

	public MiscellaneousAccountDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public MiscellaneousAccount save(MiscellaneousAccount miscellaneousAccount) {
		return persist(miscellaneousAccount);
	}

	public List<MiscellaneousAccount> findAll() {
		try {
			Query namedQuery = namedQuery("com.ambika.web.core.entity.MiscellaneousAccount.findAll");
			return list(namedQuery);
		} catch(NoResultException nrex) {
			return new ArrayList<MiscellaneousAccount>();
		}
	}

	public MiscellaneousAccount getById(Long id) {
		return get(id);
	}

	public List<MiscellaneousAccount> findByType(boolean type) {
		Criteria criteria = criteria();
		criteria.add(Restrictions.eq("isDelete", false));
		criteria.add(Restrictions.eq("type", type));
		return list(criteria);
	}


}
