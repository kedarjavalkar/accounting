package com.ambika.web.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.ambika.web.utils.async.LoggerMail;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class JsonUtils {
	private static final Logger logger = Logger.getLogger((JsonUtils.class).getName());

	public static String getJson(Object object) {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new Hibernate4Module()).setSerializationInclusion(Include.NON_NULL);
		objectMapper.configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, false);
		objectMapper.setVisibility(PropertyAccessor.ALL, Visibility.ANY);

		String json = null;
		try {
			json = objectMapper.writeValueAsString(object);
		} catch (JsonProcessingException jpex) {
			LoggerMail.instance().send(jpex);
			logger.log(Level.WARNING, "Faliled to parse object to json string. Oops: " + jpex, logger);
		}
		return json;
	}

	public static String getErrorJson(String message) {
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("warning", message);
		return new Gson().toJson(jsonObject);
	}

	public static String getSuccessJson(String message) {
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("success", message);
		return new Gson().toJson(jsonObject);
	}

}
