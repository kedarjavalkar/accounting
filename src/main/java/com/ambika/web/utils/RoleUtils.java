package com.ambika.web.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.ambika.web.auth.AuthUser;
import com.ambika.web.core.User;
import com.ambika.web.utils.async.LoggerMail;

public class RoleUtils {
	private static final Logger logger = Logger.getLogger((RoleUtils.class).getName());
	private static final String ADMIN = User.Role.ADMIN;
	//private static final String USER = User.Role.USER;

	public static String validate(AuthUser auth, String str) {
		try {
			if(auth.getData().equals(ADMIN))
				return str;
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.WARNING, "Failed to read AuthUser data...");
		}
		return "-";
	}

}
