package com.ambika.web.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateUtils {

	private final static String PATTERN = "dd MMM yyyy";
	private final static SimpleDateFormat FORMATTER = new SimpleDateFormat(PATTERN);

	public static String format(Date date) {
		return FORMATTER.format(date);
	}

	public static Date format(String date) throws ParseException {
		return FORMATTER.parse(date);
	}

	public static Date setInitialTime(Date date) throws ParseException {
		return format(format(date));
	}

	public static Date getMidnight() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_YEAR, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 1);
		cal.set(Calendar.SECOND, 0);
		return cal.getTime();
	}

	public static Date getYesterday(Date date) throws ParseException {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_YEAR, -1);
		return setInitialTime(cal.getTime());
	}

	public static List<Date> getDateRange(Date from, Date to) throws ParseException {
		List<Date> dates = new ArrayList<Date>();
		Calendar cal = Calendar.getInstance();
		cal.setTime(from);
		dates.add(setInitialTime(cal.getTime()));
		while (cal.getTime().before(to)) {
			cal.add(Calendar.DATE, 1);
			dates.add(setInitialTime(cal.getTime()));
		}
		return dates;
	}

}
