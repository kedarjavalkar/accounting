package com.ambika.web.utils;

import org.hibernate.SessionFactory;

import com.ambika.web.dao.AdvanceDao;
import com.ambika.web.dao.AdvanceTransactionDao;
import com.ambika.web.dao.BalanceSheetDao;
import com.ambika.web.dao.BuyerDao;
import com.ambika.web.dao.FarmerDao;
import com.ambika.web.dao.MiscellaneousAccountDao;
import com.ambika.web.dao.PurchaseOrderDao;
import com.ambika.web.dao.SaleOrderDao;
import com.ambika.web.dao.UserDao;
import com.ambika.web.dao.transaction.BalanceSheetTransactionDao;
import com.ambika.web.dao.transaction.MiscellaneousAccountTransactionDao;
import com.ambika.web.dao.transaction.PurchaseOrderTransactionDao;
import com.ambika.web.dao.transaction.SaleOrderTransactionDao;

public class Dao {

	private UserDao userDao;

	private FarmerDao farmerDao;
	private PurchaseOrderDao purchaseOrderDao;
	private PurchaseOrderTransactionDao purchaseOrderTransactionDao;

	private BalanceSheetDao balanceSheetDao;
	private BalanceSheetTransactionDao balanceSheetTransactionDao;

	private MiscellaneousAccountDao miscellaneousAccountDao;
	private MiscellaneousAccountTransactionDao miscellaneousAccountTransactionDao;

	private BuyerDao buyerDao;
	private SaleOrderDao saleOrderDao;
	private SaleOrderTransactionDao saleOrderTransactionDao;

	private AdvanceDao advanceDao;
	private AdvanceTransactionDao advanceTransactionDao;

	public Dao(SessionFactory sessionFactory) {
		this.userDao = new UserDao(sessionFactory);

		this.farmerDao = new FarmerDao(sessionFactory);
		this.purchaseOrderDao = new PurchaseOrderDao(sessionFactory);
		this.purchaseOrderTransactionDao = new PurchaseOrderTransactionDao(sessionFactory);

		this.balanceSheetDao = new BalanceSheetDao(sessionFactory);
		this.balanceSheetTransactionDao = new BalanceSheetTransactionDao(sessionFactory);

		this.miscellaneousAccountDao = new MiscellaneousAccountDao(sessionFactory);
		this.miscellaneousAccountTransactionDao = new MiscellaneousAccountTransactionDao(sessionFactory);

		this.buyerDao = new BuyerDao(sessionFactory);
		this.saleOrderDao = new SaleOrderDao(sessionFactory);
		this.saleOrderTransactionDao = new SaleOrderTransactionDao(sessionFactory);

		this.advanceDao = new AdvanceDao(sessionFactory);
		this.advanceTransactionDao = new AdvanceTransactionDao(sessionFactory);

	}

	public UserDao getUserDao() {
		return userDao;
	}

	public FarmerDao getFarmerDao() {
		return farmerDao;
	}

	public PurchaseOrderDao getPurchaseOrderDao() {
		return purchaseOrderDao;
	}

	public PurchaseOrderTransactionDao getPurchaseOrderTransactionDao() {
		return purchaseOrderTransactionDao;
	}

	public BalanceSheetDao getBalanceSheetDao() {
		return balanceSheetDao;
	}

	public BalanceSheetTransactionDao getBalanceSheetTransactionDao() {
		return balanceSheetTransactionDao;
	}

	public MiscellaneousAccountDao getMiscellaneousAccountDao() {
		return miscellaneousAccountDao;
	}

	public MiscellaneousAccountTransactionDao getMiscellaneousAccountTransactionDao() {
		return miscellaneousAccountTransactionDao;
	}

	public BuyerDao getBuyerDao() {
		return buyerDao;
	}

	public SaleOrderDao getSaleOrderDao() {
		return saleOrderDao;
	}

	public SaleOrderTransactionDao getSaleOrderTransactionDao() {
		return saleOrderTransactionDao;
	}

	public AdvanceDao getAdvanceDao() {
		return advanceDao;
	}

	public AdvanceTransactionDao getAdvanceTransactionDao() {
		return advanceTransactionDao;
	}
}
