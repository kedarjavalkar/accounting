package com.ambika.web.utils;

public class ApplicationContext {
	
	private String mailUser;
	private String mailPassword;
	private String cookieName;
	private Integer cookieAge;
	
	private static ApplicationContext instance = new ApplicationContext();
	
	private ApplicationContext() {
		
	}
	
	public static ApplicationContext instance() {
		return instance;
	}
	
	public void setup(String user, String password, String cookieName, Integer cookieAge) {
		this.mailUser = user;
		this.mailPassword = password;
		this.cookieName = cookieName;
		this.cookieAge = cookieAge;
	}

	public String getMailUser() {
		return mailUser;
	}

	public String getMailPassword() {
		return mailPassword;
	}
	
	public String getCookieName() {
		return cookieName;
	}
	
	public Integer getCookieAge() {
		return cookieAge;
	}

}
