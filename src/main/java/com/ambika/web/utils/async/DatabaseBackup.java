package com.ambika.web.utils.async;

import java.io.File;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.ambika.web.utils.ApplicationContext;
import com.ambika.web.utils.DateUtils;

public class DatabaseBackup {
	private static final Logger logger = Logger.getLogger(DatabaseBackup.class.getName());

	public void backup(String content) throws Exception {
		String fileName = "ambika.sql";

		logger.info("Backing up database...");
		String batchCommand = System.getenv("OPENSHIFT_DATA_DIR") + "db_backup/backup.sh";
		Process proc = Runtime.getRuntime().exec(batchCommand);
		proc.waitFor();
		logger.info("Backing up database....  Done");

		final String USER = ApplicationContext.instance().getMailUser();
		final String PASSWORD = ApplicationContext.instance().getMailPassword();
		final String SEND_TO = USER;
		logger.info("Preparing mail...");

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(USER, PASSWORD);
			}
		});

		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(USER));
		message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(SEND_TO));
		message.setSubject("[Ambika] " + DateUtils.format(new Date()));
		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setText(content);
		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);

		// Attachment
		logger.info("Attaching file...");
		messageBodyPart = new MimeBodyPart();
		File file = new File(System.getenv("OPENSHIFT_DATA_DIR") + "db_backup/" + fileName);
		DataSource source = new FileDataSource(file);
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(fileName);
		multipart.addBodyPart(messageBodyPart);
		message.setContent(multipart);

		logger.info("Sending mail...");
		Transport.send(message);

		file.delete();
		logger.info("[Info] Database Backup mail sent...");
	}

}
