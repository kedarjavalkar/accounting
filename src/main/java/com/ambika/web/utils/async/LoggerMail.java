package com.ambika.web.utils.async;

import java.util.Date;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.ambika.web.utils.ApplicationContext;
import com.ambika.web.utils.DateUtils;

public class LoggerMail {
	private static final Logger logger = Logger.getLogger(LoggerMail.class.getName());
	private static final String USER = ApplicationContext.instance().getMailUser();
	private static final String PASSWORD = ApplicationContext.instance().getMailPassword();
	private static final String SEND_TO = USER;
	private static Session session = null;
	private static Message message = null;
	private static LoggerMail instance = new LoggerMail();

	public static LoggerMail instance() {
		return instance;
	}

	private LoggerMail() {
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(USER, PASSWORD);
			}
		});

		message = new MimeMessage(session);
	}


	private void sendUsingThread() {
		Runnable r = new Runnable() {
			public void run() {
				try {
					Transport.send(message);
				} catch (Exception e) {
					logger.log(Level.SEVERE, "Failed to send Logger Mail using thread. Msg: " + e.toString());
				}
			}
		};

		ExecutorService executor = Executors.newCachedThreadPool();
		executor.submit(r);
	}

	public void send(Exception ex) {
		if(USER == null) { 
			System.err.println(ExceptionUtils.getStackTrace(ex));
			return;
		}
		String subject = ex.getCause() == null ? subject = ex.getMessage() : ex.getCause().getMessage();
		try {
			message.setFrom(new InternetAddress(USER));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(SEND_TO));
			message.setSubject("[Ambika] " + subject + " :: " + DateUtils.format(new Date()));
			message.setText(ExceptionUtils.getStackTrace(ex));
			sendUsingThread(); //Transport.send(message);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Failed to send Logger Mail. Msg: " + e.toString());
		}
	}

}
