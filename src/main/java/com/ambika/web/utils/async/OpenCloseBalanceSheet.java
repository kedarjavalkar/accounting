package com.ambika.web.utils.async;

import java.io.IOException;
import java.util.Date;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.ambika.web.utils.ApplicationContext;

public class OpenCloseBalanceSheet extends TimerTask {
	private final static Logger logger = Logger.getLogger((OpenCloseBalanceSheet.class).getName());
	private String url;

	public OpenCloseBalanceSheet(String url) {
		this.url = url;
		run();
	}

	@Override
	public void run() {
		String content = "";
		CloseableHttpClient client = null;
		try {
			client = HttpClients.createDefault();
			HttpGet httpGet = new HttpGet(url+"/api/balancesheet/openclose?from=oldest");
			CloseableHttpResponse response = client.execute(httpGet);
			if(response.getStatusLine().getStatusCode() == 200){
				logger.log(Level.INFO, "Balance sheet created successfuly: " + new Date());
				content = url+"/api/balancesheet";
			} else {
				logger.log(Level.SEVERE, "Oops. Failed to open close balance sheet... Msg: " + response.getStatusLine());
				content = "Failed to create balanceSheet :: " + System.lineSeparator() + EntityUtils.toString(response.getEntity());
				new Mail("[Severe] Ambika", "Failed to create balanceSheet :: " + content).send();
			}
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Oops. Failed to open close balance sheet... Msg: " + ex.getMessage());
		} finally {
			try {
				client.close();
			} catch(IOException ioex) { }
		}

		try {
			if(ApplicationContext.instance().getMailUser() != null) {
				new DatabaseBackup().backup(content);
			}
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
		}
	}

}
