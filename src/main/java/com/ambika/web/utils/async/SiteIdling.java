package com.ambika.web.utils.async;

import java.io.IOException;
import java.util.Date;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class SiteIdling extends TimerTask {
	private static final Logger logger = Logger.getLogger((SiteIdling.class).getName());
	private String url;

	public SiteIdling(String url) {
		this.url = url;
	}

	@Override
	public void run() {
		// Called every X hours to prevent site idling
		CloseableHttpClient client = HttpClients.createDefault();
		try {
			HttpGet httpGet = new HttpGet(url+"/admin/ping");
			CloseableHttpResponse response = client.execute(httpGet);
			if(response.getStatusLine().getStatusCode() == 200){
				logger.log(Level.INFO, "Heartbeated on : " + new Date(), logger);
			} else {
				logger.log(Level.SEVERE, "Oops. Failed to prevent site idling... Msg: " + response.getStatusLine(), logger);
			}
		} catch(Exception ex) {
			logger.log(Level.SEVERE, "Oops. Failed to prevent site idling..." + ex.getMessage(), logger);
		} finally {
			try {
				client.close();
			} catch(IOException ioex) { }
		}
	}
}
