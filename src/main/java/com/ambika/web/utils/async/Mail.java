package com.ambika.web.utils.async;

import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.ambika.web.utils.ApplicationContext;

public class Mail {
	private final static Logger logger = Logger.getLogger(Mail.class.getName());
	private final static String USER = ApplicationContext.instance().getMailUser();
	private final static String PASSWORD = ApplicationContext.instance().getMailPassword();
	private final static String SEND_TO = USER;

	private Session session = null;
	private Message message = null;

	private String subject;
	private String content;

	public Mail(String subject, String content) {
		this.subject = subject;
		this.content = content;

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(USER, PASSWORD);
			}
		});

		message = new MimeMessage(session);
	}

	private void sendUsingThread() {
		Runnable r = new Runnable() {
			public void run() {
				try {
					Transport.send(message);
				} catch (Exception e) {
					logger.log(Level.SEVERE, "Failed to send mail using thread. Msg: " + e.toString());
				}
			}
		};

		ExecutorService executor = Executors.newCachedThreadPool();
		executor.submit(r);
	}

	public void send() {
		if(USER == null) return;
		try {
			message.setFrom(new InternetAddress(USER));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(SEND_TO));
			message.setSubject(subject);
			message.setText(content);
			sendUsingThread(); //Transport.send(message);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Failed to send mail. Msg: " + e.toString());
		}
	}

}
