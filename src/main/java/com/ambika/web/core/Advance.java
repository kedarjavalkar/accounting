package com.ambika.web.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Digits;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.ambika.web.core.transaction.AdvanceTransaction;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="advance")
@NamedNativeQueries({
	@NamedNativeQuery(name = "com.ambika.web.core.entity.Advance.findAll", 
			query = "SELECT * FROM advance where is_delete = 0", resultClass = Advance.class)
})
public class Advance implements Serializable {
	private static final long serialVersionUID = 2193306589298995786L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "date")
	private Date date;

	@Digits(integer=10, fraction=2)
	@Column(name = "total_amount")
	private BigDecimal totalAmount;

	@Digits(integer=10, fraction=2)
	@Column(name = "pending_amount")
	private BigDecimal pendingAmount;

	@Column(name = "complete")
	private boolean complete;

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade({ CascadeType.SAVE_UPDATE })
	@JoinColumn(name="farmer_id", nullable = true)
	@JsonBackReference(value = "farmerAdvance")
	private Farmer farmer;

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade({ CascadeType.SAVE_UPDATE })
	@JoinColumn(name="buyer_id", nullable = true)
	@JsonBackReference(value = "buyerAdvance")
	private Buyer buyer;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "advance")
	@JsonManagedReference(value = "advanceAdvanceTransaction")
	private List<AdvanceTransaction> advanceTransaction = new ArrayList<AdvanceTransaction>();

	@Column(name = "is_delete")
	private boolean isDelete;

	@Column(name = "delete_date")
	private Date deleteDate;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getPendingAmount() {
		return pendingAmount;
	}

	public void setPendingAmount(BigDecimal pendingAmount) {
		this.pendingAmount = pendingAmount;
	}

	public boolean isComplete() {
		return complete;
	}

	public void setComplete(boolean complete) {
		this.complete = complete;
	}

	public Farmer getFarmer() {
		return farmer;
	}

	public void setFarmer(Farmer farmer) {
		this.farmer = farmer;
	}

	public Buyer getBuyer() {
		return buyer;
	}

	public void setBuyer(Buyer buyer) {
		this.buyer = buyer;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Date getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}

	public List<AdvanceTransaction> getAdvanceTransaction() {
		return advanceTransaction;
	}

	public void setAdvanceTransaction(List<AdvanceTransaction> advanceTransaction) {
		this.advanceTransaction = advanceTransaction;
	}

}
