package com.ambika.web.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="farmers")
@NamedNativeQueries({
	@NamedNativeQuery(name = "com.ambika.web.core.entity.Farmer.findAll", query = "SELECT * FROM farmers where is_delete = 0", resultClass = Farmer.class)
})
public class Farmer implements Serializable {
	private static final long serialVersionUID = -3343472432482988990L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "farmer")
	@JsonManagedReference(value = "farmerPurchaseOrder")
	private List<PurchaseOrder> purchaseOrders = new ArrayList<PurchaseOrder>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "farmer")
	@JsonManagedReference(value = "farmerAdvance")
	private List<Advance> advance = new ArrayList<Advance>();

	@Column(name = "name", nullable = false, length = 255)
	private String name;
	
	@Column(name = "is_delete")
	private boolean isDelete;
	
	@Column(name = "delete_date")
	private Date deleteDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<PurchaseOrder> getPurchaseOrders() {
		return purchaseOrders;
	}

	public void setPurchaseOrders(List<PurchaseOrder> purchaseOrders) {
		this.purchaseOrders = purchaseOrders;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Date getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}

	public List<Advance> getAdvance() {
		return advance;
	}

	public void setAdvance(List<Advance> advance) {
		this.advance = advance;
	}

}
