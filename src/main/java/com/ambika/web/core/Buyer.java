package com.ambika.web.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="buyers")
@NamedNativeQueries({
	@NamedNativeQuery(name = "com.ambika.web.core.entity.Buyer.findAll", query = "SELECT * FROM buyers where is_delete = 0", resultClass = Buyer.class)
})
public class Buyer implements Serializable {
	private static final long serialVersionUID = 6760343397567597327L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "buyer")
	@JsonManagedReference(value = "buyerSaleOrder")
	private List<SaleOrder> saleOrders = new ArrayList<SaleOrder>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "buyer")
	@JsonManagedReference(value = "buyerAdvance")
	private List<Advance> advance = new ArrayList<Advance>();

	@Column(name = "name", nullable = false, length = 255)
	private String name;

	@Column(name = "is_delete")
	private boolean isDelete;

	@Column(name = "delete_date")
	private Date deleteDate;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<SaleOrder> getSaleOrders() {
		return saleOrders;
	}

	public void setSaleOrders(List<SaleOrder> saleOrders) {
		this.saleOrders = saleOrders;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Date getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}

	public List<Advance> getAdvance() {
		return advance;
	}

	public void setAdvance(List<Advance> advance) {
		this.advance = advance;
	}

}
