package com.ambika.web.core.transaction;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;
import javax.validation.constraints.Digits;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.ambika.web.core.BalanceSheet;
import com.ambika.web.core.MiscellaneousAccount;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="balance_sheet_transactions")
@NamedNativeQueries({
	@NamedNativeQuery(name = "com.ambika.web.core.entity.BalanceSheetTransaction.findAll", 
			query = "SELECT * FROM balance_sheet_transactions where is_delete = 0", resultClass = BalanceSheetTransaction.class)
})
public class BalanceSheetTransaction implements Serializable {
	private static final long serialVersionUID = 5719309423513524218L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
	
	@Column(name = "date")
	private Date date;

	@Digits(integer=10, fraction=2)
	@Column(name = "amount")
	private BigDecimal amount;
	
	@Column(name = "account_name", nullable = false, length = 255)
	private String accountName;

	@Column(name = "account_type")
	private boolean accountType;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade({ CascadeType.SAVE_UPDATE })
	@JoinColumn(name="balance_sheet_id", nullable = false)
	@JsonBackReference(value = "balanceSheet")
	private BalanceSheet balanceSheet;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade({ CascadeType.SAVE_UPDATE })
	@JoinColumn(name="purchase_order_transaction_id", nullable = true)
	//@JsonBackReference(value = "balanceSheetPurchaseOrderTransaction")
	private PurchaseOrderTransaction purchaseOrderTransaction;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade({ CascadeType.SAVE_UPDATE })
	@JoinColumn(name="sale_order_transaction_id", nullable = true)
	//@JsonBackReference(value = "balanceSheetSaleOrderTransaction")
	private SaleOrderTransaction saleOrderTransaction;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade({ CascadeType.SAVE_UPDATE })
	@JoinColumn(name="miscellaneous_account_transaction_id", nullable = true)
	//@JsonBackReference(value = "balanceSheetMiscellaneousAccountTransaction")
	private MiscellaneousAccountTransaction miscellaneousAccountTransaction;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade({ CascadeType.SAVE_UPDATE })
	@JoinColumn(name="miscellaneous_account_id", nullable = true)
	//@JsonBackReference(value = "balanceSheetMiscellaneousAccount")
	private MiscellaneousAccount miscellaneousAccount;

	@Column(name = "is_delete")
	private boolean isDelete;
	
	@Column(name = "delete_date")
	private Date deleteDate;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public boolean isAccountType() {
		return accountType;
	}

	public void setAccountType(boolean accountType) {
		this.accountType = accountType;
	}

	public BalanceSheet getBalanceSheet() {
		return balanceSheet;
	}

	public void setBalanceSheet(BalanceSheet balanceSheet) {
		this.balanceSheet = balanceSheet;
	}

	public PurchaseOrderTransaction getPurchaseOrderTransaction() {
		return purchaseOrderTransaction;
	}

	public void setPurchaseOrderTransaction(
			PurchaseOrderTransaction purchaseOrderTransaction) {
		this.purchaseOrderTransaction = purchaseOrderTransaction;
	}

	public SaleOrderTransaction getSaleOrderTransaction() {
		return saleOrderTransaction;
	}

	public void setSaleOrderTransaction(SaleOrderTransaction saleOrderTransaction) {
		this.saleOrderTransaction = saleOrderTransaction;
	}

	public MiscellaneousAccountTransaction getMiscellaneousAccountTransaction() {
		return miscellaneousAccountTransaction;
	}

	public void setMiscellaneousAccountTransaction(
			MiscellaneousAccountTransaction miscellaneousAccountTransaction) {
		this.miscellaneousAccountTransaction = miscellaneousAccountTransaction;
	}

	public MiscellaneousAccount getMiscellaneousAccount() {
		return miscellaneousAccount;
	}

	public void setMiscellaneousAccount(MiscellaneousAccount miscellaneousAccount) {
		this.miscellaneousAccount = miscellaneousAccount;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Date getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}
	
}