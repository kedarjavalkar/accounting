package com.ambika.web.core.transaction;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Digits;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.ambika.web.core.PurchaseOrder;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="purchase_order_transactions")
@NamedNativeQueries({
	@NamedNativeQuery(name = "com.ambika.web.core.entity.PurchaseOrderTransaction.findAll", 
			query = "SELECT * FROM purchase_order_transactions where is_delete = 0", resultClass = PurchaseOrderTransaction.class)
})
public class PurchaseOrderTransaction implements Serializable {
	private static final long serialVersionUID = -769440950509704707L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade({ CascadeType.SAVE_UPDATE })
	@JoinColumn(name="purchase_order_id", nullable = false)
	@JsonBackReference(value = "purchaseOrderTransaction")
	private PurchaseOrder purchaseOrder;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "purchaseOrderTransaction")
	@JsonManagedReference(value = "purchaseOrderTransactionAdvanceTransaction")
	private List<AdvanceTransaction> advanceTransaction = new ArrayList<AdvanceTransaction>();
	
	@Column(name = "date")
	private Date date;

	@Digits(integer=10, fraction=2)
	@Column(name = "paid_amount")
	private BigDecimal paidAmount;

	// 0 expense., 1 income
	@Column(name = "is_type")
	private boolean isType;

	@Digits(integer=10, fraction=2)
	@Column(name = "pending_amount")
	private BigDecimal pendingAmount;
	
	@Digits(integer=10, fraction=2)
	@Column(name = "discount_amount")
	private BigDecimal discountAmount;

	@Digits(integer=10, fraction=2)
	@Column(name = "total_amount")
	private BigDecimal totalAmount;

	@Column(name = "is_delete")
	private boolean isDelete;
	
	@Column(name = "delete_date")
	private Date deleteDate;
	
	@Column(name = "is_advance")
	private boolean isAdvance;
	
	@Column(name = "is_discount")
	private boolean isDiscount;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PurchaseOrder getPurchaseOrder() {
		return purchaseOrder;
	}

	public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public boolean isType() {
		return isType;
	}

	public void setType(boolean isType) {
		this.isType = isType;
	}

	public BigDecimal getPendingAmount() {
		return pendingAmount;
	}

	public void setPendingAmount(BigDecimal pendingAmount) {
		this.pendingAmount = pendingAmount;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Date getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}

	public boolean isAdvance() {
		return isAdvance;
	}

	public void setAdvance(boolean isAdvance) {
		this.isAdvance = isAdvance;
	}

	public boolean isDiscount() {
		return isDiscount;
	}

	public void setDiscount(boolean isDiscount) {
		this.isDiscount = isDiscount;
	}

	public List<AdvanceTransaction> getAdvanceTransaction() {
		return advanceTransaction;
	}

	public void setAdvanceTransaction(List<AdvanceTransaction> advanceTransaction) {
		this.advanceTransaction = advanceTransaction;
	}

}
