package com.ambika.web.core.transaction;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;
import javax.validation.constraints.Digits;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.ambika.web.core.Advance;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="advance_transactions")
@NamedNativeQueries({
	@NamedNativeQuery(name = "com.ambika.web.core.entity.AdvanceTransaction.findAll",
			query = "SELECT * FROM advance_transactions where is_delete = 0", resultClass = AdvanceTransaction.class)
})
public class AdvanceTransaction implements Serializable {
	private static final long serialVersionUID = 9206793984878404673L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "date")
	private Date date;

	@Digits(integer=10, fraction=2)
	@Column(name = "amount")
	private BigDecimal amount;

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade({ CascadeType.SAVE_UPDATE })
	@JoinColumn(name="advance_id", nullable = false)
	@JsonBackReference(value = "advanceAdvanceTransaction")
	private Advance advance;

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade({ CascadeType.SAVE_UPDATE })
	@JoinColumn(name="purchase_order_transaction_id", nullable = true)
	@JsonBackReference(value = "purchaseOrderTransactionAdvanceTransaction")
	private PurchaseOrderTransaction purchaseOrderTransaction;

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade({ CascadeType.SAVE_UPDATE })
	@JoinColumn(name="sale_order_transaction_id", nullable = true)
	@JsonBackReference(value = "saleOrderTransactionAdvanceTransaction")
	private SaleOrderTransaction saleOrderTransaction;

	@Column(name = "is_delete")
	private boolean isDelete;

	@Column(name = "delete_date")
	private Date deleteDate;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Advance getAdvance() {
		return advance;
	}

	public void setAdvance(Advance advance) {
		this.advance = advance;
	}

	public PurchaseOrderTransaction getPurchaseOrderTransaction() {
		return purchaseOrderTransaction;
	}

	public void setPurchaseOrderTransaction(
			PurchaseOrderTransaction purchaseOrderTransaction) {
		this.purchaseOrderTransaction = purchaseOrderTransaction;
	}

	public SaleOrderTransaction getSaleOrderTransaction() {
		return saleOrderTransaction;
	}

	public void setSaleOrderTransaction(SaleOrderTransaction saleOrderTransaction) {
		this.saleOrderTransaction = saleOrderTransaction;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Date getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}

}