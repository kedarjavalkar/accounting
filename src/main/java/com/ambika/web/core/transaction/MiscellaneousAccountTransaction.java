package com.ambika.web.core.transaction;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;
import javax.validation.constraints.Digits;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.ambika.web.core.MiscellaneousAccount;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="miscellaneous_account_transactions")
@NamedNativeQueries({
	@NamedNativeQuery(name = "com.ambika.web.core.entity.MiscellaneousAccountTransaction.findAll", 
			query = "SELECT * FROM miscellaneous_account_transactions where id = 0", resultClass = MiscellaneousAccountTransaction.class)
})
public class MiscellaneousAccountTransaction implements Serializable {
	private static final long serialVersionUID = 1750453745332648774L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade({ CascadeType.SAVE_UPDATE })
	@JoinColumn(name="miscellaneous_account_id", nullable = false)
	@JsonBackReference(value = "miscellaneousAccountTransaction")
	private MiscellaneousAccount miscellaneousAccount;

	@Column(name = "date")
	private Date date;

	@Digits(integer=10, fraction=2)
	@Column(name = "amount")
	private BigDecimal amount;

	@Column(name = "is_delete")
	private boolean isDelete;

	@Column(name = "delete_date")
	private Date deleteDate;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MiscellaneousAccount getMiscellaneousAccount() {
		return miscellaneousAccount;
	}

	public void setMiscellaneousAccount(MiscellaneousAccount miscellaneousAccount) {
		this.miscellaneousAccount = miscellaneousAccount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Date getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}

}
