package com.ambika.web.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Digits;

import com.ambika.web.core.transaction.BalanceSheetTransaction;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="balance_sheet")
@NamedNativeQueries({
	@NamedNativeQuery(name = "com.ambika.web.core.entity.BalanceSheet.findAll", query = "SELECT * FROM balance_sheet", resultClass = BalanceSheet.class)
})
public class BalanceSheet implements Serializable {
	private static final long serialVersionUID = 590934899615321779L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "date")
	private Date date;

	@Digits(integer=10, fraction=2)
	@Column(name = "opening_amount")
	private BigDecimal openingAmount;

	@Digits(integer=10, fraction=2)
	@Column(name = "closing_amount")
	private BigDecimal closingAmount;

	@Digits(integer=10, fraction=2)
	@Column(name = "total_expense")
	private BigDecimal totalExpenses;

	@Digits(integer=10, fraction=2)
	@Column(name = "total_income")
	private BigDecimal totalIncome;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "balanceSheet")
	@JsonManagedReference(value = "balanceSheet")
	private List<BalanceSheetTransaction> balanceSheetTransaction = new ArrayList<BalanceSheetTransaction>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getOpeningAmount() {
		return openingAmount;
	}

	public void setOpeningAmount(BigDecimal openingAmount) {
		this.openingAmount = openingAmount;
	}

	public BigDecimal getClosingAmount() {
		return closingAmount;
	}

	public void setClosingAmount(BigDecimal closingAmount) {
		this.closingAmount = closingAmount;
	}

	public BigDecimal getTotalExpenses() {
		return totalExpenses;
	}

	public void setTotalExpenses(BigDecimal totalExpenses) {
		this.totalExpenses = totalExpenses;
	}

	public BigDecimal getTotalIncome() {
		return totalIncome;
	}

	public void setTotalIncome(BigDecimal totalIncome) {
		this.totalIncome = totalIncome;
	}

	public List<BalanceSheetTransaction> getBalanceSheetTransaction() {
		return balanceSheetTransaction;
	}

	public void setBalanceSheetTransaction(
			List<BalanceSheetTransaction> balanceSheetTransaction) {
		this.balanceSheetTransaction = balanceSheetTransaction;
	}

}
