package com.ambika.web.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Digits;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.ambika.web.core.transaction.SaleOrderTransaction;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="sale_orders")
@NamedNativeQueries({
	@NamedNativeQuery(name = "com.ambika.web.core.entity.SaleOrder.findAll", query = "SELECT * FROM sale_orders where is_delete = 0", resultClass = SaleOrder.class)
})
public class SaleOrder implements Serializable {
	private static final long serialVersionUID = 4805214499669191346L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade({ CascadeType.SAVE_UPDATE })
	@JoinColumn(name="buyer_id", nullable = false)
	@JsonBackReference(value = "buyerSaleOrder")
	private Buyer buyer;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "saleOrder")
	@JsonManagedReference(value = "saleOrderTransaction")
	private List<SaleOrderTransaction> saleOrderTransactions = new ArrayList<SaleOrderTransaction>();

	@Column(name = "date")
	private Date date;

	@Digits(integer=10, fraction=2)
	@Column(name = "rate")
	private BigDecimal rate;

	@Digits(integer=10, fraction=2)
	@Column(name = "amount")
	private BigDecimal amount;

	@Digits(integer=10, fraction=2)
	@Column(name = "pending_amount")
	private BigDecimal pendingAmount;

	@Digits(integer=10, fraction=2)
	@Column(name = "tray")
	private BigDecimal tray;

	@Digits(integer=10, fraction=2)
	@Column(name = "pending_trays")
	private BigDecimal pendingTrays;

	@Digits(integer=10, fraction=2)
	@Column(name = "discount_amount")
	private BigDecimal discountAmount;

	@Column(name = "complete")
	private boolean complete;

	@Column(name = "is_delete")
	private boolean isDelete;
	
	@Column(name = "delete_date")
	private Date deleteDate;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Buyer getBuyer() {
		return buyer;
	}

	public void setBuyer(Buyer buyer) {
		this.buyer = buyer;
	}

	public List<SaleOrderTransaction> getSaleOrderTransactions() {
		return saleOrderTransactions;
	}

	public void setSaleOrderTransactions(
			List<SaleOrderTransaction> saleOrderTransactions) {
		this.saleOrderTransactions = saleOrderTransactions;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getPendingAmount() {
		return pendingAmount;
	}

	public void setPendingAmount(BigDecimal pendingAmount) {
		this.pendingAmount = pendingAmount;
	}

	public BigDecimal getTray() {
		return tray;
	}

	public void setTray(BigDecimal tray) {
		this.tray = tray;
	}

	public BigDecimal getPendingTrays() {
		return pendingTrays;
	}

	public void setPendingTrays(BigDecimal pendingTrays) {
		this.pendingTrays = pendingTrays;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public boolean isComplete() {
		return complete;
	}

	public void setComplete(boolean complete) {
		this.complete = complete;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Date getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}

}
