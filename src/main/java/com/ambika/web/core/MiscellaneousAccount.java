package com.ambika.web.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.ambika.web.core.transaction.MiscellaneousAccountTransaction;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="miscellaneous_accounts")
@NamedNativeQueries({
	@NamedNativeQuery(name = "com.ambika.web.core.entity.MiscellaneousAccount.findAll", 
			query = "SELECT * FROM miscellaneous_accounts where is_delete = 0", resultClass = MiscellaneousAccount.class)
})
public class MiscellaneousAccount implements Serializable {
	private static final long serialVersionUID = -8361132737702521056L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "miscellaneousAccount")
	@JsonManagedReference(value = "miscellaneousAccountTransaction")
	private List<MiscellaneousAccountTransaction> miscellaneousAccountTransaction = new ArrayList<MiscellaneousAccountTransaction>();

	@Column(name = "name", nullable = false, length = 255)
	private String name;

	@Column(name = "is_type")
	private boolean isType;

	@Column(name = "is_delete")
	private boolean isDelete;
	
	@Column(name = "delete_date")
	private Date deleteDate;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<MiscellaneousAccountTransaction> getMiscellaneousAccountTransaction() {
		return miscellaneousAccountTransaction;
	}

	public void setMiscellaneousAccountTransaction(
			List<MiscellaneousAccountTransaction> miscellaneousAccountTransaction) {
		this.miscellaneousAccountTransaction = miscellaneousAccountTransaction;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isType() {
		return isType;
	}

	public void setType(boolean isType) {
		this.isType = isType;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Date getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}

}
