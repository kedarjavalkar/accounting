package com.ambika.web.resources.transaction;

import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.ambika.web.auth.AuthUser;
import com.ambika.web.controller.transaction.SaleOrderTransactionController;
import com.ambika.web.core.transaction.SaleOrderTransaction;
import com.ambika.web.utils.Dao;
import com.ambika.web.utils.DateUtils;
import com.ambika.web.utils.JsonUtils;
import com.ambika.web.utils.async.LoggerMail;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Path("/saleordertransaction")
@Produces(MediaType.APPLICATION_JSON)
public class SaleOrderTransactionResource {
	private static final Logger logger = Logger.getLogger((SaleOrderTransactionResource.class).getName());
	private SaleOrderTransactionController sotController;

	public SaleOrderTransactionResource(Dao dao) {
		this.sotController = new SaleOrderTransactionController(dao);
	}

	@GET
	@UnitOfWork
	public Response findAll(@Auth AuthUser auth) {
		List<SaleOrderTransaction> list = sotController.findAll();
		return Response.ok(JsonUtils.getJson(list)).build();
	}

	@POST
	@UnitOfWork
	public Response save(@Auth AuthUser auth, @QueryParam("soids") List<Long> soIds, @QueryParam("amt") String amountPaid, 
			@QueryParam("discamt") String discountAmount, @QueryParam("date") String date,
			@QueryParam("miscid") Long miscId) {
		try {
			sotController.save(soIds, miscId, date, amountPaid, discountAmount, null);
			return Response.ok(JsonUtils.getSuccessJson("Transaction(s) saved successfully...")).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to save sale order transaction. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

	@GET
	@UnitOfWork
	@Path("search")
	public Response search(@Auth AuthUser auth, @QueryParam("buyerid") Long buyerId, @QueryParam("from") String from, @QueryParam("to") String to) {
		try {
			List<SaleOrderTransaction> list = sotController.search(buyerId, from, to);

			JsonArray jsonArray = new JsonArray();
			for (SaleOrderTransaction sot : list) {
				if(!sot.getPaidAmount().equals(new BigDecimal("0.00"))) {
					JsonObject jsonObject = new JsonObject();
					jsonObject.addProperty("id", sot.getId());
					jsonObject.addProperty("paidAmount", sot.getPaidAmount());
					jsonObject.addProperty("pendingAmount", sot.getPendingAmount());
					jsonObject.addProperty("totalAmount", sot.getTotalAmount());
					jsonObject.addProperty("type", sot.isType());
					jsonObject.addProperty("returnTrays", sot.getReturnTrays());
					jsonObject.addProperty("discountAmount", sot.getDiscountAmount());
					jsonObject.addProperty("name", sot.getSaleOrder().getBuyer().getName());
					if(sot.getDate() != null)
						jsonObject.addProperty("date", DateUtils.format(sot.getDate()));

					if(sot.isDiscount())
						jsonObject.addProperty("paymentMethod", "Discount");
					else if(sot.isAdvance())
						jsonObject.addProperty("paymentMethod", "Advance");
					else
						jsonObject.addProperty("paymentMethod", "Cash");

					jsonArray.add(jsonObject);
				}
			}
			return Response.ok(new Gson().toJson(jsonArray)).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to search sale order transaction. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

	@POST
	@UnitOfWork
	@Path("delete/{id}")
	public Response delete(@Auth AuthUser auth, @PathParam("id") Long id) {
		try {
			String str = sotController.delete(id);
			return Response.ok(JsonUtils.getSuccessJson(str)).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to delete sale order transaction. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

}
