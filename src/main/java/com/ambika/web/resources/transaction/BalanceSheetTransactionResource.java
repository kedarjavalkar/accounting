package com.ambika.web.resources.transaction;

import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ambika.web.auth.AuthUser;
import com.ambika.web.controller.transaction.BalanceSheetTransactionController;
import com.ambika.web.core.transaction.BalanceSheetTransaction;
import com.ambika.web.utils.Dao;
import com.ambika.web.utils.JsonUtils;

@Path("/balancesheettransaction")
@Produces(MediaType.APPLICATION_JSON)
public class BalanceSheetTransactionResource {
	//private static final Logger logger = Logger.getLogger((BalanceSheetTransactionResource.class).getName());
	private BalanceSheetTransactionController bstController;

	public BalanceSheetTransactionResource(Dao dao) {
		this.bstController = new BalanceSheetTransactionController(dao);
	}

	@GET
	@UnitOfWork
	public Response findAll(@Auth AuthUser auth) {
		List<BalanceSheetTransaction> list = bstController.findAll();
		return Response.ok(JsonUtils.getJson(list)).build();
	}

}
