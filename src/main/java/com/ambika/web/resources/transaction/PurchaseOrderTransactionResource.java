package com.ambika.web.resources.transaction;

import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.ambika.web.auth.AuthUser;
import com.ambika.web.controller.transaction.PurchaseOrderTransactionController;
import com.ambika.web.core.transaction.PurchaseOrderTransaction;
import com.ambika.web.utils.Dao;
import com.ambika.web.utils.DateUtils;
import com.ambika.web.utils.JsonUtils;
import com.ambika.web.utils.async.LoggerMail;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Path("/purchaseordertransaction")
@Produces(MediaType.APPLICATION_JSON)
public class PurchaseOrderTransactionResource {
	private static final Logger logger = Logger.getLogger((PurchaseOrderTransactionResource.class).getName());
	private PurchaseOrderTransactionController potController;

	public PurchaseOrderTransactionResource(Dao dao) {
		this.potController = new PurchaseOrderTransactionController(dao);
	}

	@GET
	@UnitOfWork
	public Response findAll(@Auth AuthUser auth) {
		List<PurchaseOrderTransaction> list = potController.findAll();
		return Response.ok(JsonUtils.getJson(list)).build();
	}

	@POST
	@UnitOfWork
	public Response save(@Auth AuthUser auth, @QueryParam("poids") List<Long> poIds, @QueryParam("amt") String amountPaid, 
			@QueryParam("discamt") String discountAmount, @QueryParam("date") String date,
			@QueryParam("miscid") Long miscId) {
		try {
			potController.save(poIds, miscId, date, amountPaid, discountAmount, null);
			return Response.ok(JsonUtils.getSuccessJson("Transaction(s) saved successfully")).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to save purchase order transaction. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}


	@GET
	@UnitOfWork
	@Path("search")
	public Response search(@Auth AuthUser auth, @QueryParam("farmerid") Long farmerId, @QueryParam("from") String from, @QueryParam("to") String to) {
		try {
			List<PurchaseOrderTransaction> list = potController.search(farmerId, from, to);

			JsonArray jsonArray = new JsonArray();
			for (PurchaseOrderTransaction pot : list) {
				JsonObject jsonObject = new JsonObject();
				jsonObject.addProperty("id", pot.getId());
				jsonObject.addProperty("paidAmount", pot.getPaidAmount());
				jsonObject.addProperty("pendingAmount", pot.getPendingAmount());
				jsonObject.addProperty("discountAmount", pot.getDiscountAmount());
				jsonObject.addProperty("totalAmount", pot.getTotalAmount());
				jsonObject.addProperty("type", pot.isType());
				jsonObject.addProperty("name", pot.getPurchaseOrder().getFarmer().getName());
				if(pot.getDate() != null)	
					jsonObject.addProperty("date", DateUtils.format(pot.getDate()));
				if(pot.getDate() != null)
					jsonObject.addProperty("date", DateUtils.format(pot.getDate()));

				if(pot.isDiscount())
					jsonObject.addProperty("paymentMethod", "Discount");
				else if(pot.isAdvance())
					jsonObject.addProperty("paymentMethod", "Advance");
				else
					jsonObject.addProperty("paymentMethod", "Cash");
				jsonArray.add(jsonObject);
			}
			return Response.ok(new Gson().toJson(jsonArray)).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to search purchase order transaction. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

	@POST
	@UnitOfWork
	@Path("delete/{id}")
	public Response delete(@Auth AuthUser auth, @PathParam("id") Long id) {
		try {
			String str = potController.delete(id);
			return Response.ok(JsonUtils.getSuccessJson(str)).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to delete purchase order transaction. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

}
