package com.ambika.web.resources.transaction;

import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.ambika.web.auth.AuthUser;
import com.ambika.web.controller.transaction.MiscellaneousTransactionController;
import com.ambika.web.core.transaction.MiscellaneousAccountTransaction;
import com.ambika.web.utils.Dao;
import com.ambika.web.utils.JsonUtils;
import com.ambika.web.utils.async.LoggerMail;

@Path("/miscellaneoustransaction")
@Produces(MediaType.APPLICATION_JSON)
public class MiscellaneousTransactionResource {
	private static final Logger logger = Logger.getLogger((MiscellaneousTransactionResource.class).getName());
	private MiscellaneousTransactionController matController;

	public MiscellaneousTransactionResource(Dao dao) {
		this.matController = new MiscellaneousTransactionController(dao);
	}

	@GET
	@UnitOfWork
	public Response findAll(@Auth AuthUser auth) {
		List<MiscellaneousAccountTransaction> list = matController.findAll();
		return Response.ok(JsonUtils.getJson(list)).build();
	}

	@POST
	@UnitOfWork
	public Response save(@Auth AuthUser auth, MiscellaneousAccountTransaction misct) {
		try {
			misct = matController.save(misct);
			return Response.ok(JsonUtils.getJson(misct)).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to save miscellaneous account transaction. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

	@POST
	@UnitOfWork
	@Path("delete/{id}")
	public Response delete(@Auth AuthUser auth, @PathParam("id") Long id) {
		try {
			String str = matController.delete(id);
			return Response.ok(JsonUtils.getSuccessJson(str)).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to delete miscellaneous account transaction. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}
}