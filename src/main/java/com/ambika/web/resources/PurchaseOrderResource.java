package com.ambika.web.resources;

import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.ambika.web.auth.AuthUser;
import com.ambika.web.controller.PurchaseOrderController;
import com.ambika.web.controller.transaction.PurchaseOrderTransactionController;
import com.ambika.web.core.Advance;
import com.ambika.web.core.PurchaseOrder;
import com.ambika.web.core.transaction.PurchaseOrderTransaction;
import com.ambika.web.utils.Dao;
import com.ambika.web.utils.DateUtils;
import com.ambika.web.utils.JsonUtils;
import com.ambika.web.utils.RoleUtils;
import com.ambika.web.utils.async.LoggerMail;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Path("/purchaseorder")
@Produces(MediaType.APPLICATION_JSON)
public class PurchaseOrderResource {
	private static final Logger logger = Logger.getLogger((PurchaseOrderResource.class).getName());
	private PurchaseOrderController poController;
	private PurchaseOrderTransactionController potController;

	public PurchaseOrderResource(Dao dao) {
		this.poController = new PurchaseOrderController(dao);
		this.potController = new PurchaseOrderTransactionController(dao);
	}

	@GET
	@UnitOfWork
	public Response findAll(@Auth AuthUser auth) {
		List<PurchaseOrder> list = poController.findAll();
		return Response.ok(JsonUtils.getJson(list)).build();
	}

	@POST
	@UnitOfWork
	public Response save(@Auth AuthUser auth, PurchaseOrder purchaseOrder) {
		try {
			purchaseOrder = poController.save(purchaseOrder);

			for (Advance adv : purchaseOrder.getFarmer().getAdvance()) {
				if(adv.isDelete() == false && adv.isComplete() == false) {
					List<Long> poIds = new ArrayList<Long>();
					poIds.add(purchaseOrder.getId());
					potController.save(poIds, null, DateUtils.format(purchaseOrder.getDate()), adv.getPendingAmount().toString(), "0.00", adv.getId());
				}
			}

			JsonObject jsonObject = new JsonObject();
			jsonObject.addProperty("id", purchaseOrder.getId());
			if(purchaseOrder.getDate() != null)
				jsonObject.addProperty("date", DateUtils.format(purchaseOrder.getDate()));
			jsonObject.addProperty("name", purchaseOrder.getFarmer().getName());
			jsonObject.addProperty("weight", purchaseOrder.getWeight());
			jsonObject.addProperty("tray", RoleUtils.validate(auth, purchaseOrder.getTray().toString()));
			jsonObject.addProperty("amount", purchaseOrder.getAmount());
			jsonObject.addProperty("pendingAmount", purchaseOrder.getPendingAmount());
			jsonObject.addProperty("discountAmount", purchaseOrder.getDiscountAmount());

			return Response.ok(new Gson().toJson(jsonObject)).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to save purchase order. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

	@GET
	@UnitOfWork
	@Path("search")
	public Response search(@Auth AuthUser auth, @QueryParam("farmerid") Long farmerId, @QueryParam("status") String status, 
			@QueryParam("from") String from, @QueryParam("to") String to) {
		try {
			JsonArray jsonArray = new JsonArray();
			for (PurchaseOrder purchaseOrder : poController.search(farmerId, status, from, to)) {
				JsonObject jsonObject = new JsonObject();
				jsonObject.addProperty("id", purchaseOrder.getId());
				if(purchaseOrder.getDate() != null)
					jsonObject.addProperty("date", DateUtils.format(purchaseOrder.getDate()));
				jsonObject.addProperty("name", purchaseOrder.getFarmer().getName());
				jsonObject.addProperty("weight", purchaseOrder.getWeight());
				jsonObject.addProperty("tray", RoleUtils.validate(auth, purchaseOrder.getTray().toString()));
				jsonObject.addProperty("amount", purchaseOrder.getAmount());
				jsonObject.addProperty("pendingAmount", purchaseOrder.getPendingAmount());
				jsonObject.addProperty("discountAmount", purchaseOrder.getDiscountAmount());

				BigDecimal advancePending = new BigDecimal("0.00");
				for (Advance adv : purchaseOrder.getFarmer().getAdvance()) {
					if(adv.isDelete() == false) {
						advancePending = advancePending.add(adv.getPendingAmount());
					}
				}
				jsonObject.addProperty("pendingAdvance", advancePending);
				jsonArray.add(jsonObject);
			}
			return Response.ok(new Gson().toJson(jsonArray)).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to search purchase order. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

	@GET
	@UnitOfWork
	@Path("pot")
	public Response findById(@Auth AuthUser auth, @QueryParam("poids") List<Long> poIds) {
		List<PurchaseOrder> list = poController.getPoByIds(poIds);

		JsonArray poArray = new JsonArray();
		for (PurchaseOrder po : list) {
			JsonObject poObj = new JsonObject();
			poObj.addProperty("id", po.getId());
			poObj.addProperty("name", po.getFarmer().getName());
			poObj.addProperty("weight", po.getWeight());
			poObj.addProperty("tray", RoleUtils.validate(auth, po.getTray().toString()));
			poObj.addProperty("amount", po.getAmount());
			poObj.addProperty("pendingAmount", po.getPendingAmount());
			poObj.addProperty("discountAmount", po.getDiscountAmount());
			if(po.getDate() != null)
				poObj.addProperty("date", DateUtils.format(po.getDate()));

			JsonArray potArray = new JsonArray();
			for (PurchaseOrderTransaction pot : po.getPurchaseOrderTransactions()) {
				if(pot.isDelete() == false) {
					JsonObject potObj = new JsonObject();
					potObj.addProperty("id", pot.getId());
					potObj.addProperty("paidAmount", pot.getPaidAmount());
					potObj.addProperty("pendingAmount", pot.getPendingAmount());
					potObj.addProperty("discountAmount", pot.getDiscountAmount());
					potObj.addProperty("totalAmount", pot.getTotalAmount());
					potObj.addProperty("type", pot.isType());
					if(pot.getDate() != null)
						potObj.addProperty("date", DateUtils.format(pot.getDate()));

					if(pot.isDiscount())
						potObj.addProperty("paymentMethod", "Discount");
					else if(pot.isAdvance())
						potObj.addProperty("paymentMethod", "Advance");
					else
						potObj.addProperty("paymentMethod", "Cash");
					potArray.add(potObj);
				}
			}
			poObj.add("purchaseOrderTransaction", potArray);
			poArray.add(poObj);
		}

		return Response.ok(new Gson().toJson(poArray)).build();
	}

	@POST
	@UnitOfWork
	@Path("advance")
	public Response advancePo(@Auth AuthUser auth, @QueryParam("poids") List<Long> poIds) {
		try {
			for (Long id : poIds) {
				PurchaseOrder po = poController.getById(id);
				for (Advance adv : po.getFarmer().getAdvance()) {
					if(adv.isDelete() == false && adv.isComplete() == false) {
						List<Long> ids = new ArrayList<Long>();
						ids.add(po.getId());
						potController.save(ids, null, DateUtils.format(adv.getDate()), adv.getPendingAmount().toString(), "0.00", adv.getId());
					}
				}
				return Response.ok(JsonUtils.getSuccessJson("Advance payment(s) recorded successfully...")).build();
			}
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson("Incorrect Input...")).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to make advance payment. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

	@POST
	@UnitOfWork
	@Path("delete/{id}")
	public Response delete(@Auth AuthUser auth, @PathParam("id") Long id) {
		try {
			String str = poController.delete(id);
			return Response.ok(JsonUtils.getSuccessJson(str)).build();
		} catch(Exception ex) {
			logger.log(Level.SEVERE, "Failed to delete purchase order. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

}

