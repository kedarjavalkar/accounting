package com.ambika.web.resources;

import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.ambika.web.auth.AuthUser;
import com.ambika.web.controller.AdvanceController;
import com.ambika.web.core.Advance;
import com.ambika.web.utils.Dao;
import com.ambika.web.utils.DateUtils;
import com.ambika.web.utils.JsonUtils;
import com.ambika.web.utils.async.LoggerMail;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Path("/advance")
@Produces(MediaType.APPLICATION_JSON)
public class AdvanceResource {
	private static final Logger logger = Logger.getLogger((AdvanceResource.class).getName());
	private AdvanceController advController;

	public AdvanceResource(Dao dao) {
		this.advController = new AdvanceController(dao);
	}

	@GET
	@UnitOfWork
	public Response findAll(@Auth AuthUser auth) {
		List<Advance> list = advController.findAll();
		JsonArray jsonArray = getAdvanceBoiler(list);
		return Response.ok(new Gson().toJson(jsonArray)).build();
	}

	@GET
	@UnitOfWork
	@Path("{type}")
	public Response findByType(@Auth AuthUser auth, @PathParam("type") String type) {
		try {
			List<Advance> list = advController.findByType(type);
			JsonArray jsonArray = getAdvanceBoiler(list);
			return Response.ok(new Gson().toJson(jsonArray)).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to get advance list. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

	@POST
	@UnitOfWork
	public Response save(@Auth AuthUser auth, Advance advance) {
		try {
			advance = advController.save(advance);
			return Response.ok(JsonUtils.getJson(advance)).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to save advance. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

	@POST
	@UnitOfWork
	@Path("delete/{id}")
	public Response delete(@Auth AuthUser auth, @PathParam("id") Long id) {
		try {
			String str = advController.delete(id);
			return Response.ok(JsonUtils.getSuccessJson(str)).build();
		} catch(Exception ex) {
			logger.log(Level.SEVERE, "Failed to delete advance. Oops: " + ex.toString());
			LoggerMail.instance().send(ex);
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

	private JsonArray getAdvanceBoiler(List<Advance> list) {
		JsonArray jsonArray = new JsonArray();
		for (Advance advance : list) {
			JsonObject jsonObject = new JsonObject();
			jsonObject.addProperty("id", advance.getId());
			jsonObject.addProperty("totalAmount", advance.getTotalAmount());
			jsonObject.addProperty("pendingAmount", advance.getPendingAmount());
			if(advance.getDate() != null)
				jsonObject.addProperty("date", DateUtils.format(advance.getDate()));
			if(advance.getFarmer() != null)
				jsonObject.addProperty("name", advance.getFarmer().getName());
			if(advance.getBuyer() != null)
				jsonObject.addProperty("name", advance.getBuyer().getName());
			jsonArray.add(jsonObject);
		}
		return jsonArray;
	}

}