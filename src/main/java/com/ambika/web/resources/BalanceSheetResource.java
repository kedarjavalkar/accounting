package com.ambika.web.resources;

import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.ambika.web.auth.AuthUser;
import com.ambika.web.controller.BalanceSheetController;
import com.ambika.web.core.BalanceSheet;
import com.ambika.web.core.transaction.BalanceSheetTransaction;
import com.ambika.web.utils.Dao;
import com.ambika.web.utils.DateUtils;
import com.ambika.web.utils.JsonUtils;
import com.ambika.web.utils.async.LoggerMail;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Path("/balancesheet")
@Produces(MediaType.APPLICATION_JSON)
public class BalanceSheetResource {
	private static final Logger logger = Logger.getLogger((BalanceSheetResource.class).getName());
	private BalanceSheetController bsController;

	public BalanceSheetResource(Dao dao) {
		this.bsController = new BalanceSheetController(dao);
	}

	@GET
	@UnitOfWork
	public Response findAll() {
		List<BalanceSheet> list = bsController.findAll();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonElement je = new JsonParser().parse(JsonUtils.getJson(list));
		return Response.ok(gson.toJson(je)).build();
	}

	@GET
	@UnitOfWork
	@Path("openclose")
	public Response openCloseBalanceSheet(@QueryParam("from") String fromDate) {
		try {
			bsController.openCloseBalanceSheet(fromDate);
			return Response.ok(JsonUtils.getSuccessJson("success...")).build();
		} catch(Exception ex){
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Oops. Failed to open close balance sheet... Msg: " + ex.toString(), logger);
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

	@GET
	@UnitOfWork
	@Path("date")
	public Response findByDateRange(@Auth AuthUser auth, @QueryParam("from") String from, @QueryParam("to") String to,
			@QueryParam("date") String date) {
		try {
			List<BalanceSheet> list = bsController.findByDateRange(date, from, to);

			JsonArray jsonArray = new JsonArray();
			for (BalanceSheet bs : list) {
				JsonObject jsonObject = new JsonObject();
				jsonObject.addProperty("closingAmount", bs.getClosingAmount());
				jsonObject.addProperty("id", bs.getId());
				jsonObject.addProperty("openingAmount", bs.getOpeningAmount());
				jsonObject.addProperty("totalAmount", bs.getTotalExpenses());
				jsonObject.addProperty("totalIncome", bs.getTotalIncome());
				jsonObject.addProperty("date", DateUtils.format(bs.getDate()));

				JsonArray arr = new JsonArray();
				for (BalanceSheetTransaction bst : bs.getBalanceSheetTransaction()) {
					if(bst.isDelete() == false) {
						JsonObject obj = new JsonObject();
						obj.addProperty("accountType", bst.isAccountType());
						obj.addProperty("accountName", bst.getAccountName());
						obj.addProperty("amount", bst.getAmount());
						obj.addProperty("id", bst.getId());
						arr.add(obj);
					}
				}
				jsonObject.add("balanceSheetTransaction", arr);
				jsonArray.add(jsonObject);;
			}

			return Response.ok(new Gson().toJson(jsonArray)).build();
		}catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to get Balance Sheet Transactions. Oops: " + ex.toString(), logger);
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

}
