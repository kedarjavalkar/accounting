package com.ambika.web.resources;

import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.ambika.web.auth.AuthUser;
import com.ambika.web.controller.SaleOrderController;
import com.ambika.web.controller.transaction.SaleOrderTransactionController;
import com.ambika.web.core.Advance;
import com.ambika.web.core.SaleOrder;
import com.ambika.web.core.transaction.SaleOrderTransaction;
import com.ambika.web.utils.Dao;
import com.ambika.web.utils.DateUtils;
import com.ambika.web.utils.JsonUtils;
import com.ambika.web.utils.async.LoggerMail;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Path("/saleorder")
@Produces(MediaType.APPLICATION_JSON)
public class SaleOrderResource {
	private static final Logger logger = Logger.getLogger((SaleOrderResource.class).getName());
	private SaleOrderController soController;
	private SaleOrderTransactionController sotController;

	public SaleOrderResource(Dao dao) {
		this.soController = new SaleOrderController(dao);
		this.sotController = new SaleOrderTransactionController(dao);
	}

	@GET
	@UnitOfWork
	public Response findAll(@Auth AuthUser auth) {
		List<SaleOrder> list = soController.findAll();
		return Response.ok(JsonUtils.getJson(list)).build();
	}

	@POST
	@UnitOfWork
	public Response save(@Auth AuthUser auth, SaleOrder saleOrder) {
		try {
			saleOrder = soController.save(saleOrder);

			for (Advance adv : saleOrder.getBuyer().getAdvance()) {
				if(adv.isDelete() == false && adv.isComplete() == false) {
					List<Long> soIds = new ArrayList<Long>();
					soIds.add(saleOrder.getId());
					sotController.save(soIds, null, DateUtils.format(saleOrder.getDate()), adv.getPendingAmount().toString(), "0.00", adv.getId());
				}
			}

			JsonObject jsonObject = new JsonObject();
			jsonObject.addProperty("id", saleOrder.getId());
			if(saleOrder.getDate() != null)
				jsonObject.addProperty("date", DateUtils.format(saleOrder.getDate()));
			jsonObject.addProperty("name", saleOrder.getBuyer().getName());
			jsonObject.addProperty("tray", saleOrder.getTray());
			jsonObject.addProperty("amount", saleOrder.getAmount());
			jsonObject.addProperty("pendingAmount", saleOrder.getPendingAmount());
			jsonObject.addProperty("pendingTray", saleOrder.getPendingTrays());

			return Response.ok(new Gson().toJson(jsonObject)).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to save sale order. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

	@GET
	@UnitOfWork
	@Path("search")
	public Response search(@Auth AuthUser auth, @QueryParam("buyerid") Long buyerId, @QueryParam("status") String status, 
			@QueryParam("from") String from, @QueryParam("to") String to) {
		try {
			JsonArray jsonArray = new JsonArray();
			for (SaleOrder saleOrder : soController.search(buyerId, status, from, to)) {
				JsonObject jsonObject = new JsonObject();
				jsonObject.addProperty("id", saleOrder.getId());
				jsonObject.addProperty("name", saleOrder.getBuyer().getName());
				jsonObject.addProperty("tray", saleOrder.getTray());
				jsonObject.addProperty("pendingTray", saleOrder.getPendingTrays());
				jsonObject.addProperty("amount", saleOrder.getAmount());
				jsonObject.addProperty("pendingAmount", saleOrder.getPendingAmount());
				if(saleOrder.getDate() != null)
					jsonObject.addProperty("date", DateUtils.format(saleOrder.getDate()));

				BigDecimal advancePending = new BigDecimal("0.00");
				for (Advance adv : saleOrder.getBuyer().getAdvance()) {
					if(adv.isDelete() == false) {
						advancePending = advancePending.add(adv.getPendingAmount());
					}
				}
				jsonObject.addProperty("pendingAdvance", advancePending);
				jsonArray.add(jsonObject);
			}
			return Response.ok(new Gson().toJson(jsonArray)).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to search sale order. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

	@POST
	@UnitOfWork
	@Path("tray")
	public Response updateTrays(@Auth AuthUser auth, SaleOrder so) {
		try {
			soController.updateTray(so);
			return Response.ok(JsonUtils.getSuccessJson("Updated trays...")).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to update trays. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

	@GET
	@UnitOfWork
	@Path("sot")
	public Response findById(@Auth AuthUser auth, @QueryParam("soids") List<Long> soIds) {
		try {
			JsonArray soArray = new JsonArray();
			for (SaleOrder saleOrder : soController.getSoByIds(soIds)) {
				JsonObject soObj = new JsonObject();
				soObj.addProperty("id", saleOrder.getId());
				soObj.addProperty("name", saleOrder.getBuyer().getName());
				soObj.addProperty("tray", saleOrder.getTray());
				soObj.addProperty("pendingTray", saleOrder.getPendingTrays());
				soObj.addProperty("amount", saleOrder.getAmount());
				soObj.addProperty("pendingAmount", saleOrder.getPendingAmount());
				if(saleOrder.getDate() != null)
					soObj.addProperty("date", DateUtils.format(saleOrder.getDate()));

				JsonArray sotArray = new JsonArray();
				for (SaleOrderTransaction sot : saleOrder.getSaleOrderTransactions()) {
					if(sot.isDelete() == false) {
						if(!sot.getPaidAmount().equals(new BigDecimal("0.00"))) {
							JsonObject sotObj = new JsonObject();
							sotObj.addProperty("id", sot.getId());
							sotObj.addProperty("paidAmount", sot.getPaidAmount());
							sotObj.addProperty("pendingAmount", sot.getPendingAmount());
							sotObj.addProperty("totalAmount", sot.getTotalAmount());
							sotObj.addProperty("type", sot.isType());
							sotObj.addProperty("returnTrays", sot.getReturnTrays());
							sotObj.addProperty("discountAmount", sot.getDiscountAmount());
							if(sot.getDate() != null)
								sotObj.addProperty("date", DateUtils.format(sot.getDate()));

							if(sot.isDiscount())
								sotObj.addProperty("paymentMethod", "Discount");
							else if(sot.isAdvance())
								sotObj.addProperty("paymentMethod", "Advance");
							else
								sotObj.addProperty("paymentMethod", "Cash");

							sotArray.add(sotObj);
						}
					}
				}
				soObj.add("saleOrderTransaction", sotArray);
				soArray.add(soObj);
			}
			return Response.ok(new Gson().toJson(soArray)).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to get SOT from SO ids. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

	@POST
	@UnitOfWork
	@Path("advance")
	public Response advancePo(@Auth AuthUser auth, @QueryParam("soids") List<Long> soIds) {
		try {
			for (Long id : soIds) {
				SaleOrder so = soController.getById(id);
				for (Advance adv : so.getBuyer().getAdvance()) {
					if(adv.isDelete() == false && adv.isComplete() == false) {
						List<Long> ids = new ArrayList<Long>();
						ids.add(so.getId());
						sotController.save(ids, null, DateUtils.format(adv.getDate()), adv.getPendingAmount().toString(), "0.00", adv.getId());
					}
				}
				return Response.ok(JsonUtils.getSuccessJson("Advance payment(s) recorded successfully...")).build();
			}
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson("Incorrect Input...")).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to make advance payment. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

	@POST
	@UnitOfWork
	@Path("delete/{id}")
	public Response delete(@Auth AuthUser auth, @PathParam("id") Long id) {
		try {
			String str = soController.delete(id);
			return Response.ok(JsonUtils.getSuccessJson(str)).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to delete sale order. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}
}
