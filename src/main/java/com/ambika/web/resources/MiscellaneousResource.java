package com.ambika.web.resources;

import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.ambika.web.auth.AuthUser;
import com.ambika.web.controller.MiscellaneousController;
import com.ambika.web.core.MiscellaneousAccount;
import com.ambika.web.utils.Dao;
import com.ambika.web.utils.JsonUtils;
import com.ambika.web.utils.async.LoggerMail;

@Path("/miscellaneous")
@Produces(MediaType.APPLICATION_JSON)
public class MiscellaneousResource {
	private static final Logger logger = Logger.getLogger((MiscellaneousResource.class).getName());
	private MiscellaneousController maController;

	public MiscellaneousResource(Dao dao) {
		this.maController = new MiscellaneousController(dao);
	}

	@GET
	@UnitOfWork
	public Response findAll(@Auth AuthUser auth) {
		List<MiscellaneousAccount> list = maController.findAll();
		return Response.ok(JsonUtils.getJson(list)).build();
	}

	@POST
	@UnitOfWork
	public Response save(@Auth AuthUser auth, MiscellaneousAccount miscellaneous) {
		try {
			miscellaneous = maController.save(miscellaneous);
			return Response.ok(JsonUtils.getJson(miscellaneous)).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to save miscellaneous account. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

	@GET
	@UnitOfWork
	@Path("{type}")
	public Response findByType(@Auth AuthUser auth, @PathParam("type") String type) {
		List<MiscellaneousAccount> list;
		try {
			list = maController.findByType(type);
			return Response.ok(JsonUtils.getJson(list)).build();
		} catch (Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to get miscellaneous account. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}

	}

	@POST
	@UnitOfWork
	@Path("delete/{id}")
	public Response delete(@Auth AuthUser auth, @PathParam("id") Long id) {
		try {
			String str = maController.delete(id);
			return Response.ok(JsonUtils.getSuccessJson(str)).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to delete miscellaneous account. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

}