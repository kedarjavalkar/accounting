package com.ambika.web.resources;

import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.ambika.web.auth.AuthUser;
import com.ambika.web.controller.AdvanceController;
import com.ambika.web.controller.PurchaseOrderController;
import com.ambika.web.controller.transaction.PurchaseOrderTransactionController;
import com.ambika.web.core.Advance;
import com.ambika.web.core.PurchaseOrder;
import com.ambika.web.core.transaction.PurchaseOrderTransaction;
import com.ambika.web.utils.Dao;
import com.ambika.web.utils.DateUtils;
import com.ambika.web.utils.JsonUtils;
import com.ambika.web.utils.RoleUtils;
import com.ambika.web.utils.async.LoggerMail;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Path("/report")
@Produces(MediaType.APPLICATION_JSON)
public class ReportResource {
	private static final Logger logger = Logger.getLogger((ReportResource.class).getName());
	private PurchaseOrderController poController;
	private AdvanceController advanceController;
	private PurchaseOrderTransactionController potController;

	public ReportResource(Dao dao) {
		this.poController = new PurchaseOrderController(dao);
		this.advanceController = new AdvanceController(dao);
		this.potController = new PurchaseOrderTransactionController(dao);
	}

	@GET
	@UnitOfWork
	@Path("farmer")
	public Response search(@Auth AuthUser auth, @QueryParam("farmerid") Long farmerId, 
			@QueryParam("from") String from, @QueryParam("to") String to) {
		JsonObject rootJson = new JsonObject();
		try {
			JsonArray poArray = new JsonArray();
			for (PurchaseOrder purchaseOrder : poController.search(farmerId, null, from, to)) {
				JsonObject jsonObject = new JsonObject();
				jsonObject.addProperty("id", purchaseOrder.getId());
				if(purchaseOrder.getDate() != null)
					jsonObject.addProperty("date", DateUtils.format(purchaseOrder.getDate()));
				jsonObject.addProperty("name", purchaseOrder.getFarmer().getName());
				jsonObject.addProperty("weight", purchaseOrder.getWeight());
				jsonObject.addProperty("tray", RoleUtils.validate(auth, purchaseOrder.getTray().toString()));
				jsonObject.addProperty("amount", purchaseOrder.getAmount());
				jsonObject.addProperty("pendingAmount", purchaseOrder.getPendingAmount());
				jsonObject.addProperty("discountAmount", purchaseOrder.getDiscountAmount());

				BigDecimal advancePending = new BigDecimal("0.00");
				for (Advance adv : purchaseOrder.getFarmer().getAdvance()) {
					if(adv.isDelete() == false) {
						advancePending = advancePending.add(adv.getPendingAmount());
					}
				}
				jsonObject.addProperty("pendingAdvance", advancePending);
				poArray.add(jsonObject);
			}

			JsonArray advanceArray = new JsonArray();
			for (Advance advance: advanceController.search(farmerId, from, to)) {
				JsonObject jsonObject = new JsonObject();
				jsonObject.addProperty("name", advance.getFarmer().getName());
				jsonObject.addProperty("pendingAmount", advance.getPendingAmount());
				jsonObject.addProperty("totalAmount", advance.getTotalAmount());
				if(advance.getDate() != null)
					jsonObject.addProperty("date", DateUtils.format(advance.getDate()));
				advanceArray.add(jsonObject);
			}

			JsonArray potArray = new JsonArray();
			for (PurchaseOrderTransaction pot : potController.search(farmerId, from, to)) {
				JsonObject jsonObject = new JsonObject();
				jsonObject.addProperty("id", pot.getId());
				jsonObject.addProperty("paidAmount", pot.getPaidAmount());
				jsonObject.addProperty("pendingAmount", pot.getPendingAmount());
				jsonObject.addProperty("discountAmount", pot.getDiscountAmount());
				jsonObject.addProperty("totalAmount", pot.getTotalAmount());
				jsonObject.addProperty("type", pot.isType());
				jsonObject.addProperty("name", pot.getPurchaseOrder().getFarmer().getName());
				if(pot.getDate() != null)	
					jsonObject.addProperty("date", DateUtils.format(pot.getDate()));
				if(pot.getDate() != null)
					jsonObject.addProperty("date", DateUtils.format(pot.getDate()));

				if(pot.isDiscount())
					jsonObject.addProperty("paymentMethod", "Discount");
				else if(pot.isAdvance())
					jsonObject.addProperty("paymentMethod", "Advance");
				else
					jsonObject.addProperty("paymentMethod", "Cash");
				potArray.add(jsonObject);
			}

			rootJson.add("po", poArray);
			rootJson.add("adv", advanceArray);
			rootJson.add("pot", potArray);
			return Response.ok(new Gson().toJson(rootJson)).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to get farmer personal account. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

}
