package com.ambika.web.resources;

import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.ambika.web.auth.AuthUser;
import com.ambika.web.controller.FarmerController;
import com.ambika.web.core.Farmer;
import com.ambika.web.utils.Dao;
import com.ambika.web.utils.JsonUtils;
import com.ambika.web.utils.async.LoggerMail;

@Path("/farmer")
@Produces(MediaType.APPLICATION_JSON)
public class FarmerResource {
	private static final Logger logger = Logger.getLogger((FarmerResource.class).getName());
	private FarmerController farmerController;

	public FarmerResource(Dao dao) {
		this.farmerController = new FarmerController(dao);
	}

	@GET
	@UnitOfWork
	public Response findAll(@Auth AuthUser auth) {
		List<Farmer> list = farmerController.findAll();
		return Response.ok(JsonUtils.getJson(list)).build();
	}

	@POST
	@UnitOfWork
	public Response save(@Auth AuthUser auth, Farmer farmer) {
		try {
			farmer = farmerController.save(farmer);
			return Response.ok(JsonUtils.getJson(farmer)).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to save farmer. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

	@POST
	@UnitOfWork
	@Path("delete/{id}")
	public Response delete(@Auth AuthUser auth, @PathParam("id") Long id) {
		try {
			String str = farmerController.delete(id);
			return Response.ok(JsonUtils.getSuccessJson(str)).build();
		} catch(Exception ex) {
			logger.log(Level.SEVERE, "Failed to delete farmer. Oops: " + ex.toString());
			LoggerMail.instance().send(ex);
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

}