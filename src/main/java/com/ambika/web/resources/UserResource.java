package com.ambika.web.resources;

import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.ambika.web.auth.AuthUser;
import com.ambika.web.controller.UserController;
import com.ambika.web.core.User;
import com.ambika.web.utils.Dao;
import com.ambika.web.utils.JsonUtils;
import com.ambika.web.utils.async.LoggerMail;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {
	private static final Logger logger = Logger.getLogger((UserResource.class).getName());
	private UserController userController;

	public UserResource(Dao dao) {
		this.userController = new UserController(dao);
	}

	@GET
	@UnitOfWork
	public Response findAll(@Auth AuthUser user) {
		List<User> list = userController.findAll();
		return Response.ok(JsonUtils.getJson(list)).build();
	}

	@POST
	@UnitOfWork
	public Response save(@Auth AuthUser auth, User user) {
		try {
			user = userController.save(user, true);
			return Response.ok(JsonUtils.getJson(user)).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to save user. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

	@POST
	@UnitOfWork
	@Path("login")
	public Response login(User user, @Context HttpServletResponse res) {
		try {
			boolean isValid = userController.login(user, res);
			if(isValid)
				return Response.status(Response.Status.OK)
						.entity(JsonUtils.getJson("<script type='text/javascript'>window.location.href = 'purchase.html'</script>")).build();
			else
				return Response.status(Status.BAD_REQUEST)
						.entity(JsonUtils.getErrorJson("Incorrect user name password...")).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to login user. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

	@POST
	@UnitOfWork
	@Path("logout")
	public Response logout(@Context HttpServletRequest req, @Context HttpServletResponse res) {
		try {
			userController.logout(req, res);
			return Response.status(Response.Status.OK)
					.entity(JsonUtils.getJson("<script type='text/javascript'>window.location.href = '/'</script>")).build();
		} catch(Exception ex) {
			LoggerMail.instance().send(ex);
			logger.log(Level.SEVERE, "Failed to logout user. Oops: " + ex.toString());
			return Response.status(Status.BAD_REQUEST).entity(JsonUtils.getErrorJson(ex.toString())).build();
		}
	}

}