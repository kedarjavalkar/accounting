package com.ambika.web;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.auth.AuthFactory;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.lifecycle.ServerLifecycleListener;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.server.AbstractServerFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import java.util.Date;
import java.util.EnumSet;
import java.util.TimeZone;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.hibernate.SessionFactory;

import com.ambika.web.auth.AuthUser;
import com.ambika.web.auth.CookieAuthFactory;
import com.ambika.web.auth.WebAuthenticator;
import com.ambika.web.core.Advance;
import com.ambika.web.core.BalanceSheet;
import com.ambika.web.core.Buyer;
import com.ambika.web.core.Farmer;
import com.ambika.web.core.MiscellaneousAccount;
import com.ambika.web.core.PurchaseOrder;
import com.ambika.web.core.SaleOrder;
import com.ambika.web.core.User;
import com.ambika.web.core.transaction.AdvanceTransaction;
import com.ambika.web.core.transaction.BalanceSheetTransaction;
import com.ambika.web.core.transaction.MiscellaneousAccountTransaction;
import com.ambika.web.core.transaction.PurchaseOrderTransaction;
import com.ambika.web.core.transaction.SaleOrderTransaction;
import com.ambika.web.resources.AdvanceResource;
import com.ambika.web.resources.BalanceSheetResource;
import com.ambika.web.resources.BuyerResource;
import com.ambika.web.resources.FarmerResource;
import com.ambika.web.resources.MiscellaneousResource;
import com.ambika.web.resources.PurchaseOrderResource;
import com.ambika.web.resources.ReportResource;
import com.ambika.web.resources.SaleOrderResource;
import com.ambika.web.resources.UserResource;
import com.ambika.web.resources.transaction.BalanceSheetTransactionResource;
import com.ambika.web.resources.transaction.MiscellaneousTransactionResource;
import com.ambika.web.resources.transaction.PurchaseOrderTransactionResource;
import com.ambika.web.resources.transaction.SaleOrderTransactionResource;
import com.ambika.web.utils.ApplicationContext;
import com.ambika.web.utils.Dao;
import com.ambika.web.utils.DateUtils;
import com.ambika.web.utils.async.OpenCloseBalanceSheet;
import com.ambika.web.utils.async.SiteIdling;

public class WebApplication extends Application<WebConfiguration> {
	public static void main(String[] args) throws Exception {
		new WebApplication().run(args);
	}

	@Override
	public void initialize(Bootstrap<WebConfiguration> bootstrap) {
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		bootstrap.addBundle(new MigrationsBundle<WebConfiguration>() {
			@Override
			public DataSourceFactory getDataSourceFactory(WebConfiguration configuration) {
				return configuration.getDataSourceFactory();
			}
		});
		bootstrap.addBundle(hibernate);
		bootstrap.addBundle(new AssetsBundle("/webapps", "/", "index.html"));
	}

	// Mapping entities to hibernate
	private final HibernateBundle<WebConfiguration> hibernate = new HibernateBundle<WebConfiguration> (
			Farmer.class, PurchaseOrder.class, PurchaseOrderTransaction.class, Buyer.class, SaleOrder.class, SaleOrderTransaction.class,
			MiscellaneousAccount.class, MiscellaneousAccountTransaction.class, BalanceSheet.class, BalanceSheetTransaction.class,
			Advance.class, AdvanceTransaction.class, User.class) {
		@Override
		public DataSourceFactory getDataSourceFactory(WebConfiguration configuration) {
			return configuration.getDataSourceFactory();
		}
	};

	@Override
	public void run(final WebConfiguration configuration, Environment environment) {
		((AbstractServerFactory) configuration.getServerFactory()).setJerseyRootPath("/api/*");

		// Application Context
		ApplicationContext.instance().setup(configuration.getMailUser(), configuration.getMailPassword(),
				configuration.getCookieName(), configuration.getCookieAge());

		// Hibernate session factory 
		final SessionFactory sessionFactory = hibernate.getSessionFactory();

		// Mapping session factory to Dao
		final Dao dao = new Dao(sessionFactory);

		// Resource registration with hibernate session enabled dao
		environment.jersey().register(new UserResource(dao));
		environment.jersey().register(new FarmerResource(dao));
		environment.jersey().register(new PurchaseOrderResource(dao));
		environment.jersey().register(new PurchaseOrderTransactionResource(dao));
		environment.jersey().register(new BalanceSheetResource(dao));
		environment.jersey().register(new MiscellaneousResource(dao));
		environment.jersey().register(new MiscellaneousTransactionResource(dao));
		environment.jersey().register(new BalanceSheetTransactionResource(dao));
		environment.jersey().register(new BuyerResource(dao));
		environment.jersey().register(new SaleOrderResource(dao));
		environment.jersey().register(new SaleOrderTransactionResource(dao));
		environment.jersey().register(new AdvanceResource(dao));
		environment.jersey().register(new ReportResource(dao));

		// Cookie Authenticator
		environment.jersey().register(AuthFactory.binder(
				new CookieAuthFactory<>(new WebAuthenticator(dao), "Cookie Authenticator", AuthUser.class)));

		// CORS
		final FilterRegistration.Dynamic cors = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
		cors.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
		cors.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_HEADERS_HEADER, "*");
		cors.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_REQUEST_HEADERS_HEADER, "*");
		cors.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_CREDENTIALS_HEADER, "true");
		cors.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_METHODS_HEADER, "OPTIONS, GET, POST");
		cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/api/*");

		environment.lifecycle().addServerLifecycleListener(new ServerLifecycleListener() {
			@Override
			public void serverStarted(Server server) {
				new Timer().schedule(new SiteIdling(configuration.getUrl()), new Date(), TimeUnit.HOURS.toMillis(3));
				new Timer().schedule(new OpenCloseBalanceSheet(configuration.getUrl()), 
						DateUtils.getMidnight(), TimeUnit.DAYS.toMillis(1));
			}
		});
	}
}
