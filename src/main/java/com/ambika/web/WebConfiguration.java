package com.ambika.web;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WebConfiguration extends Configuration {
	@Valid
	@NotNull
	@JsonProperty("database")
	private DataSourceFactory database = new DataSourceFactory();

	@Valid
	@NotNull
	@JsonProperty("url")
	private String url;

	@JsonProperty("mail.user")
	private String mailUser;

	@JsonProperty("mail.password")
	private String mailPassword;

	@Valid
	@NotNull
	@JsonProperty("cookie.name")
	private String cookieName;

	@Valid
	@NotNull
	@JsonProperty("cookie.age")
	private Integer cookieAge;


	public DataSourceFactory getDataSourceFactory() {
		return database;
	}

	public void setDatabase(DataSourceFactory database) {
		this.database = database;
	}

	public String getUrl() {
		return url;
	}

	public String getMailUser() {
		return mailUser;
	}

	public String getMailPassword() {
		return mailPassword;
	}

	public String getCookieName() {
		return cookieName;
	}

	public Integer getCookieAge() {
		return cookieAge;
	}
}
