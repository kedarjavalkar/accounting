package com.ambika.web.auth;

import io.dropwizard.auth.AuthFactory;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.DefaultUnauthorizedHandler;
import io.dropwizard.auth.UnauthorizedHandler;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ambika.web.utils.ApplicationContext;
import com.google.common.base.Optional;

public final class CookieAuthFactory<T> extends AuthFactory<String, T> {
	private static final Logger LOGGER = LoggerFactory.getLogger(CookieAuthFactory.class);

	private final boolean required;
	private final Class<T> generatedClass;
	private final String realm;
	private String prefix = "Basic";
	private final String cookieName = ApplicationContext.instance().getCookieName();
	private final Integer cookieAge = ApplicationContext.instance().getCookieAge();
	private UnauthorizedHandler unauthorizedHandler = new DefaultUnauthorizedHandler();

	@Context
	private HttpServletRequest request;
	@Context
	private HttpServletResponse response;

	public CookieAuthFactory(final Authenticator<String, T> authenticator,
			final String realm,
			final Class<T> generatedClass) {
		super(authenticator);
		this.required = false;
		this.realm = realm;
		this.generatedClass = generatedClass;
	}

	private CookieAuthFactory(final boolean required,
			final Authenticator<String, T> authenticator,
			final String realm,
			final Class<T> generatedClass) {
		super(authenticator);
		this.required = required;
		this.realm = realm;
		this.generatedClass = generatedClass;
	}

	public CookieAuthFactory<T> prefix(String prefix) {
		this.prefix = prefix;
		return this;
	}

	public CookieAuthFactory<T> responseBuilder(UnauthorizedHandler unauthorizedHandler) {
		this.unauthorizedHandler = unauthorizedHandler;
		return this;
	}

	@Override
	public AuthFactory<String, T> clone(boolean required) {
		return new CookieAuthFactory<T>(required, authenticator(), this.realm, this.generatedClass).prefix(prefix).responseBuilder(unauthorizedHandler);
	}

	@Override
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	@Override
	public T provide() {
		if (request != null) {
			try {
				if(request.getCookies() != null) {
					for (Cookie cookie : request.getCookies()) {
						if(cookieName.equalsIgnoreCase(cookie.getName())) {
							final Optional<T> result = authenticator().authenticate(cookie.getValue());
							if (result.isPresent()) {
								cookie.setMaxAge(cookieAge);
								cookie.setPath("/");
								response.addCookie(cookie);
								return result.get();
							} else {
								cookie.setMaxAge(0);
								cookie.setPath("/");
								response.addCookie(cookie);
							}
						}
					}
				}

			} catch (IllegalArgumentException e) {
				LOGGER.warn("Error decoding credentials", e);
			} catch (AuthenticationException e) {
				LOGGER.warn("Error authenticating credentials", e);
				throw new InternalServerErrorException();
			}

		}

		if (required) {
			throw new WebApplicationException(Response.status(Status.UNAUTHORIZED)
					.entity("<script type=\"text/javascript\">window.location.href=\"/\"</script>")
					.type(MediaType.TEXT_HTML_TYPE).build());
		}

		return null;
	}

	@Override
	public Class<T> getGeneratedClass() {
		return generatedClass;
	}


}
