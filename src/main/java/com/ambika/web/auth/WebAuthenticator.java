package com.ambika.web.auth;

import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;

import com.ambika.web.core.User;
import com.ambika.web.dao.UserDao;
import com.ambika.web.utils.Dao;
import com.google.common.base.Optional;

public class WebAuthenticator implements Authenticator<String, AuthUser> {

	UserDao userDao;

	public WebAuthenticator(Dao dao) {
		this.userDao = dao.getUserDao();
	}

	@Override
	public Optional<AuthUser> authenticate(String str) throws AuthenticationException {
		try {
			String[] strArray = str.split("~");
			if(strArray.length == 2) {
				Long id = Long.parseLong(strArray[0]);
				String md5 = strArray[1];
				User user = userDao.getById(id);
				if(user != null)
					if(user.getMd5().equals(md5))
						return Optional.of(new AuthUser(user.getRole()));
			}
		} catch(Exception ex) {
			// Do nothing
		}

		return Optional.absent();
	}


}