package com.ambika.web.controller;

import java.util.Date;
import java.util.List;

import com.ambika.web.core.Advance;
import com.ambika.web.core.Buyer;
import com.ambika.web.core.SaleOrder;
import com.ambika.web.core.transaction.AdvanceTransaction;
import com.ambika.web.core.transaction.BalanceSheetTransaction;
import com.ambika.web.core.transaction.SaleOrderTransaction;
import com.ambika.web.dao.AdvanceDao;
import com.ambika.web.dao.AdvanceTransactionDao;
import com.ambika.web.dao.BuyerDao;
import com.ambika.web.dao.SaleOrderDao;
import com.ambika.web.dao.transaction.BalanceSheetTransactionDao;
import com.ambika.web.dao.transaction.SaleOrderTransactionDao;
import com.ambika.web.exception.AmbikaException;
import com.ambika.web.utils.Dao;

public class BuyerController {

	private BuyerDao buyerDao;
	private AdvanceDao advanceDao;
	private AdvanceTransactionDao atDao;
	private SaleOrderDao soDao;
	private SaleOrderTransactionDao sotDao;
	private BalanceSheetTransactionDao bstDao;

	public BuyerController(Dao dao) {
		this.buyerDao = dao.getBuyerDao();

		this.advanceDao = dao.getAdvanceDao();
		this.atDao = dao.getAdvanceTransactionDao();
		this.soDao = dao.getSaleOrderDao();
		this.sotDao = dao.getSaleOrderTransactionDao();
		this.bstDao = dao.getBalanceSheetTransactionDao();
	}

	public Buyer save(Buyer buyer) throws Exception {
		if(buyer.getName() == null || buyer.getName().trim().isEmpty())
			throw new AmbikaException("Name cannot be empty...");

		buyer.setName(buyer.getName().trim());
		return buyerDao.save(buyer);
	}

	public List<Buyer> findAll() {
		return buyerDao.findAll();
	}

	public Buyer getById(Long id) throws Exception {
		if(id == null)
			throw new AmbikaException("Buyer id cannot be empty...");
		return buyerDao.getById(id);
	}

	public String delete(Long id) throws Exception {
		String str = "";

		Buyer buyer = getById(id);
		if(buyer == null)
			throw new AmbikaException("Buyer not found with Id: " + id);

		Date now = new Date();
		buyer.setDelete(true);
		buyer.setDeleteDate(now);
		buyerDao.save(buyer);

		for (Advance advance : buyer.getAdvance()) {
			advance.setDelete(true);
			advance.setDeleteDate(now);
			advanceDao.save(advance);
			for (AdvanceTransaction adt : advance.getAdvanceTransaction()) {
				adt.setDelete(true);
				adt.setDeleteDate(now);
				atDao.save(adt);
			}
		}

		for (SaleOrder so : buyer.getSaleOrders()) {
			so.setDelete(true);
			so.setDeleteDate(now);
			soDao.save(so);
			for (SaleOrderTransaction sot : so.getSaleOrderTransactions()) {
				sot.setDelete(true);
				sot.setDeleteDate(now);
				sotDao.save(sot);
				for (BalanceSheetTransaction bst : bstDao.findBySot(sot)) {
					bst.setDelete(true);
					bst.setDeleteDate(now);
					bstDao.save(bst);
				}
			}
		}

		str = "Deleted buyer " + buyer.getName() + "...";
		return str;
	}
}
