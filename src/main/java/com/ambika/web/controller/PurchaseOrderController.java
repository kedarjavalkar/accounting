package com.ambika.web.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ambika.web.core.Advance;
import com.ambika.web.core.Farmer;
import com.ambika.web.core.PurchaseOrder;
import com.ambika.web.core.transaction.AdvanceTransaction;
import com.ambika.web.core.transaction.BalanceSheetTransaction;
import com.ambika.web.core.transaction.PurchaseOrderTransaction;
import com.ambika.web.dao.AdvanceDao;
import com.ambika.web.dao.AdvanceTransactionDao;
import com.ambika.web.dao.BalanceSheetDao;
import com.ambika.web.dao.FarmerDao;
import com.ambika.web.dao.PurchaseOrderDao;
import com.ambika.web.dao.transaction.BalanceSheetTransactionDao;
import com.ambika.web.dao.transaction.PurchaseOrderTransactionDao;
import com.ambika.web.exception.AmbikaException;
import com.ambika.web.utils.Dao;
import com.ambika.web.utils.DateUtils;

public class PurchaseOrderController {

	private FarmerDao farmerDao;
	private PurchaseOrderDao poDao;
	private PurchaseOrderTransactionDao potDao;
	private BalanceSheetDao bsDao;
	private BalanceSheetTransactionDao bstDao;
	private AdvanceDao advDao;
	private AdvanceTransactionDao advtDao;

	public PurchaseOrderController(Dao dao) {
		this.farmerDao = dao.getFarmerDao();
		this.poDao = dao.getPurchaseOrderDao();
		this.potDao = dao.getPurchaseOrderTransactionDao();
		this.bsDao = dao.getBalanceSheetDao();
		this.bstDao = dao.getBalanceSheetTransactionDao();
		this.advDao = dao.getAdvanceDao();
		this.advtDao = dao.getAdvanceTransactionDao();
	}

	public PurchaseOrder getById(Long id) throws Exception {
		if(id == null)
			throw new AmbikaException("Purchase Order id cannot be empty");
		return poDao.getById(id);
	}

	public PurchaseOrder save(PurchaseOrder purchaseOrder) throws Exception {
		if(purchaseOrder.getFarmer() == null || purchaseOrder.getFarmer().getId() == null)
			throw new AmbikaException("Farmer not found");

		Farmer farmer = farmerDao.getById(purchaseOrder.getFarmer().getId());
		if(farmer == null)
			throw new AmbikaException("Famer not found");

		Date dt = new Date();
		if(purchaseOrder.getDate() == null)
			throw new AmbikaException("Date can not be empty");

		dt = purchaseOrder.getDate();
		if(bsDao.findByDate(dt) == null)
			throw new AmbikaException("Balance Sheet not found for date " + DateUtils.format(dt));

		purchaseOrder.setFarmer(farmer);
		purchaseOrder.setDiscountAmount(new BigDecimal("0.00"));
		return poDao.save(purchaseOrder);
	}

	public List<PurchaseOrder> findAll() {
		return poDao.findAll();
	}

	public List<PurchaseOrder> search(Long farmerId, String status, String fromDate, String toDate) throws Exception {
		Farmer farmerObj = null;
		Date from = null;
		Date to = null;

		if(farmerId != null)
			farmerObj = farmerDao.getById(farmerId);
		if(fromDate != null)
			from = DateUtils.format(fromDate);
		if(toDate != null)
			to = DateUtils.format(toDate);

		return poDao.search(farmerObj, status, from, to);
	}

	public List<PurchaseOrder> getPoByIds(List<Long> poIds) {
		List<PurchaseOrder> list = new ArrayList<PurchaseOrder>();
		for (Long id : poIds) {
			PurchaseOrder po = poDao.getById(id);
			if(po != null)
				list.add(po);
		}
		return list;
	}

	public String delete(Long id) throws Exception {

		PurchaseOrder po = getById(id);
		if(po == null)
			throw new AmbikaException("Purchase order not found with Id: " + id);

		Date now = new Date();
		po.setDelete(true);
		po.setDeleteDate(now);
		poDao.save(po);

		for (PurchaseOrderTransaction pot : po.getPurchaseOrderTransactions()) {
			pot.setDelete(true);
			pot.setDeleteDate(now);
			potDao.save(pot);

			for (AdvanceTransaction advt : pot.getAdvanceTransaction()) {
				advt.setDelete(true);
				advt.setDeleteDate(now);
				advtDao.save(advt);

				Advance adv = advt.getAdvance();
				adv.setPendingAmount(adv.getPendingAmount().add(advt.getAmount()));
				adv.setComplete(false);
				advDao.save(adv);
			}
			for (BalanceSheetTransaction bst : bstDao.findByPot(pot)) {
				bst.setDelete(true);
				bst.setDeleteDate(now);
				bstDao.save(bst);
			}
		}

		return "Purchase order deleted...";
	}

}
