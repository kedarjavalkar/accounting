package com.ambika.web.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ambika.web.core.Advance;
import com.ambika.web.core.Buyer;
import com.ambika.web.core.SaleOrder;
import com.ambika.web.core.transaction.AdvanceTransaction;
import com.ambika.web.core.transaction.BalanceSheetTransaction;
import com.ambika.web.core.transaction.SaleOrderTransaction;
import com.ambika.web.dao.AdvanceDao;
import com.ambika.web.dao.AdvanceTransactionDao;
import com.ambika.web.dao.BalanceSheetDao;
import com.ambika.web.dao.BuyerDao;
import com.ambika.web.dao.SaleOrderDao;
import com.ambika.web.dao.transaction.BalanceSheetTransactionDao;
import com.ambika.web.dao.transaction.SaleOrderTransactionDao;
import com.ambika.web.exception.AmbikaException;
import com.ambika.web.utils.Dao;
import com.ambika.web.utils.DateUtils;

public class SaleOrderController {

	private BuyerDao buyerDao;
	private SaleOrderDao soDao;
	private SaleOrderTransactionDao sotDao;
	private BalanceSheetDao bsDao;
	private BalanceSheetTransactionDao bstDao;
	private AdvanceDao advDao;
	private AdvanceTransactionDao advtDao;

	public SaleOrderController(Dao dao) {
		this.buyerDao = dao.getBuyerDao();
		this.soDao = dao.getSaleOrderDao();
		this.sotDao = dao.getSaleOrderTransactionDao();
		this.bsDao = dao.getBalanceSheetDao();
		this.bstDao = dao.getBalanceSheetTransactionDao();
		this.advDao = dao.getAdvanceDao();
		this.advtDao = dao.getAdvanceTransactionDao();	
	}

	public SaleOrder getById(Long id) {
		return soDao.getById(id);
	}

	public SaleOrder save(SaleOrder saleOrder) throws Exception {
		if(saleOrder.getBuyer() == null || saleOrder.getBuyer().getId() == null)
			throw new AmbikaException("Buyer not found");

		Buyer buyer = buyerDao.getById(saleOrder.getBuyer().getId());
		if(buyer == null)
			throw new AmbikaException("Buyer not found");

		Date dt = new Date();
		if(saleOrder.getDate() == null)
			throw new AmbikaException("Date can not be empty");

		dt = saleOrder.getDate();
		if(bsDao.findByDate(dt) == null)
			throw new AmbikaException("Balance Sheet not found for date " + DateUtils.format(dt));

		saleOrder.setBuyer(buyer);
		saleOrder.setDiscountAmount(new BigDecimal("0.00"));
		saleOrder.setPendingTrays(saleOrder.getTray());
		return soDao.save(saleOrder);
	}

	public List<SaleOrder> findAll() {
		return soDao.findAll();
	}

	public List<SaleOrder> search(Long buyerId, String status, String fromDate, String toDate) throws Exception {
		Buyer buyerObj = null;
		Date from = null;
		Date to = null;

		if(buyerId != null)
			buyerObj = buyerDao.getById(buyerId);
		if(fromDate != null)
			from = DateUtils.format(fromDate);
		if(toDate != null)
			to = DateUtils.format(toDate);

		return soDao.search(buyerObj, status, from, to);
	}

	public List<SaleOrder> getSoByIds(List<Long> poIds) throws Exception {
		List<SaleOrder> list = new ArrayList<SaleOrder>();
		for (Long id : poIds) {
			SaleOrder so = soDao.getById(id);
			if(so != null)
				list.add(so);
		}
		return list;
	}

	public String delete(Long id) throws Exception {

		SaleOrder so = getById(id);
		if(so == null)
			throw new AmbikaException("Sale order not found with Id: " + id);

		Date now = new Date();
		so.setDelete(true);
		so.setDeleteDate(now);
		soDao.save(so);

		for (SaleOrderTransaction sot : so.getSaleOrderTransactions()) {
			sot.setDelete(true);
			sot.setDeleteDate(now);
			sotDao.save(sot);

			for (AdvanceTransaction advt : sot.getAdvanceTransaction()) {
				advt.setDelete(true);
				advt.setDeleteDate(now);
				advtDao.save(advt);

				Advance adv = advt.getAdvance();
				adv.setPendingAmount(adv.getPendingAmount().add(advt.getAmount()));
				adv.setComplete(false);
				advDao.save(adv);
			}
			for (BalanceSheetTransaction bst : bstDao.findBySot(sot)) {
				bst.setDelete(true);
				bst.setDeleteDate(now);
				bstDao.save(bst);
			}
		}

		return "Sale order deleted...";
	}

	public boolean updateTray(SaleOrder so) throws Exception {
		Date dt = null;

		if(so.getId() == null)
			throw new AmbikaException("Sale order id cannot be empty");

		if(so.getPendingTrays() == null)
			throw new AmbikaException("Tray cannot be empty");

		if(so.getDate() == null)
			dt = new Date();
		else
			dt = so.getDate();

		SaleOrder soDb = getById(so.getId());
		if(soDb == null)
			throw new AmbikaException("Sale order not found with id: " + so.getId());

		if(so.getPendingTrays().compareTo(soDb.getPendingTrays()) == 1)
			throw new AmbikaException("Number of trays returned are higher than pending trays");

		soDb.setPendingTrays(soDb.getPendingTrays().subtract((so.getPendingTrays())));
		soDao.save(soDb);

		SaleOrderTransaction sot = new SaleOrderTransaction();
		sot.setDate(dt);
		sot.setReturnTrays(so.getPendingTrays());
		sot.setPaidAmount(new BigDecimal("0.00"));
		sot.setPendingAmount(soDb.getPendingAmount());
		sot.setTotalAmount(soDb.getAmount());
		sot.setSaleOrder(soDb);
		sotDao.save(sot);

		return true;
	}

}
