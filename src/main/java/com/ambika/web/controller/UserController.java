package com.ambika.web.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;

import com.ambika.web.core.User;
import com.ambika.web.dao.UserDao;
import com.ambika.web.exception.AmbikaException;
import com.ambika.web.utils.ApplicationContext;
import com.ambika.web.utils.Dao;

public class UserController {
	private final static String COOKIE_NAME = ApplicationContext.instance().getCookieName();
	private final static Integer COOKIE_MAX_AGE = ApplicationContext.instance().getCookieAge();

	private UserDao userDao;

	public UserController(Dao dao) {
		this.userDao = dao.getUserDao();
	}

	public User getById(Long id) throws Exception {
		if(id == null)
			throw new AmbikaException("User id cannot be empty");
		return userDao.getById(id);
	}

	public List<User> findAll() {
		return userDao.findAll();
	}

	public User save(User user, boolean newUser) throws Exception {
		if(user.getEmail() == null || user.getEmail().trim().isEmpty())
			throw new AmbikaException("User Email cannot be empty");

		if(user.getPassword() == null || user.getPassword().isEmpty())
			throw new AmbikaException("User Password cannot be empty");

		if(user.getRole() == null)
			throw new AmbikaException("User Role cannot be empty");

		user.setEmail(user.getEmail().trim());

		findByEmail(user.getEmail(), newUser);

		if(!user.getRole().equals(User.Role.ADMIN) || user.getRole().equals(User.Role.USER))
			throw new AmbikaException("Failed to associate role... Choose role from : " +
					User.Role.ADMIN + ", " + User.Role.USER);

		return userDao.save(user);
	}

	public User findByEmail(String email, boolean newUser) throws Exception {
		User user = userDao.findByEmail(email);
		if(newUser)
			if(user != null)
				throw new AmbikaException("User with " + email + " email already exists");

		return user;
	}

	public boolean login(User user, HttpServletResponse response) throws Exception {
		boolean isValid = false;
		if(user.getEmail() == null || user.getEmail().isEmpty())
			throw new AmbikaException("User name cannot be empty");

		if(user.getPassword() == null || user.getPassword().isEmpty())
			throw new AmbikaException("Password cannot be empty");

		User userObj = findByEmail(user.getEmail(), false);
		if(userObj != null) {
			if(userObj.isDelete() == false) {
				if(userObj.getPassword().equals(user.getPassword())) {
					String md5 = generateMd5(userObj.getEmail() + userObj.getPassword() + new Date());
					String cookieValue = userObj.getId() + "~" + md5;
					userObj.setMd5(md5);
					userDao.save(userObj);
					setUserCookie(response, cookieValue, COOKIE_MAX_AGE);
					isValid = true;
				}
			}
		}

		return isValid;
	}

	public void logout(HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			if(request.getCookies() != null) {
				for (Cookie cookie : request.getCookies()) {
					if(COOKIE_NAME.equalsIgnoreCase(cookie.getName())) {
						String id = cookie.getValue().split("~")[0];
						userDao.getById(Long.parseLong(id)).setMd5(null);
					}
				}
			}
		} catch(Exception ex) {
			// do nothing
		} finally {
			setUserCookie(response, "", 0);
		}
	}

	private String generateMd5(String content) throws Exception {
		return DigestUtils.md5Hex(content);
	}

	private void setUserCookie(HttpServletResponse response, String cookieValue, Integer age) throws Exception {
		Cookie cookie = new Cookie(COOKIE_NAME, cookieValue);
		cookie.setMaxAge(age);
		cookie.setPath("/");
		response.addCookie(cookie);
	}
}
