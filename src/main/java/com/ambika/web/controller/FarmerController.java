package com.ambika.web.controller;

import java.util.Date;
import java.util.List;

import com.ambika.web.core.Advance;
import com.ambika.web.core.Farmer;
import com.ambika.web.core.PurchaseOrder;
import com.ambika.web.core.transaction.AdvanceTransaction;
import com.ambika.web.core.transaction.BalanceSheetTransaction;
import com.ambika.web.core.transaction.PurchaseOrderTransaction;
import com.ambika.web.dao.AdvanceDao;
import com.ambika.web.dao.AdvanceTransactionDao;
import com.ambika.web.dao.FarmerDao;
import com.ambika.web.dao.PurchaseOrderDao;
import com.ambika.web.dao.transaction.BalanceSheetTransactionDao;
import com.ambika.web.dao.transaction.PurchaseOrderTransactionDao;
import com.ambika.web.exception.AmbikaException;
import com.ambika.web.utils.Dao;

public class FarmerController {

	private FarmerDao farmerDao;
	private AdvanceDao advanceDao;
	private AdvanceTransactionDao atDao;
	private PurchaseOrderDao poDao;
	private PurchaseOrderTransactionDao potDao;
	private BalanceSheetTransactionDao bstDao;

	public FarmerController(Dao dao) {
		this.farmerDao = dao.getFarmerDao();
		this.advanceDao = dao.getAdvanceDao();
		this.atDao = dao.getAdvanceTransactionDao();
		this.poDao = dao.getPurchaseOrderDao();
		this.potDao = dao.getPurchaseOrderTransactionDao();
		this.bstDao = dao.getBalanceSheetTransactionDao();
	}

	public Farmer save(Farmer farmer) throws Exception {
		if(farmer.getName() == null || farmer.getName().trim().isEmpty())
			throw new AmbikaException("Name cannot be empty...");

		farmer.setName(farmer.getName().trim());
		return farmerDao.save(farmer);
	}

	public List<Farmer> findAll() {
		return farmerDao.findAll();
	}

	public Farmer getById(Long id) throws Exception {
		if(id == null)
			throw new AmbikaException("Farmer id cannot be empty...");
		return farmerDao.getById(id);
	}

	public String delete(Long id) throws Exception {
		String str = "";
		Farmer farmer = getById(id);
		if(farmer == null)
			throw new AmbikaException("Farmer not found with Id: " + id);

		Date now = new Date();
		farmer.setDelete(true);
		farmer.setDeleteDate(now);
		farmerDao.save(farmer);

		for (Advance advance : farmer.getAdvance()) {
			advance.setDelete(true);
			advance.setDeleteDate(now);
			advanceDao.save(advance);
			for (AdvanceTransaction adt : advance.getAdvanceTransaction()) {
				adt.setDelete(true);
				adt.setDeleteDate(now);
				atDao.save(adt);
			}
		}

		for (PurchaseOrder po : farmer.getPurchaseOrders()) {
			po.setDelete(true);
			po.setDeleteDate(now);
			poDao.save(po);
			for (PurchaseOrderTransaction pot : po.getPurchaseOrderTransactions()) {
				pot.setDelete(true);
				pot.setDeleteDate(now);
				potDao.save(pot);
				for (BalanceSheetTransaction bst : bstDao.findByPot(pot)) {
					bst.setDelete(true);
					bst.setDeleteDate(now);
					bstDao.save(bst);
				}
			}
		}

		str = "Deleted farmer " + farmer.getName() + "...";
		return str;
	}

}
