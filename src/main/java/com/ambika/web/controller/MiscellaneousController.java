package com.ambika.web.controller;

import java.util.Date;
import java.util.List;

import com.ambika.web.core.MiscellaneousAccount;
import com.ambika.web.core.transaction.BalanceSheetTransaction;
import com.ambika.web.core.transaction.MiscellaneousAccountTransaction;
import com.ambika.web.dao.MiscellaneousAccountDao;
import com.ambika.web.dao.transaction.BalanceSheetTransactionDao;
import com.ambika.web.dao.transaction.MiscellaneousAccountTransactionDao;
import com.ambika.web.exception.AmbikaException;
import com.ambika.web.utils.Dao;

public class MiscellaneousController {

	private MiscellaneousAccountDao maDao;
	private MiscellaneousAccountTransactionDao matDao;
	private BalanceSheetTransactionDao bstDao;

	public MiscellaneousController(Dao dao) {
		this.maDao = dao.getMiscellaneousAccountDao();
		this.matDao = dao.getMiscellaneousAccountTransactionDao();
		this.bstDao = dao.getBalanceSheetTransactionDao();
	}

	public MiscellaneousAccount save(MiscellaneousAccount miscellaneousAccount) throws Exception {
		if(miscellaneousAccount.getName() == null || miscellaneousAccount.getName().trim().isEmpty())
			throw new AmbikaException("Name cannot be empty...");

		return maDao.save(miscellaneousAccount);
	}

	public List<MiscellaneousAccount> findAll() {
		return maDao.findAll();
	}

	public MiscellaneousAccount getById(Long id) throws Exception {
		if(id == null)
			throw new AmbikaException("Miscellaneous id cannot be empty...");
		return maDao.getById(id);
	}

	public List<MiscellaneousAccount> findByType(String type) throws Exception {
		if(type.equalsIgnoreCase("income")){
			return maDao.findByType(true);
		} else if(type.equalsIgnoreCase("expense")) {
			return maDao.findByType(false);
		} else {
			throw new AmbikaException("Failed to match type...");
		}
	}

	public String delete(Long id) throws Exception {
		String str = "";

		MiscellaneousAccount ma = getById(id);
		if(ma == null)
			throw new AmbikaException("Miscellaneous Account not found with id : " + id);

		String name = ma.getName();
		if(name.equalsIgnoreCase("Discount") || name.equalsIgnoreCase("Loss"))
			throw new AmbikaException("Deleting '" + name + "' is prohibited...");

		Date now = new Date();
		ma.setDelete(true);
		ma.setDeleteDate(now);
		maDao.save(ma);

		for (MiscellaneousAccountTransaction mat : ma.getMiscellaneousAccountTransaction()) {
			mat.setDelete(true);
			mat.setDeleteDate(now);
			matDao.save(mat);
			for (BalanceSheetTransaction bst : bstDao.findByMat(mat)) {
				bst.setDelete(true);
				bst.setDeleteDate(now);
				bstDao.save(bst);
			}
		}

		str = "Deleted miscellaneous account " + name + "...";
		return str;
	}
}
