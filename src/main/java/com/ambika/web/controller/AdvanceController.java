package com.ambika.web.controller;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.ambika.web.core.Advance;
import com.ambika.web.core.Buyer;
import com.ambika.web.core.Farmer;
import com.ambika.web.core.PurchaseOrder;
import com.ambika.web.core.SaleOrder;
import com.ambika.web.core.transaction.AdvanceTransaction;
import com.ambika.web.core.transaction.BalanceSheetTransaction;
import com.ambika.web.core.transaction.PurchaseOrderTransaction;
import com.ambika.web.core.transaction.SaleOrderTransaction;
import com.ambika.web.dao.AdvanceDao;
import com.ambika.web.dao.AdvanceTransactionDao;
import com.ambika.web.dao.BuyerDao;
import com.ambika.web.dao.FarmerDao;
import com.ambika.web.dao.PurchaseOrderDao;
import com.ambika.web.dao.SaleOrderDao;
import com.ambika.web.dao.transaction.BalanceSheetTransactionDao;
import com.ambika.web.dao.transaction.PurchaseOrderTransactionDao;
import com.ambika.web.dao.transaction.SaleOrderTransactionDao;
import com.ambika.web.exception.AmbikaException;
import com.ambika.web.utils.Dao;
import com.ambika.web.utils.DateUtils;

public class AdvanceController {

	private AdvanceDao advDao;
	private AdvanceTransactionDao advtDao;
	private FarmerDao farmerDao;
	private BuyerDao buyerDao;
	private PurchaseOrderDao poDao;
	private PurchaseOrderTransactionDao potDao;
	private SaleOrderDao soDao;
	private SaleOrderTransactionDao sotDao;
	private BalanceSheetTransactionDao bstDao;

	public AdvanceController(Dao dao) {
		this.advDao = dao.getAdvanceDao();
		this.advtDao = dao.getAdvanceTransactionDao();
		this.farmerDao = dao.getFarmerDao();
		this.buyerDao = dao.getBuyerDao();
		this.poDao = dao.getPurchaseOrderDao();
		this.potDao = dao.getPurchaseOrderTransactionDao();
		this.soDao = dao.getSaleOrderDao();
		this.sotDao = dao.getSaleOrderTransactionDao();
		this.bstDao = dao.getBalanceSheetTransactionDao();
	}

	public Advance getById(Long id) {
		return advDao.getById(id);
	}

	public Advance save(Advance advance) throws Exception {
		if(advance.getFarmer() == null && advance.getBuyer() == null)
			throw new AmbikaException("Incorrect input...");

		Farmer farmer = advance.getFarmer();
		Buyer buyer = advance.getBuyer();

		if(farmer != null) {
			if(farmer.getId() == null || farmer.getId().toString().trim().isEmpty())
				throw new AmbikaException("Incorrect input...");
			Long farmerId = farmer.getId();
			farmer = farmerDao.getById(farmerId);
			if(farmer == null)
				throw new AmbikaException("Farmer not found with id: " + farmerId);
			advance.setFarmer(farmer);
		}
		if(buyer != null) {
			if(buyer.getId() == null || buyer.getId().toString().trim().isEmpty())
				throw new AmbikaException("Incorrect input");
			Long buyerId = buyer.getId();
			buyer = buyerDao.getById(buyerId);
			if(buyer == null)
				throw new AmbikaException("Buyer not found with id: " + buyerId);
			advance.setBuyer(buyer);
		}

		if(advance.getDate() == null)
			throw new AmbikaException("Date cannot be empty...");

		if(advance.getTotalAmount() == null || advance.getTotalAmount().equals(new BigDecimal("0.00")))
			throw new AmbikaException("Amount cannot be empty...");

		advance.setPendingAmount(advance.getTotalAmount());
		return advDao.save(advance);
	}

	public List<Advance> findAll() {
		return advDao.findAll();
	}

	public String delete(Long id) throws Exception {
		Advance adv = getById(id);
		if(adv == null)
			throw new AmbikaException("Failed to find advance with id: " + id);

		Date now = new Date();
		adv.setDelete(true);
		adv.setDeleteDate(now);
		advDao.save(adv);

		for (AdvanceTransaction advt : adv.getAdvanceTransaction()) {
			advt.setDelete(true);
			advt.setDeleteDate(now);
			advtDao.save(advt);

			PurchaseOrderTransaction pot = advt.getPurchaseOrderTransaction();
			if(pot != null) {
				pot.setDelete(true);
				pot.setDeleteDate(now);
				potDao.save(pot);
				PurchaseOrder po = pot.getPurchaseOrder();
				po.setPendingAmount(po.getPendingAmount().add(pot.getPaidAmount()));
				po.setComplete(false);
				poDao.save(po);
			}

			SaleOrderTransaction sot = advt.getSaleOrderTransaction();
			if(sot != null) {
				sot.setDelete(true);
				sot.setDeleteDate(now);
				sotDao.save(sot);
				SaleOrder so = sot.getSaleOrder();
				so.setPendingAmount(so.getPendingAmount().add(sot.getPaidAmount()));
				so.setComplete(false);
				soDao.save(so);
			}
			for (BalanceSheetTransaction bst : bstDao.findByPot(pot)) {
				bst.setDelete(true);
				bst.setDeleteDate(now);
				bstDao.save(bst);
			}
		}

		return "Deleted advance...";
	}

	public List<Advance> findByType(String type) throws Exception {
		return advDao.findByType(type);
	}

	public List<Advance> search(Long farmerId, String from, String to) throws Exception {
		Farmer farmerObj = null;
		Date fromDt = null;
		Date toDt = null;

		if(farmerId != null)
			farmerObj = farmerDao.getById(farmerId);
		if(from != null && !from.trim().isEmpty())
			fromDt = DateUtils.format(from);
		if(to != null && !to.trim().isEmpty())
			toDt = DateUtils.format(to);

		return advDao.search(farmerObj, fromDt, toDt);
	}

}