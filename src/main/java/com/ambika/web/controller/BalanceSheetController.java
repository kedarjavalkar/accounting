package com.ambika.web.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ambika.web.core.BalanceSheet;
import com.ambika.web.core.transaction.BalanceSheetTransaction;
import com.ambika.web.dao.BalanceSheetDao;
import com.ambika.web.dao.transaction.BalanceSheetTransactionDao;
import com.ambika.web.exception.AmbikaException;
import com.ambika.web.utils.Dao;
import com.ambika.web.utils.DateUtils;

public class BalanceSheetController {

	private BalanceSheetDao bsDao;
	private BalanceSheetTransactionDao bstDao;
	private static final BigDecimal ZERO = new BigDecimal("0.00");

	public BalanceSheetController(Dao dao) {
		this.bsDao = dao.getBalanceSheetDao();
		this.bstDao = dao.getBalanceSheetTransactionDao();
	}

	public BalanceSheet save(BalanceSheet balanceSheet) {
		return bsDao.save(balanceSheet);
	}

	public BalanceSheet getById(Long id) throws Exception {
		if(id == null)
			throw new AmbikaException("Balance Sheet id cannot be empty...");
		return bsDao.getById(id);
	}

	public List<BalanceSheet> findAll() {
		return bsDao.findAll();
	}

	public BalanceSheet createBalanceSheet(Date date) throws Exception {
		date = DateUtils.setInitialTime(date);
		BalanceSheet balSheet = bsDao.findByDate(date);
		if(balSheet == null) {
			balSheet = new BalanceSheet();
			balSheet.setDate(date);
			balSheet.setOpeningAmount(ZERO);
			balSheet.setClosingAmount(ZERO);
			balSheet.setTotalExpenses(ZERO);
			balSheet.setTotalIncome(ZERO);
			balSheet = save(balSheet);
		}
		return balSheet;
	}

	// CRON
	public boolean openCloseBalanceSheet(String fromDate) throws Exception {

		Date from = DateUtils.setInitialTime(new Date());
		Date to = from;

		if(fromDate != null && !fromDate.trim().isEmpty()) {
			from = DateUtils.setInitialTime(bsDao.getOldestBS().getDate());
		}

		for (Date today : DateUtils.getDateRange(from, to)) {
			BigDecimal income = ZERO;
			BigDecimal expense = ZERO;
			BigDecimal openingAmount = ZERO;
			BigDecimal closingAmount = ZERO;

			BalanceSheet todayBs = createBalanceSheet(today);

			Date yesterday = DateUtils.getYesterday(today);
			BalanceSheet yesterdayBs = bsDao.findByDate(yesterday);
			if(yesterdayBs != null) {
				List<BalanceSheetTransaction> bstList = bstDao.findByBs(yesterdayBs);

				for (BalanceSheetTransaction bst : bstList) {
					if(bst.isDelete() == false) {
						if(bst.isAccountType()){
							income = income.add(bst.getAmount());
						} else {
							expense = expense.add(bst.getAmount());
						}
					}
				}

				openingAmount = yesterdayBs.getOpeningAmount();
				closingAmount = (openingAmount.subtract(expense)).add(income);
				yesterdayBs.setTotalExpenses(expense);
				yesterdayBs.setTotalIncome(income);
				yesterdayBs.setClosingAmount(closingAmount);
				bsDao.save(yesterdayBs);

				todayBs.setOpeningAmount(closingAmount);
				bsDao.save(todayBs);
			}
		}

		return true;
	}

	public List<BalanceSheet> findByDateRange(String date, String fromDate, String toDate) throws Exception {
		List<BalanceSheet> list = new ArrayList<BalanceSheet>();
		if(date != null) {
			list = new ArrayList<BalanceSheet>();
			Date dt = DateUtils.format(date);
			BalanceSheet bs = bsDao.findByDate(dt);
			if(bs != null)
				list.add(bs);
		}

		if(fromDate != null && toDate != null) {
			list = new ArrayList<BalanceSheet>();
			Date dtFrom = DateUtils.format(fromDate);
			Date dtTo = DateUtils.format(toDate);
			list.addAll(bsDao.findByDate(dtFrom, dtTo));
		}

		return list;
	}

}
