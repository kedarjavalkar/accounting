package com.ambika.web.controller.transaction;

import java.util.Date;
import java.util.List;

import com.ambika.web.core.BalanceSheet;
import com.ambika.web.core.MiscellaneousAccount;
import com.ambika.web.core.transaction.BalanceSheetTransaction;
import com.ambika.web.core.transaction.MiscellaneousAccountTransaction;
import com.ambika.web.dao.BalanceSheetDao;
import com.ambika.web.dao.MiscellaneousAccountDao;
import com.ambika.web.dao.transaction.BalanceSheetTransactionDao;
import com.ambika.web.dao.transaction.MiscellaneousAccountTransactionDao;
import com.ambika.web.exception.AmbikaException;
import com.ambika.web.utils.Dao;
import com.ambika.web.utils.DateUtils;

public class MiscellaneousTransactionController {

	private MiscellaneousAccountDao maDao;
	private MiscellaneousAccountTransactionDao matDao;
	private BalanceSheetDao bsDao;
	private BalanceSheetTransactionDao bstDao;

	public MiscellaneousTransactionController(Dao dao) {
		this.maDao = dao.getMiscellaneousAccountDao();
		this.matDao = dao.getMiscellaneousAccountTransactionDao();
		this.bsDao = dao.getBalanceSheetDao();
		this.bstDao = dao.getBalanceSheetTransactionDao();
	}

	public MiscellaneousAccountTransaction save(MiscellaneousAccountTransaction miscellaneousAccountTransaction) throws Exception {
		MiscellaneousAccount ma = null;
		BalanceSheet bs = null;
		Date dt = null;

		if(miscellaneousAccountTransaction.getMiscellaneousAccount() == null || 
				miscellaneousAccountTransaction.getMiscellaneousAccount().getId() == null)
			throw new AmbikaException("Miscellaneous account cannot be empty");

		if(miscellaneousAccountTransaction.getDate() == null)
			throw new AmbikaException("Date cannot be empty...");


		ma = maDao.getById(miscellaneousAccountTransaction.getMiscellaneousAccount().getId());
		dt = miscellaneousAccountTransaction.getDate();

		bs = bsDao.findByDate(DateUtils.setInitialTime(dt));

		if(ma == null)
			throw new AmbikaException("Miscellaneous account not found...");

		if(bs == null)
			throw new AmbikaException("Balance sheet not found for " +DateUtils.format(dt));

		miscellaneousAccountTransaction.setMiscellaneousAccount(ma);
		miscellaneousAccountTransaction = matDao.save(miscellaneousAccountTransaction);

		BalanceSheetTransaction bst = new BalanceSheetTransaction();
		bst.setAccountName(miscellaneousAccountTransaction.getMiscellaneousAccount().getName());
		bst.setAccountType(miscellaneousAccountTransaction.getMiscellaneousAccount().isType());
		bst.setAmount(miscellaneousAccountTransaction.getAmount());
		bst.setDate(dt);
		bst.setBalanceSheet(bs);
		bst.setMiscellaneousAccount(miscellaneousAccountTransaction.getMiscellaneousAccount());
		bst.setMiscellaneousAccountTransaction(miscellaneousAccountTransaction);
		bstDao.save(bst);

		return miscellaneousAccountTransaction;
	}

	public List<MiscellaneousAccountTransaction> findAll() {
		return matDao.findAll();
	}

	public MiscellaneousAccountTransaction getById(Long id) throws Exception {
		if(id == null)
			throw new AmbikaException("Miscellaneous Transaction id cannot be empty");
		return matDao.getById(id);
	}

	public String delete(Long id) throws Exception {
		String str = "";

		MiscellaneousAccountTransaction mat = getById(id);
		if(mat == null)
			throw new AmbikaException("Miscellaneous account transaction not found with id: " + id);

		Date now =  new Date();
		mat.setDelete(true);
		mat.setDeleteDate(now);
		matDao.save(mat);

		for (BalanceSheetTransaction bst : bstDao.findByMat(mat)) {
			bst.setDelete(true);
			bst.setDeleteDate(now);
			bstDao.save(bst);
		}

		str = "Deleted miscellaneous account transaction...";
		return str;
	}

}
