package com.ambika.web.controller.transaction;

import java.util.List;

import com.ambika.web.core.transaction.AdvanceTransaction;
import com.ambika.web.dao.AdvanceTransactionDao;
import com.ambika.web.exception.AmbikaException;
import com.ambika.web.utils.Dao;

public class AdvanceTransactionController {

	private AdvanceTransactionDao advtDao;

	public AdvanceTransactionController(Dao dao) {
		this.advtDao = dao.getAdvanceTransactionDao();
	}

	public List<AdvanceTransaction> findAll() {
		return advtDao.findAll();
	}

	public AdvanceTransaction getById(Long id) throws Exception {
		if(id == null)
			throw new AmbikaException("Advance Transaction id cannot be empty...");
		return advtDao.getById(id);
	}

}
