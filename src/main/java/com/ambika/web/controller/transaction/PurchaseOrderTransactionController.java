package com.ambika.web.controller.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ambika.web.core.Advance;
import com.ambika.web.core.BalanceSheet;
import com.ambika.web.core.Farmer;
import com.ambika.web.core.MiscellaneousAccount;
import com.ambika.web.core.PurchaseOrder;
import com.ambika.web.core.transaction.AdvanceTransaction;
import com.ambika.web.core.transaction.BalanceSheetTransaction;
import com.ambika.web.core.transaction.PurchaseOrderTransaction;
import com.ambika.web.dao.AdvanceDao;
import com.ambika.web.dao.AdvanceTransactionDao;
import com.ambika.web.dao.BalanceSheetDao;
import com.ambika.web.dao.FarmerDao;
import com.ambika.web.dao.MiscellaneousAccountDao;
import com.ambika.web.dao.PurchaseOrderDao;
import com.ambika.web.dao.transaction.BalanceSheetTransactionDao;
import com.ambika.web.dao.transaction.PurchaseOrderTransactionDao;
import com.ambika.web.exception.AmbikaException;
import com.ambika.web.utils.Dao;
import com.ambika.web.utils.DateUtils;

public class PurchaseOrderTransactionController {

	private FarmerDao farmerDao;
	private PurchaseOrderDao poDao;
	private PurchaseOrderTransactionDao potDao;
	private BalanceSheetDao bsDao;
	private BalanceSheetTransactionDao bstDao;
	private MiscellaneousAccountDao miscDao;
	private AdvanceDao advDao;
	private AdvanceTransactionDao advtDao;

	public PurchaseOrderTransactionController(Dao dao) {
		this.farmerDao = dao.getFarmerDao();
		this.poDao = dao.getPurchaseOrderDao();
		this.potDao = dao.getPurchaseOrderTransactionDao();
		this.bsDao = dao.getBalanceSheetDao();
		this.bstDao = dao.getBalanceSheetTransactionDao();
		this.miscDao = dao.getMiscellaneousAccountDao();
		this.advDao = dao.getAdvanceDao();
		this.advtDao = dao.getAdvanceTransactionDao();
	}

	public PurchaseOrderTransaction getById(Long id) {
		return potDao.getById(id);
	}

	public boolean save(List<Long> poIds, Long miscId, String date, String amountPaid, String discountAmount, Long advId) throws Exception {
		BalanceSheet bs = null;
		PurchaseOrderTransaction pot = null;
		MiscellaneousAccount miscObj = null;
		Date dt = null;
		BigDecimal paidAmt = new BigDecimal(amountPaid);
		BigDecimal discountAmt = new BigDecimal(discountAmount);

		if(miscId != null) {
			miscObj = miscDao.getById(miscId);
			if(miscObj == null)
				throw new AmbikaException("Miscellaneous Account not found with id: " + miscId);
		}

		if(date == null || date.isEmpty())
			throw new AmbikaException("Date cannot be empty");

		dt = DateUtils.format(date);
		bs = bsDao.findByDate(dt);
		if(bs == null)
			throw new AmbikaException("Balance sheet not found for " + date);

		List<PurchaseOrder> list = new ArrayList<PurchaseOrder>();
		for (Long id : poIds) {
			PurchaseOrder po = poDao.getById(id);
			if(po == null)
				throw new AmbikaException("Purchase order not found with id: " + id);
			if(po.isComplete() == false)
				list.add(po);
		}

		for (PurchaseOrder po : list) {

			// When discount given
			if(miscId != null && discountAmt.compareTo(new BigDecimal("0.00")) > 0
					&& po.getPendingAmount().compareTo(new BigDecimal("0.00")) > 0) {
				pot = new PurchaseOrderTransaction();
				pot.setDate(dt);

				BigDecimal carriedDiscount = new BigDecimal("0.00");

				if(po.getPendingAmount().compareTo(discountAmt) <= 0) {
					discountAmt = discountAmt.subtract(po.getPendingAmount());
					pot.setPaidAmount(po.getPendingAmount());
					pot.setDiscountAmount(pot.getPaidAmount());
					carriedDiscount = po.getPendingAmount();
					po.setDiscountAmount(po.getDiscountAmount().add(po.getPendingAmount()));
					po.setPendingAmount(new BigDecimal("0.00"));
					po.setComplete(true);
				} else if(po.getPendingAmount().compareTo(discountAmt) > 0) {
					carriedDiscount = discountAmt;
					po.setDiscountAmount(po.getDiscountAmount().add(discountAmt));
					po.setPendingAmount(po.getPendingAmount().subtract(discountAmt));
					pot.setPaidAmount(discountAmt);
					pot.setDiscountAmount(pot.getPaidAmount());
					discountAmt = new BigDecimal("0.00");
				}
				po = poDao.save(po);
				pot.setPendingAmount(po.getPendingAmount());
				pot.setTotalAmount(po.getAmount());
				pot.setType(miscObj.isType());
				pot.setPurchaseOrder(po);
				pot.setDiscount(true);
				pot = potDao.save(pot);

				BalanceSheetTransaction bst = new BalanceSheetTransaction();
				bst.setAccountType(pot.isType());
				bst.setAmount(pot.getPaidAmount());
				bst.setDate(pot.getDate());
				bst.setPurchaseOrderTransaction(pot);
				bst.setBalanceSheet(bs);
				if(miscId != null) {
					bst.setAccountName(pot.getPurchaseOrder().getFarmer().getName() + " - " + miscObj.getName());
					bst.setMiscellaneousAccount(miscObj);
				}
				bstDao.save(bst);

				bst = new BalanceSheetTransaction();
				bst.setAccountType(!pot.isType());
				bst.setAmount(carriedDiscount);
				bst.setDate(pot.getDate());
				bst.setPurchaseOrderTransaction(pot);
				bst.setBalanceSheet(bs);
				if(miscId != null) {
					bst.setAccountName(pot.getPurchaseOrder().getFarmer().getName() + " - " + miscObj.getName() + " - Balance");
					bst.setMiscellaneousAccount(miscObj);
				}
				bstDao.save(bst);
			}

			// When payment is done
			if(paidAmt.compareTo(new BigDecimal("0.00")) > 0
					&& po.getPendingAmount().compareTo(new BigDecimal("0.00")) > 0) {
				pot = new PurchaseOrderTransaction();
				pot.setDate(dt);

				if(po.getPendingAmount().compareTo(paidAmt) <= 0) {
					pot.setPaidAmount(po.getPendingAmount());
					po.setComplete(true);
					paidAmt = paidAmt.subtract(po.getPendingAmount());
					po.setPendingAmount(new BigDecimal("0.00"));
				} else if(po.getPendingAmount().compareTo(paidAmt) > 0) {
					pot.setPaidAmount(paidAmt);
					po.setPendingAmount(po.getPendingAmount().subtract(paidAmt));
					paidAmt = new BigDecimal("0.00");
				}

				po = poDao.save(po);
				pot.setDiscountAmount(new BigDecimal("0.00"));
				pot.setPendingAmount(po.getPendingAmount());
				pot.setTotalAmount(po.getAmount());
				pot.setType(false);
				pot.setPurchaseOrder(po);
				pot = potDao.save(pot);

				String bstName = pot.getPurchaseOrder().getFarmer().getName();
				if(advId != null) {
					Advance adv = advDao.getById(advId);
					if(adv == null)
						throw new AmbikaException("Advance not found with id: " + advId);

					adv.setPendingAmount(adv.getPendingAmount().subtract(pot.getPaidAmount()));
					if(adv.getPendingAmount().compareTo(new BigDecimal("0.00")) == 0)
						adv.setComplete(true);
					advDao.save(adv);

					AdvanceTransaction advt = new AdvanceTransaction();
					advt.setAdvance(adv);
					advt.setAmount(pot.getPaidAmount());
					advt.setDate(dt);
					advt.setPurchaseOrderTransaction(pot);
					advtDao.save(advt);

					pot.setAdvance(true);
					potDao.save(pot);	
					bstName = bstName.concat(" - Advance");
				} else {
					bstName = bstName.concat(" - Cash");
				}

				BalanceSheetTransaction bst = new BalanceSheetTransaction();
				bst.setAccountType(pot.isType());
				bst.setAmount(pot.getPaidAmount());
				bst.setDate(pot.getDate());
				bst.setPurchaseOrderTransaction(pot);
				bst.setBalanceSheet(bs);
				bst.setAccountName(bstName);
				bstDao.save(bst);
			}

		}
		return true;
	}

	public List<PurchaseOrderTransaction> findAll() {
		return potDao.findAll();
	}

	public List<PurchaseOrderTransaction> search(Long farmerId, String from, String to) throws Exception {
		List<PurchaseOrder> list = null;
		Date fromDate = null;
		Date toDate = null;

		if(farmerId != null) {
			Farmer farmer = farmerDao.getById(farmerId);
			if(farmer != null) {
				list = new ArrayList<PurchaseOrder>();
				list = poDao.search(farmer, null, null, null);
			}
		}
		if(from != null)
			fromDate = DateUtils.format(from);
		if(to != null)
			toDate = DateUtils.format(to);

		return potDao.search(list, fromDate, toDate);
	}

	public String delete(Long id) throws Exception {

		PurchaseOrderTransaction pot = getById(id);
		if(pot == null)
			throw new AmbikaException("Purchase order transaction not found with Id: " + id);

		Date now = new Date();
		pot.setDelete(true);
		pot.setDeleteDate(now);
		potDao.save(pot);

		for (BalanceSheetTransaction bst : bstDao.findByPot(pot)) {
			bst.setDelete(true);
			bst.setDeleteDate(now);
			bstDao.save(bst);
		}

		PurchaseOrder po = pot.getPurchaseOrder();
		if(pot.isDiscount())
			po.setDiscountAmount(po.getDiscountAmount().subtract(pot.getPaidAmount()));

		if(pot.isAdvance()) {
			for (AdvanceTransaction advt : pot.getAdvanceTransaction()) {
				advt.setDelete(true);
				advt.setDeleteDate(now);
				advtDao.save(advt);

				Advance adv = advt.getAdvance();
				adv.setPendingAmount(adv.getPendingAmount().add(advt.getAmount()));
				adv.setComplete(false);
				advDao.save(adv);
			}
		}
		po.setPendingAmount(po.getPendingAmount().add(pot.getPaidAmount()));
		po.setComplete(false);
		poDao.save(po);

		return "Purchase order transaction deleted...";
	}

}
