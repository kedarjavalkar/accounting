package com.ambika.web.controller.transaction;

import java.util.List;

import com.ambika.web.core.transaction.BalanceSheetTransaction;
import com.ambika.web.dao.transaction.BalanceSheetTransactionDao;
import com.ambika.web.utils.Dao;

public class BalanceSheetTransactionController {

	private BalanceSheetTransactionDao bstDao;

	public BalanceSheetTransactionController(Dao dao) {
		this.bstDao = dao.getBalanceSheetTransactionDao();
	}

	public BalanceSheetTransaction getById(Long id) {
		return bstDao.getById(id);
	}

	public List<BalanceSheetTransaction> findAll() {
		return bstDao.findAll();
	}

}
