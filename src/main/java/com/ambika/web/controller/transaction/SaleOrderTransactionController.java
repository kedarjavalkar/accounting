package com.ambika.web.controller.transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ambika.web.core.Advance;
import com.ambika.web.core.BalanceSheet;
import com.ambika.web.core.Buyer;
import com.ambika.web.core.MiscellaneousAccount;
import com.ambika.web.core.SaleOrder;
import com.ambika.web.core.transaction.AdvanceTransaction;
import com.ambika.web.core.transaction.BalanceSheetTransaction;
import com.ambika.web.core.transaction.SaleOrderTransaction;
import com.ambika.web.dao.AdvanceDao;
import com.ambika.web.dao.AdvanceTransactionDao;
import com.ambika.web.dao.BalanceSheetDao;
import com.ambika.web.dao.BuyerDao;
import com.ambika.web.dao.MiscellaneousAccountDao;
import com.ambika.web.dao.SaleOrderDao;
import com.ambika.web.dao.transaction.BalanceSheetTransactionDao;
import com.ambika.web.dao.transaction.SaleOrderTransactionDao;
import com.ambika.web.exception.AmbikaException;
import com.ambika.web.utils.Dao;
import com.ambika.web.utils.DateUtils;

public class SaleOrderTransactionController {

	private BuyerDao buyerDao;
	private SaleOrderDao soDao;
	private SaleOrderTransactionDao sotDao;
	private BalanceSheetDao bsDao;
	private BalanceSheetTransactionDao bstDao;
	private MiscellaneousAccountDao miscDao;
	private AdvanceDao advDao;
	private AdvanceTransactionDao advtDao;

	public SaleOrderTransactionController(Dao dao) {
		this.buyerDao = dao.getBuyerDao();
		this.soDao = dao.getSaleOrderDao();
		this.sotDao = dao.getSaleOrderTransactionDao();
		this.bsDao = dao.getBalanceSheetDao();
		this.bstDao = dao.getBalanceSheetTransactionDao();
		this.miscDao = dao.getMiscellaneousAccountDao();
		this.advDao = dao.getAdvanceDao();
		this.advtDao = dao.getAdvanceTransactionDao();
	}

	public SaleOrderTransaction getById(Long id) {
		return sotDao.getById(id);
	}

	public boolean save(List<Long> soIds, Long miscId, String date, String amountPaid, String discountAmount, Long advId) throws Exception {
		BalanceSheet bs = null;
		SaleOrderTransaction sot = null;
		MiscellaneousAccount miscObj = null;
		Date dt = null;
		BigDecimal paidAmt = new BigDecimal(amountPaid);
		BigDecimal discountAmt = new BigDecimal(discountAmount);

		if(miscId != null) {
			miscObj = miscDao.getById(miscId);
			if(miscObj == null)
				throw new AmbikaException("Miscellaneous Account not found with id: " + miscId);
		}

		if(date == null || date.isEmpty())
			throw new AmbikaException("Date cannot be empty");

		dt = DateUtils.format(date);
		bs = bsDao.findByDate(dt);
		if(bs == null)
			throw new AmbikaException("Balance sheet not found for " + date);

		List<SaleOrder> list = new ArrayList<SaleOrder>();
		for (Long id : soIds) {
			SaleOrder so = soDao.getById(id);
			if(so == null)
				throw new AmbikaException("Sale order not found with id: " + id);
			if(so.isComplete() == false)
				list.add(so);
		}

		for (SaleOrder so : list) {

			// When discount given
			if(miscId != null && discountAmt.compareTo(new BigDecimal("0.00")) > 0
					&& so.getPendingAmount().compareTo(new BigDecimal("0.00")) > 0) {
				sot = new SaleOrderTransaction();
				sot.setDate(dt);

				BigDecimal carriedDiscount = new BigDecimal("0.00");

				if(so.getPendingAmount().compareTo(discountAmt) <= 0) {
					discountAmt = discountAmt.subtract(so.getPendingAmount());
					sot.setPaidAmount(so.getPendingAmount());
					sot.setDiscountAmount(sot.getPaidAmount());
					carriedDiscount = so.getPendingAmount();
					so.setDiscountAmount(so.getDiscountAmount().add(so.getPendingAmount()));
					so.setPendingAmount(new BigDecimal("0.00"));
					so.setComplete(true);
				} else if(so.getPendingAmount().compareTo(discountAmt) > 0) {
					carriedDiscount = discountAmt;
					so.setDiscountAmount(so.getDiscountAmount().add(discountAmt));
					so.setPendingAmount(so.getPendingAmount().subtract(discountAmt));
					sot.setPaidAmount(discountAmt);
					sot.setDiscountAmount(sot.getPaidAmount());
					discountAmt = new BigDecimal("0.00");
				}
				so = soDao.save(so);
				sot.setPendingAmount(so.getPendingAmount());
				sot.setTotalAmount(so.getAmount());
				sot.setType(miscObj.isType());
				sot.setSaleOrder(so);
				sot.setDiscount(true);
				sot = sotDao.save(sot);

				BalanceSheetTransaction bst = new BalanceSheetTransaction();
				bst.setAccountType(sot.isType());
				bst.setAmount(sot.getPaidAmount());
				bst.setDate(sot.getDate());
				bst.setSaleOrderTransaction(sot);
				bst.setBalanceSheet(bs);
				if(miscId != null) {
					bst.setAccountName(sot.getSaleOrder().getBuyer().getName() + " - " + miscObj.getName());
					bst.setMiscellaneousAccount(miscObj);
				}
				bstDao.save(bst);

				bst = new BalanceSheetTransaction();
				bst.setAccountType(!sot.isType());
				bst.setAmount(carriedDiscount);
				bst.setDate(sot.getDate());
				bst.setSaleOrderTransaction(sot);
				bst.setBalanceSheet(bs);
				if(miscId != null) {
					bst.setAccountName(sot.getSaleOrder().getBuyer().getName() + " - " + miscObj.getName() + " - Balance");
					bst.setMiscellaneousAccount(miscObj);
				}
				bstDao.save(bst);
			}

			// When payment is done
			if(paidAmt.compareTo(new BigDecimal("0.00")) > 0
					&& so.getPendingAmount().compareTo(new BigDecimal("0.00")) > 0) {
				sot = new SaleOrderTransaction();
				sot.setDate(dt);

				if(so.getPendingAmount().compareTo(paidAmt) <= 0) {
					sot.setPaidAmount(so.getPendingAmount());
					so.setComplete(true);
					paidAmt = paidAmt.subtract(so.getPendingAmount());
					so.setPendingAmount(new BigDecimal("0.00"));
				} else if(so.getPendingAmount().compareTo(paidAmt) > 0) {
					sot.setPaidAmount(paidAmt);
					so.setPendingAmount(so.getPendingAmount().subtract(paidAmt));
					paidAmt = new BigDecimal("0.00");
				}

				so = soDao.save(so);
				sot.setDiscountAmount(new BigDecimal("0.00"));
				sot.setPendingAmount(so.getPendingAmount());
				sot.setTotalAmount(so.getAmount());
				sot.setType(true);
				sot.setSaleOrder(so);
				sot = sotDao.save(sot);

				String bstName = sot.getSaleOrder().getBuyer().getName();
				if(advId != null) {
					Advance adv = advDao.getById(advId);
					if(adv == null)
						throw new AmbikaException("Advance not found with id: " + advId);

					adv.setPendingAmount(adv.getPendingAmount().subtract(sot.getPaidAmount()));
					if(adv.getPendingAmount().compareTo(new BigDecimal("0.00")) == 0)
						adv.setComplete(true);
					advDao.save(adv);

					AdvanceTransaction advt = new AdvanceTransaction();
					advt.setAdvance(adv);
					advt.setAmount(sot.getPaidAmount());
					advt.setDate(dt);
					advt.setSaleOrderTransaction(sot);
					advtDao.save(advt);

					sot.setAdvance(true);
					sotDao.save(sot);	
					bstName = bstName.concat(" - Advance");
				} else {
					bstName = bstName.concat(" - Cash");
				}

				BalanceSheetTransaction bst = new BalanceSheetTransaction();
				bst.setAccountType(sot.isType());
				bst.setAmount(sot.getPaidAmount());
				bst.setDate(sot.getDate());
				bst.setSaleOrderTransaction(sot);
				bst.setBalanceSheet(bs);
				bst.setAccountName(bstName);
				bstDao.save(bst);
			}

		}
		return true;
	}

	public List<SaleOrderTransaction> findAll() {
		return sotDao.findAll();
	}

	public String delete(Long id) throws Exception {

		SaleOrderTransaction sot = getById(id);
		if(sot == null)
			throw new AmbikaException("Sale order transaction not found with Id: " + id);

		Date now = new Date();
		sot.setDelete(true);
		sot.setDeleteDate(now);
		sotDao.save(sot);

		for (BalanceSheetTransaction bst : bstDao.findBySot(sot)) {
			bst.setDelete(true);
			bst.setDeleteDate(now);
			bstDao.save(bst);
		}

		SaleOrder so = sot.getSaleOrder();
		if(sot.isDiscount())
			so.setDiscountAmount(so.getDiscountAmount().subtract(sot.getPaidAmount()));

		if(sot.isAdvance()) {
			for (AdvanceTransaction advt : sot.getAdvanceTransaction()) {
				advt.setDelete(true);
				advt.setDeleteDate(now);
				advtDao.save(advt);

				Advance adv = advt.getAdvance();
				adv.setPendingAmount(adv.getPendingAmount().add(advt.getAmount()));
				adv.setComplete(false);
				advDao.save(adv);
			}
		}
		so.setPendingAmount(so.getPendingAmount().add(sot.getPaidAmount()));
		so.setComplete(false);
		soDao.save(so);

		return "Sale order transaction deleted...";
	}

	public List<SaleOrderTransaction> search(Long buyerId, String from, String to) throws Exception {
		List<SaleOrder> list = null;
		Date fromDate = null;
		Date toDate = null;

		if(buyerId != null) {
			Buyer byuer = buyerDao.getById(buyerId);
			if(byuer != null) {
				list = new ArrayList<SaleOrder>();
				list = soDao.search(byuer, null, null, null);
			}
		}
		if(from != null)
			fromDate = DateUtils.format(from);
		if(to != null)
			toDate = DateUtils.format(to);

		return sotDao.search(list, fromDate, toDate);
	}

}