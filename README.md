
# Running The Application

* To package., run

	```mvn package```

* To setup MySql database., run

	```java -jar target/ambika-webservices-0.1.jar db migrate ambika.yml```

* To run server., run

	```java -jar target/ambika-webservices-0.1.jar server ambika.yml```