@echo off
TITLE = Emergency no : +91 9766904230

echo.
echo Pulling from Openshift....
mkdir RED_HAT
cd RED_HAT
git clone ssh://55d09dde7628e18a4200006d@app-ambika.rhcloud.com/~/git/app.git/
cd ..\
echo Pulling from Openshift..... Done


echo.
echo Pulling from BitBucket....
git pull
echo Pulling from BitBucket..... Done


echo.
echo File deleting....
@RD /S /Q RED_HAT\app\diy
@RD /S /Q RED_HAT\app\.openshift
echo File deleting..... Done
echo File copying....
mkdir RED_HAT\app\diy\src
mkdir RED_HAT\app\.openshift
XCOPY /S src RED_HAT\app\diy\src
XCOPY /S .openshift RED_HAT\app\.openshift
COPY ambika.openshift.yml RED_HAT\app\diy\ambika.openshift.yml
COPY pom.xml RED_HAT\app\diy\pom.xml
echo File copying..... Done


echo.
echo.
echo.
set /p Q=Press Enter to deploy on Openshift...
cd RED_HAT\app
git add -A --all .
git commit -m "auto deployment"
git push
cd ..\..\
echo.
set /p Q=Press Enter to exit...
@RD /S /Q RED_HAT